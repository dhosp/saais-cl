// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/values/message_values.dart';

class ExtracurricularAPI extends BaseNetwork {
  Future<Response> findOne(int id) async {
    return await get("extracurricular/$id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = ""}) async {
    Response result = await get("extracurricular",
        query: {"page": page, "perPage": perPage, "search": search ?? ""}
            .map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update(
      {required int id, required Extracurricular extracurricular}) async {
    Response result =
        await put("extracurricular/$id", extracurricular.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({required Extracurricular extracurricular}) async {
    Response result = await post("extracurricular/", extracurricular.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("extracurricular/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
