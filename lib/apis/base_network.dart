import 'dart:async';

import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/custom_widgets/custom_snackbar.dart';
import 'package:saais_client/values/constant_values.dart';
import 'package:saais_client/values/message_values.dart';

class BaseNetwork extends GetConnect {
  @override
  void onInit() {
    httpClient.baseUrl = ConstantValues.apiUrl;

    httpClient.addRequestModifier(_requestInterceptor);
    httpClient.addResponseModifier(_responseInterceptor);
    httpClient.timeout =
        const Duration(seconds: ConstantValues.maxNetworkTimeout);
  }

  FutureOr<Request> _requestInterceptor(request) async {
    var token = GetStorage().read("token");
    request.headers["Authorization"] = "Bearer $token";
    // debugPrint('Url: ${request.method} ${request.url}\n');
    // debugPrint('Header: ${request.headers}');
    return request;
  }

  FutureOr<dynamic> _responseInterceptor(
      Request request, Response response) async {
    // debugPrint('Status Code: ${response.statusCode}\n');
    // debugPrint('Data: ${response.bodyString?.toString() ?? ''}');
    try {
      if (response.body?["status"] ?? false) {
        var successMessage = response.body['message'];
        if (successMessage != null) {
          successMessage = successMessage["message"];

          if (successMessage != "") {
            CustomSnackBar.showSucessToast(successMessage);
          }
        }
      } else {
        var errorMessage = response.body['message'].toString();
        handleError(
            errorMessage != "" ? errorMessage : MessageValues.apiFailedConnect);
      }
    } catch (e) {
      handleError(MessageValues.apiFailedConnect);
    }
    return response;
  }

  void handleError(String errorMessage) {
    CustomSnackBar.showErrorToast(errorMessage);
  }
}
