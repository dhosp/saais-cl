// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/values/message_values.dart';

class SchoolAttendanceAPI extends BaseNetwork {
  Future<Response> findOne(int attendance_id) async {
    return await get("school-attendance/$attendance_id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, int? user_id}) async {
    Response result = await get("school-attendance",
        query: {"page": page, "perPage": perPage, "user_id": user_id ?? ""}
            .map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> manualCreate(
      {required int user_id,
      required String type,
      DateTime? clock_in_at,
      DateTime? clock_out_at,
      String? reason}) async {
    Response result = await post("school-attendance", {
      "user_id": user_id,
      "type": type,
      "clock_in_at":
          clock_in_at != null ? Formatter.dateFormatter(date: clock_in_at) : "",
      "clock_out_at": clock_out_at != null
          ? Formatter.dateFormatter(date: clock_out_at)
          : "",
      "reason": reason ?? ""
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> clockInOut(
      {required String username, required String type}) async {
    Response result = await post("school-attendance/realtime", {
      "username": username,
      "attendance_type": type,
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> remove({
    required int attendance_id,
  }) async {
    Response result = await delete("school-attendance/$attendance_id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
