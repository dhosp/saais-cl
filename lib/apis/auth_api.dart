import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/values/message_values.dart';

class AuthAPI extends BaseNetwork {
  Future<Response> login(String username, String password) async {
    Response result =
        await post("auth/login", {"username": username, "password": password});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }
    return result;
  }

  Future<Response> resetPass(
      String username, String code, String password) async {
    Response result = await post("auth/reset-pass",
        {"username": username, "code": code, "password": password});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }
    return result;
  }
}
