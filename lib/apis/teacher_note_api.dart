// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/teacher_note.dart';
import 'package:saais_client/values/message_values.dart';

class TeacherNoteAPI extends BaseNetwork {
  Future<Response> findOne(int id) async {
    return await get("teacher-note/$id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = "", type = ""}) async {
    Response result = await get("teacher-note",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search ?? "",
          "type": type ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update(
      {required int id, required TeacherNote attitude}) async {
    Response result = await put("teacher-note/$id", attitude.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({required TeacherNote teacherNote}) async {
    Response result = await post("teacher-note/", teacherNote.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("teacher-note/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
