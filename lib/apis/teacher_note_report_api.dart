// ignore_for_file: non_constant_identifier_names

import 'dart:async';
import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/attitude.dart';
import 'package:saais_client/models/teacher_note.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/values/message_values.dart';

class TeacherNoteReportAPI extends BaseNetwork {
  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = "", student_id = ""}) async {
    Response result = await get("teacher-note-result/",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search ?? "",
          "student_id": student_id ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required int studentId, required List<int> noteIds}) async {
    Response result = await post(
        "teacher-note-result/", {"student_id": studentId, "note_ids": noteIds});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
