// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/values/message_values.dart';

class JournalAPI extends BaseNetwork {
  Future<Response> findOne(int session_id) async {
    return await get("session/$session_id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, int class_id = 0, search = ""}) async {
    Response result = await get("session",
        query: {
          "page": page,
          "perPage": perPage,
          "class_id": class_id > 0 ? class_id : "",
          "search": search ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update({
    required int id,
    required int teacher_id,
    required String lesson_note,
    required DateTime started_at,
    required DateTime finished_at,
  }) async {
    Response result = await put("session/$id", {
      "teacher_id": teacher_id,
      "lesson_note": lesson_note,
      "started_at": Formatter.dateFormatter(date: started_at),
      "finished_at": Formatter.dateFormatter(date: finished_at)
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({
    required int subject_id,
    required int teacher_id,
    required String lesson_note,
    required DateTime started_at,
    required DateTime finished_at,
  }) async {
    Response result = await post("session/", {
      "subject_id": subject_id,
      "teacher_id": teacher_id,
      "lesson_note": lesson_note,
      "started_at": Formatter.dateFormatter(date: started_at),
      "finished_at": Formatter.dateFormatter(date: finished_at)
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> updateStudents(
      {required int id, required List<int> student_ids}) async {
    Response result =
        await put("session/$id/attendance", {"student_ids": student_ids});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("session/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
