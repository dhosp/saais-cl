// ignore_for_file: non_constant_identifier_names

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/extracurricular_score.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/values/message_values.dart';

class ExtracurricularScoreAPI extends BaseNetwork {
  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = "", int? exculId}) async {
    Response result = await get("extracurricular-score",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search ?? "",
          "nonacademic_subject_id": exculId ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required int exculId,
      required List<ExtracurricularScore> scores}) async {
    var jsonScores = [];
    for (var item in scores) {
      jsonScores.add(item.toJson());
    }
    Response result =
        await post("extracurricular-score/$exculId", {"scores": jsonScores});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
