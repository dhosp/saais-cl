// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/schedule.dart';
import 'package:saais_client/values/message_values.dart';

class ScheduleAPI extends BaseNetwork {
  Future<Response> findOne(int id) async {
    return await get("schedule/$id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = ""}) async {
    Response result = await get("schedule",
        query: {"page": page, "perPage": perPage, "search": search ?? ""}
            .map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update({required int id, required Schedule schedule}) async {
    Response result = await put("schedule/$id", schedule.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({required Schedule schedule}) async {
    Response result = await post("schedule/", schedule.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("schedule/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
