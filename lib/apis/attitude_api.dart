// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/attitude.dart';
import 'package:saais_client/values/message_values.dart';

class AttitudeAPI extends BaseNetwork {
  Future<Response> findOne(int id) async {
    return await get("attitude/$id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = "", type = ""}) async {
    Response result = await get("attitude",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search ?? "",
          "type": type ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update({required int id, required Attitude attitude}) async {
    Response result = await put("attitude/$id", attitude.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({required Attitude attitude}) async {
    Response result = await post("attitude/", attitude.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("attitude/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
