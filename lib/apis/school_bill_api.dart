// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/values/message_values.dart';

class SchoolBillAPI extends BaseNetwork {
  Future<Response> findOne(int bill_id) async {
    return await get("school-bill/$bill_id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, int? student_id}) async {
    Response result = await get("school-bill",
        query: {
          "page": page,
          "perPage": perPage,
          "student_id": student_id ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required int amount,
      required String grade,
      bool regenerate = false,
      String note = "",
      List<int>? studentIds}) async {
    Response result = await post("school-bill", {
      "amount": 60000,
      "class_grade": grade,
      "student_ids": studentIds ?? [],
      "force_regenerate": regenerate,
      "note": note,
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> createPayment(
      {required int bill_id, required int amount}) async {
    Response result = await put("school-bill/$bill_id/payment", {
      "amount": amount,
      "payment_type": "cash",
      "paid_at": Formatter.dateFormatter(
        date: DateTime.now(),
      ),
      "payment_proof": ""
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> removePayment({required int payment_id}) async {
    Response result = await delete("school-bill/payment/$payment_id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
