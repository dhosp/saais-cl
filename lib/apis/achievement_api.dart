// ignore_for_file: non_constant_identifier_names

import 'dart:async';
import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/achievement.dart';
import 'package:saais_client/values/message_values.dart';

class AchievementAPI extends BaseNetwork {
  Future<Response> findOne(int id) async {
    return await get("achievement/$id");
  }

  Future<Response> findAll(
      {int page = 1,
      int perPage = 20,
      String search = "",
      required String type,
      required int user_id}) async {
    Response result = await get("achievement/",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search,
          "type": type,
          "user_id": user_id
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update({required Achievement achievement}) async {
    Response result =
        await put("achievement/${achievement.id}", achievement.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({required Achievement achievement}) async {
    Response result = await post("achievement/", achievement.toJson());

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> remove(int achievement_id) async {
    Response result = await delete("achievement/$achievement_id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
