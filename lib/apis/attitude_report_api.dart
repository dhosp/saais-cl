// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/values/message_values.dart';

class AttitudeReportAPI extends BaseNetwork {
  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = "", int? studentId}) async {
    Response result = await get("attitude-report",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search ?? "",
          "student_id": studentId ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create({
    required int student_id,
    required String sosial_grade,
    required String spiritual_grade,
    required List<int> always_done_sosial_attitudes,
    required List<int> improve_sosial_attitudes,
    required List<int> always_done_spiritual_attitudes,
    required List<int> improve_spiritual_attitudes,
  }) async {
    Response result = await post("attitude-report/", {
      "student_id": student_id,
      "sosial_grade": sosial_grade,
      "spiritual_grade": spiritual_grade,
      "always_done_sosial_attitudes": always_done_sosial_attitudes,
      "improve_sosial_attitudes": improve_sosial_attitudes,
      "always_done_spiritual_attitudes": always_done_spiritual_attitudes,
      "improve_spiritual_attitudes": improve_spiritual_attitudes,
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
