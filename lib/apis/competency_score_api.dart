// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/competency_score.dart';
import 'package:saais_client/values/message_values.dart';

class CompetencyScoreAPI extends BaseNetwork {
  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = "", int? competencyId}) async {
    Response result = await get("competency-score",
        query: {
          "page": page,
          "perPage": perPage,
          "search": search ?? "",
          "competency_id": competencyId ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required int competency_id,
      required List<CompetencyScore> scores}) async {
    var jsonScores = [];
    for (var item in scores) {
      jsonScores.add(item.toJson());
    }
    Response result = await post("competency-score/",
        {"competency_id": competency_id, "scores": jsonScores});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
