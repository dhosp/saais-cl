// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';

class ReportAPI extends BaseNetwork {
  Future<Response> getStudentDashboard() async {
    return await get("report/student-dashboard");
  }

  Future<Response> getMainReport(int studentId) async {
    return await get("report/main-report/$studentId");
  }
}
