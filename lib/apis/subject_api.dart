// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/values/message_values.dart';

class SubjectAPI extends BaseNetwork {
  Future<Response> findOne(int subject_id) async {
    return await get("subject/$subject_id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, int class_id = 0, search = ""}) async {
    Response result = await get("subject",
        query: {
          "page": page,
          "perPage": perPage,
          "class_id": class_id > 0 ? class_id : "",
          "search": search ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update(
      {required int id,
      required int teacher_id,
      required int pass_score}) async {
    Response result = await put(
        "subject/$id", {"teacher_id": teacher_id, "pass_score": pass_score});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required String name,
      required String code,
      required int teacher_id,
      required int class_major_id,
      required String class_grade,
      bool regenerate = false}) async {
    Response result = await post("subject/", {
      "name": name,
      "code": code,
      "teacher_id": teacher_id,
      "class_major_id": class_major_id,
      "class_grade": class_grade,
      "force_regenerate": regenerate
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("subject/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
