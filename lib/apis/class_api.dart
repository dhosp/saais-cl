// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/values/message_values.dart';

class ClassAPI extends BaseNetwork {
  Future<Response> findOne(int class_id) async {
    return await get("class/$class_id");
  }

  Future<Response> findAll(
      {int page = 1, int perPage = 20, search = ""}) async {
    Response result = await get("class",
        query: {"page": page, "perPage": perPage, "search": search}
            .map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update({required int id, required int teacher_id}) async {
    Response result = await put("class/$id", {"teacher_id": teacher_id});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> updateStudents(
      {required int id, required List<int> student_ids}) async {
    Response result =
        await put("class/$id/student", {"student_ids": student_ids});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required int class_count,
      required int major_id,
      required String grade,
      bool regenerate = false}) async {
    Response result = await post("class/", {
      "class_count": class_count,
      "major_id": major_id,
      "grade": grade,
      "force_regenerate": regenerate
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("class/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
