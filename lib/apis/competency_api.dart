// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/competency.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/values/message_values.dart';

class CompetencyAPI extends BaseNetwork {
  Future<Response> findAll(
      {int page = 1,
      int perPage = 20,
      int subject_id = 0,
      search = "",
      String? type}) async {
    Response result = await get("competency",
        query: {
          "page": page,
          "perPage": perPage,
          "subject_id": subject_id > 0 ? subject_id : "",
          "type": type ?? "",
          "search": search ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(
      {required Class classroom,
      required Subject subject,
      required Competency competency}) async {
    Response result = await post("competency/", {
      "class_major_id": classroom.major_id,
      "class_grade": classroom.grade,
      "class_ids": [classroom.id],
      "subject_code": subject.code,
      "type": competency.type,
      "code": competency.code,
      "description": competency.description
    });

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> remove(int id) async {
    Response result = await delete("competency/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
