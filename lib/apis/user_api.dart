// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/message_values.dart';

class UserAPI extends BaseNetwork {
  Future<Response> findOne(int userId) async {
    return await get("user/$userId");
  }

  Future<Response> findAll(
      {int page = 1,
      int perPage = 20,
      String role = "",
      String search = "",
      bool? includeBill,
      bool? includeAttendance,
      bool? includeAchievement,
      bool? includeAttitude,
      bool? includeScore,
      int? class_id,
      int? subject_id}) async {
    Response result = await get("user",
        query: {
          "page": page,
          "perPage": perPage,
          "role": role,
          "search": search,
          "includeBill": includeBill ?? "",
          "includeAttendance": includeAttendance ?? "",
          "includeAchievement": includeAchievement ?? "",
          "includeScore": includeScore ?? "",
          "includeAttitude": includeAttitude ?? "",
          "class_id": class_id ?? "",
          "subject_id": subject_id ?? ""
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> update({required int id, required User user}) async {
    Response result = await put("user/$id", user.toJson(withId: false));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> create(User user) async {
    Response result = await post("user/", user.toJson(withId: false));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> destroy(int id) async {
    Response result = await delete("user/$id");

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }

  Future<Response> resetPass(int id) async {
    Response result = await put("user/$id/reset-pass", {});

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
