// ignore_for_file: non_constant_identifier_names

import 'dart:async';

import 'package:get/get.dart';
import 'package:saais_client/apis/base_network.dart';
import 'package:saais_client/values/message_values.dart';

class CityAPI extends BaseNetwork {
  Future<Response> findAll({int page = 1, int perPage = -1}) async {
    Response result = await get("city",
        query: {
          "page": page,
          "perPage": perPage,
        }.map((key, value) => MapEntry(key, value.toString())));

    if (result.body == null) {
      handleError(MessageValues.apiFailedConnect);
    }

    return result;
  }
}
