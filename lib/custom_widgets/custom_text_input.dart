import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomTextInput extends StatelessWidget {
  const CustomTextInput(
      {Key? key,
      this.controller,
      required this.label,
      this.isAutoFloatingLabel = true,
      this.inputType = TextInputType.text,
      this.prefixIcon,
      this.suffixIcon,
      this.isInputDigit = false,
      this.isHideText = false,
      this.onChanged,
      this.onTapSuffixIcon,
      this.maxLines = 1,
      this.readOnly = false,
      this.validator,
      this.onFieldSubmitted,
      this.autoValidateMode,
      this.initialValue})
      : super(key: key);

  final TextEditingController? controller;
  final String label;
  final bool isAutoFloatingLabel;
  final Icon? prefixIcon;
  final Icon? suffixIcon;
  final TextInputType inputType;
  final bool isInputDigit;
  final int maxLines;
  final bool isHideText;
  final ValueChanged<String>? onChanged;
  final GestureTapCallback? onTapSuffixIcon;
  final bool readOnly;
  final String? Function(String?)? validator;
  final Function(String)? onFieldSubmitted;
  final AutovalidateMode? autoValidateMode;
  final String? initialValue;

  @override
  Widget build(BuildContext context) {
    List<TextInputFormatter> digitOnly = [
      FilteringTextInputFormatter.digitsOnly
    ];

    return TextFormField(
      initialValue: initialValue,
      controller: controller,
      obscureText: isHideText,
      onFieldSubmitted: onFieldSubmitted,
      style: readOnly
          ? TextStyleValues.textRegular.copyWith(color: ColorValues.grey)
          : TextStyleValues.textRegular,
      readOnly: readOnly,
      decoration: InputDecoration(
        alignLabelWithHint: true,
        labelText: label,
        labelStyle: TextStyleValues.textRegular.copyWith(
          color: ColorValues.grey,
        ),
        errorStyle:
            TextStyleValues.textRegular.copyWith(color: ColorValues.red),
        floatingLabelBehavior: isAutoFloatingLabel
            ? FloatingLabelBehavior.auto
            : FloatingLabelBehavior.always,
        prefixIcon: prefixIcon != null
            ? Icon(
                prefixIcon!.icon,
                size: 18,
                color: ColorValues.grey,
              )
            : null,
        suffixIcon: suffixIcon != null
            ? InkWell(
                onTap: onTapSuffixIcon,
                child: Icon(
                  suffixIcon!.icon,
                  size: 18,
                  color: ColorValues.grey,
                ),
              )
            : null,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide:
              const BorderSide(color: ColorValues.bluePrimary, width: 1.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide:
              const BorderSide(color: ColorValues.greyLight, width: 1.5),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: const BorderSide(color: ColorValues.red),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: const BorderSide(color: ColorValues.red),
        ),
      ),
      maxLines: maxLines,
      keyboardType: inputType,
      onChanged: onChanged,
      inputFormatters: isInputDigit ? digitOnly : List.empty(),
      validator: validator,
      autovalidateMode: autoValidateMode ?? AutovalidateMode.onUserInteraction,
    );
  }
}
