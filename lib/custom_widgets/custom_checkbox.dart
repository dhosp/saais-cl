import 'package:flutter/material.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomCheckbox extends StatelessWidget {
  const CustomCheckbox({Key? key, this.value, this.onChanged, this.label})
      : super(key: key);

  final bool? value;
  final Function(bool?)? onChanged;
  final String? label;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
            value: value,
            onChanged: onChanged,
            activeColor: ColorValues.bluePrimary,
            side: const BorderSide(color: ColorValues.grey, width: 1.5)),
        // const SizedBox(
        //   width: 16,
        // ),
        Text(
          label ?? "",
          style: TextStyleValues.textMedium.copyWith(color: ColorValues.grey),
        )
      ],
    );
  }
}
