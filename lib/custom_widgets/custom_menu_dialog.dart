import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomMenuDialog {
  static showDialog(
      {required List<Widget> menus, bool isDismissable = true}) async {
    if (Get.isSnackbarOpen == true) {
      await Future.delayed(const Duration(milliseconds: 1500));
    }
    Get.dialog(
        AlertDialog(
          title: Center(
              child: Text("Tindakan",
                  style: TextStyleValues.textMedium
                      .copyWith(color: ColorValues.grey))),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(4))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: menus,
          ),
        ),
        barrierDismissible: isDismissable);
  }
}
