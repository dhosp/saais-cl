import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/values/color_values.dart';

class CustomModal {
  static showModal(
      {String confirmText = "Oke",
      String dismissText = "Batal",
      VoidCallback? confirmAction,
      VoidCallback? dismissAction,
      bool isDismissable = true,
      required Widget mainContent,
      Function? onInit}) async {
    if (onInit != null) {
      await onInit.call();
    }

    await Get.dialog(
        Dialog(
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(32),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: ColorValues.white,
              ),
              constraints: const BoxConstraints(maxWidth: 1000),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  mainContent,
                  const SizedBox(
                    height: 32,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Container(),
                      ),
                      Expanded(
                        child: CustomButton(
                          buttonType: "secondary",
                          title: dismissText,
                          action: () {
                            Get.back();
                            dismissAction?.call();
                          },
                        ),
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: CustomButton(
                          title: confirmText,
                          action: () {
                            confirmAction?.call();
                          },
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        barrierDismissible: isDismissable);
  }
}
