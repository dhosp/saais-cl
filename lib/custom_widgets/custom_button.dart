import 'package:flutter/material.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {Key? key, this.title, this.icon, this.buttonType = "", this.action})
      : super(key: key);

  final String? title;
  final IconData? icon;
  final String? buttonType;
  final VoidCallback? action;

  @override
  Widget build(BuildContext context) {
    Color colorText = ColorValues.white;
    Color colorButton = ColorValues.bluePrimary;
    Color colorOutline = ColorValues.bluePrimary;
    if (buttonType == "secondary") {
      colorText = ColorValues.bluePrimary;
      colorButton = ColorValues.white;
      colorOutline = ColorValues.bluePrimary;
    } else if (buttonType == "danger") {
      colorText = ColorValues.red;
      colorButton = ColorValues.white;
      colorOutline = ColorValues.red;
    }

    return InkWell(
      onTap: action,
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            color: colorButton,
            borderRadius: BorderRadius.circular(4.0),
            border: Border.all(color: colorOutline, width: 1)),
        child: Center(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buttonText(icon, title, colorText),
        )),
      ),
    );
  }

  List<Widget> _buttonText(icon, title, colorText) {
    List<Widget> widgets = [];
    if (icon != null && title != null) {
      widgets = [
        Icon(icon, color: colorText),
        const SizedBox(
          width: 4,
        ),
        Text(title,
            style: TextStyleValues.textMedium.copyWith(color: colorText))
      ];
    } else if (icon != null && title == null) {
      widgets = [
        Icon(icon, color: colorText),
      ];
    } else if (icon == null && title != null) {
      widgets = [
        Text(title,
            style: TextStyleValues.textMedium.copyWith(color: colorText))
      ];
    }

    return widgets;
  }
}
