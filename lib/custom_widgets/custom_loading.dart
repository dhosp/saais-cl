import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomLoading {
  static void showLoading() {
    Get.dialog(
        Material(
          color: Colors.transparent,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              decoration: BoxDecoration(
                  color: ColorValues.white,
                  borderRadius: BorderRadius.circular(8)),
              child: const Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CircularProgressIndicator(color: ColorValues.bluePrimary),
                  SizedBox(width: 16),
                  Text("Tunggu sebentar...", style: TextStyleValues.textMedium)
                ],
              ),
            ),
          ),
        ),
        barrierDismissible: false);
  }

  static void hideLoading() {
    if (Get.isDialogOpen == true) {
      Get.close(1);
    }
  }
}
