import 'package:flutter/material.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomAppBar extends PreferredSize {
  final String title;
  final Widget? leading;

  const CustomAppBar({
    Key? key,
    required this.title,
    this.leading,
  }) : super(
          key: key,
          child: const SizedBox.shrink(),
          preferredSize: const Size.fromHeight(kToolbarHeight),
        );

  @override
  Widget build(BuildContext context) {
    return AppBar(
        leadingWidth: 40,
        leading: leading,
        backgroundColor: ColorValues.bluePrimary,
        title: Text(
          title,
          style: TextStyleValues.textBold.copyWith(color: ColorValues.white),
        ));
  }
}
