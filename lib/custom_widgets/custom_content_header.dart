import 'package:flutter/material.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomContentHeader extends StatelessWidget {
  final String title;
  final List<Widget>? actions;
  final List<Widget>? filters;
  final Function() onRefresh;
  final int totalData;
  final int totalDisplayedData;

  const CustomContentHeader(
      {super.key,
      required this.title,
      this.actions,
      this.filters,
      required this.onRefresh,
      this.totalData = 0,
      this.totalDisplayedData = 0});

  @override
  Widget build(BuildContext context) {
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(),
      landscapeLayout: _landscapeLayout(),
      addPadding: false,
    );
  }

  Widget _potraitLayout() {
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: ColorValues.white,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [...actions!],
          ),
          const SizedBox(
            height: 16,
          ),
          ...filters!,
        ],
      ),
    );
  }

  Widget _landscapeLayout() {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      title,
                      style: TextStyleValues.textBold,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: actions ?? [],
                  )
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                children: filters ?? [],
              )
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              "Menampilkan maks. ${totalDisplayedData > totalData ? totalData : totalDisplayedData}  dari $totalData data",
              style:
                  TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
            ),
            const SizedBox(
              width: 8,
            ),
            InkWell(
              onTap: onRefresh,
              child: const Icon(
                Icons.refresh_rounded,
                color: ColorValues.bluePrimary,
              ),
            )
          ],
        )
      ],
    );
  }
}
