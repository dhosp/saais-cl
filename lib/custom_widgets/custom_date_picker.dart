import 'package:flutter/material.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

typedef DateTimeCallback = Function(DateTime date);

class CustomDatePicker extends StatelessWidget {
  const CustomDatePicker({
    Key? key,
    this.controller,
    required this.label,
    this.isAutoFloatingLabel = true,
    this.readOnly = false,
    this.validator,
    this.dateValueCallback,
    this.date,
    this.autoValidateMode,
    this.type = "both",
  }) : super(key: key);

  final TextEditingController? controller;
  final String label;
  final bool isAutoFloatingLabel;
  final bool readOnly;
  final String? Function(String?)? validator;
  final DateTimeCallback? dateValueCallback;
  final DateTime? date;
  final AutovalidateMode? autoValidateMode;
  final String type;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      style: readOnly
          ? TextStyleValues.textRegular.copyWith(color: ColorValues.grey)
          : TextStyleValues.textRegular,
      readOnly: readOnly,
      keyboardType: TextInputType.datetime,
      mouseCursor: SystemMouseCursors.click,
      showCursor: false,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        labelText: label,
        alignLabelWithHint: true,
        labelStyle:
            TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
        errorStyle:
            TextStyleValues.textRegular.copyWith(color: ColorValues.red),
        floatingLabelBehavior: isAutoFloatingLabel
            ? FloatingLabelBehavior.auto
            : FloatingLabelBehavior.always,
        suffixIcon: const Icon(
          Icons.calendar_month_rounded,
          size: 18,
          color: ColorValues.grey,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide:
              const BorderSide(color: ColorValues.bluePrimary, width: 1.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide:
              const BorderSide(color: ColorValues.greyLight, width: 1.5),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: const BorderSide(color: ColorValues.red),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: const BorderSide(color: ColorValues.red),
        ),
      ),
      validator: validator,
      autovalidateMode: autoValidateMode ?? AutovalidateMode.onUserInteraction,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
        _selectDate(context);
      },
    );
  }

  Future _selectDate(context) async {
    DateTime? selectedDate;
    TimeOfDay? selectedTime;

    if (["both", "date"].indexOf(type) >= 0) {
      selectedDate = await showDatePicker(
        context: context,
        initialDate: date != null ? date! : DateTime.now(),
        firstDate: DateTime(2000),
        lastDate: DateTime.now(),
      );
    } else {
      selectedDate = DateTime.now();
    }

    if (["both", "time"].indexOf(type) >= 0) {
      selectedTime = await showTimePicker(
        context: context,
        initialTime:
            TimeOfDay.fromDateTime(date != null ? date! : DateTime.now()),
      );

      if (selectedTime != null) {
        selectedDate = DateTime(selectedDate!.year, selectedDate.month,
            selectedDate.day, selectedTime.hour, selectedTime.minute);
      } else {
        selectedDate = null;
      }
    }

    if (selectedDate != null && controller != null) {
      controller!.text = Formatter.dateFormatter(
          date: selectedDate,
          type: type == "date"
              ? "display"
              : type == "time"
                  ? "displayTime"
                  : "displayDateTime");
      if (dateValueCallback != null) {
        dateValueCallback!(selectedDate);
      }
    }
  }
}
