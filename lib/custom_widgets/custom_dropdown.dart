import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomDropdown extends StatelessWidget {
  final String? hint;
  final Function(dynamic)? onChanged;
  final List<dynamic>? items;
  final dynamic selectedItem;
  final bool isAutoFloatingLabel;
  final String? Function(dynamic)? validator;
  final Future<List<dynamic>> Function(String)? asyncItems;
  final Icon? prefixIcon;
  final bool showSearchBox;
  final String Function(dynamic)? itemToString;
  final bool enabled;
  final bool isFilterOnline;
  final AutovalidateMode? autoValidateMode;

  const CustomDropdown(
      {Key? key,
      this.hint,
      this.onChanged,
      this.items,
      this.selectedItem,
      this.prefixIcon,
      this.isAutoFloatingLabel = true,
      this.validator,
      this.asyncItems,
      this.showSearchBox = false,
      this.itemToString,
      this.enabled = true,
      this.isFilterOnline = false,
      this.autoValidateMode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownSearch(
      onBeforePopupOpening: (selectedItem) async {
        return enabled;
      },
      asyncItems:
          asyncItems != null ? (String filter) => asyncItems!(filter) : null,
      itemAsString: itemToString != null ? (item) => itemToString!(item) : null,
      selectedItem: selectedItem,
      items: items ?? [],
      validator: validator,
      autoValidateMode: autoValidateMode ?? AutovalidateMode.onUserInteraction,
      onChanged: onChanged,
      popupProps: PopupProps.menu(
          scrollbarProps: const ScrollbarProps(thickness: 0),
          isFilterOnline: isFilterOnline,
          fit: FlexFit.loose,
          showSearchBox: showSearchBox,
          searchDelay: isFilterOnline
              ? const Duration(milliseconds: 500)
              : Duration.zero,
          searchFieldProps: TextFieldProps(
              decoration: InputDecoration(
            labelText: hint != null ? "Cari $hint" : "Cari data",
            alignLabelWithHint: true,
            labelStyle:
                TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
            errorStyle:
                TextStyleValues.textRegular.copyWith(color: ColorValues.red),
            floatingLabelBehavior: isAutoFloatingLabel
                ? FloatingLabelBehavior.auto
                : FloatingLabelBehavior.always,
            prefixIcon: prefixIcon != null
                ? Icon(
                    prefixIcon!.icon,
                    size: 18,
                    color: ColorValues.grey,
                  )
                : null,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide:
                  const BorderSide(color: ColorValues.bluePrimary, width: 1.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide:
                  const BorderSide(color: ColorValues.greyLight, width: 1.5),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: const BorderSide(color: ColorValues.red),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: const BorderSide(color: ColorValues.red),
            ),
          )),
          emptyBuilder: (context, searchEntry) =>
              const Center(child: Text("Data tidak ditemukan"))),
      dropdownDecoratorProps: DropDownDecoratorProps(
          baseStyle: TextStyleValues.textRegular
              .copyWith(color: enabled ? ColorValues.black : ColorValues.grey),
          dropdownSearchDecoration: InputDecoration(
            labelText: hint,
            alignLabelWithHint: true,
            labelStyle:
                TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
            errorStyle:
                TextStyleValues.textRegular.copyWith(color: ColorValues.red),
            floatingLabelBehavior: isAutoFloatingLabel
                ? FloatingLabelBehavior.auto
                : FloatingLabelBehavior.always,
            prefixIcon: prefixIcon != null
                ? Icon(
                    prefixIcon!.icon,
                    size: 18,
                    color: ColorValues.grey,
                  )
                : null,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide:
                  const BorderSide(color: ColorValues.bluePrimary, width: 1.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide:
                  const BorderSide(color: ColorValues.greyLight, width: 1.5),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: const BorderSide(color: ColorValues.red),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4.0),
              borderSide: const BorderSide(color: ColorValues.red),
            ),
          )),
    );
  }
}
