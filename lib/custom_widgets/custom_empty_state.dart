import 'package:flutter/material.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomEmptyState extends StatelessWidget {
  final VoidCallback? action;

  const CustomEmptyState({super.key, this.action});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Icon(
          Icons.widgets,
          color: ColorValues.grey,
          size: 100,
        ),
        const SizedBox(
          height: 16,
        ),
        Text(
          "Data masih kosong, yuk tambah data baru!",
          style: TextStyleValues.textMedium.copyWith(color: ColorValues.grey),
        ),
        Builder(builder: (BuildContext context) {
          if (action != null) {
            return Column(
              children: [
                const SizedBox(
                  height: 64,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomButton(
                      icon: Icons.add,
                      title: "Tambah Data",
                      action: action,
                    ),
                  ],
                ),
              ],
            );
          } else {
            return Container();
          }
        })
      ],
    );
  }
}
