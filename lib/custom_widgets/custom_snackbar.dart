import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomSnackBar {
  static void showSucessToast(String message) {
    Get.snackbar("", "",
        duration: const Duration(milliseconds: 1500),
        titleText: Container(),
        messageText: Center(
            child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.greenBackground),
                child: Text(
                  message,
                  style: TextStyleValues.textMedium
                      .copyWith(color: ColorValues.green),
                ))),
        backgroundColor: Colors.transparent,
        barBlur: 0,
        snackPosition: SnackPosition.TOP,
        margin: const EdgeInsets.all(4));
  }

  static void showErrorToast(String message) {
    Get.snackbar("", "",
        duration: const Duration(milliseconds: 1500),
        titleText: Container(),
        messageText: Center(
            child: Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.redBackground),
                child: Text(
                  message,
                  style: TextStyleValues.textMedium
                      .copyWith(color: ColorValues.red),
                ))),
        backgroundColor: Colors.transparent,
        barBlur: 0,
        snackPosition: SnackPosition.TOP,
        margin: const EdgeInsets.all(4));
  }
}
