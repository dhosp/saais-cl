import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/values/text_style_values.dart';

class CustomDialog {
  static showDialog(
      {required String title,
      required String message,
      String confirmText = "Oke",
      String dismissText = "Batal",
      VoidCallback? confirmAction,
      VoidCallback? dismissAction,
      bool isDismissable = true}) async {
    if (Get.isSnackbarOpen == true) {
      await Future.delayed(const Duration(milliseconds: 1500));
    }
    Get.dialog(
        AlertDialog(
          title: Center(child: Text(title, style: TextStyleValues.textBold)),
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(4))),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(message,
                  style: TextStyleValues.textMedium,
                  textAlign: TextAlign.center),
              const SizedBox(height: 32),
              Row(
                children: [
                  Flexible(
                      flex: 1,
                      child: CustomButton(
                        title: dismissText,
                        buttonType: "secondary",
                        action: () {
                          Get.back();
                          dismissAction?.call();
                        },
                      )),
                  const SizedBox(width: 16),
                  Flexible(
                      flex: 1,
                      child: CustomButton(
                        title: confirmText,
                        action: () {
                          confirmAction?.call();
                        },
                      ))
                ],
              )
            ],
          ),
        ),
        barrierDismissible: isDismissable);
  }
}
