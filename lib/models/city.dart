// ignore_for_file: non_constant_identifier_names

class City {
  int id;
  String name;
  String province_name;

  City({
    required this.id,
    required this.name,
    required this.province_name,
  });

  factory City.fromJson(Map<String, dynamic> json) => City(
        id: json["id"],
        name: json["name"],
        province_name: json["province_name"],
      );

  static String getDropdownLabel(City city) {
    return "${city.name} - ${city.province_name}";
  }
}
