// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/teacher_note.dart';

class TeacherNoteReport {
  TeacherNote? homeroom_teacher_note;

  TeacherNoteReport({this.homeroom_teacher_note});

  factory TeacherNoteReport.fromJson(Map<String, dynamic> json) {
    return TeacherNoteReport(
      homeroom_teacher_note:
          TeacherNote.fromJson(json["homeroom_teacher_note"]),
    );
  }
}
