class GradeEnum {
  static List<String> getDropdownItems({includeEmpty = false}) {
    List<String> grades = ["x", "xi", "xii"];
    if (includeEmpty) {
      grades.insert(0, "");
    }

    return grades;
  }

  static String getDropdownLabel(
      {required String grade, bool isDropdown = false}) {
    if (grade == "") {
      return isDropdown ? "Semua Tingkat" : "-";
    }
    return grade.toUpperCase();
  }
}
