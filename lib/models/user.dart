// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/competency_score.dart';
import 'package:saais_client/models/student_class.dart';
import 'package:saais_client/models/user_detail.dart';
import 'package:saais_client/models/user_role.dart';

class User {
  int id;
  String name;
  String username;
  String? password;
  bool is_reset;
  UserDetail? user_detail;
  List<UserRole> user_roles;
  List<CompetencyScore>? competency_scores;
  List<StudentClass>? student_classes;

  User(
      {required this.id,
      required this.name,
      this.username = "",
      this.password,
      this.is_reset = false,
      this.user_detail,
      required this.user_roles,
      this.competency_scores,
      this.student_classes});

  factory User.fromJson(Map<String, dynamic> json) {
    UserDetail? user_detail;
    List<UserRole> user_roles = [];
    List<CompetencyScore> competency_scores = [];
    List<StudentClass> student_classes = [];

    if (json["user_detail"] != null) {
      user_detail = UserDetail.fromJson(json["user_detail"]);
    }

    if (json["user_roles"] != null && json["user_roles"].length >= 0) {
      for (var item in json["user_roles"]) {
        user_roles.add(UserRole.fromJson(item));
      }
    }

    if (json["competency_scores"] != null) {
      for (var item in json["competency_scores"]) {
        competency_scores.add(CompetencyScore.fromJson(item));
      }
    }

    if (json["student_classes"] != null) {
      for (var item in json["student_classes"]) {
        student_classes.add(StudentClass.fromJson(item));
      }
    }

    return User(
        id: json["id"],
        name: json["name"],
        username: json["username"] ?? "",
        is_reset: json["is_reset"] ?? false,
        user_detail: user_detail,
        user_roles: user_roles,
        competency_scores: competency_scores,
        student_classes: student_classes);
  }

  Map<String, dynamic> toJson({withId = true}) {
    Map<String, dynamic> result = {
      "username": username,
      "name": name,
      "user_detail": user_detail?.toJson(),
    };

    if (withId) {
      result["id"] = id;
      result["is_reset"] = is_reset;
    }

    List<Map<String, dynamic>> roles = [];

    for (var item in user_roles) {
      roles.add(UserRole(code: item.code).toJson());
    }

    result["user_roles"] = roles;

    return result;
  }

  static String getDropdownLabel(User user) {
    if (user.username != "") {
      return "${user.username} - ${user.name}";
    } else {
      return user.name;
    }
  }
}
