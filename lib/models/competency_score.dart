// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/competency.dart';
import 'package:saais_client/models/user.dart';

class CompetencyScore {
  int id;
  int score;
  User? user;
  Competency? competency;

  CompetencyScore({this.id = 0, this.score = 0, this.user, this.competency});

  factory CompetencyScore.fromJson(Map<String, dynamic> json) {
    Competency? competency;
    User? user;

    if (json["competency"] != null) {
      competency = Competency.fromJson(json["competency"]);
    }

    if (json["user"] != null) {
      user = User.fromJson(json["user"]);
    }

    return CompetencyScore(
        id: json["id"],
        score: json["score"],
        user: user,
        competency: competency);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {"student_id": user!.id, "score": score};

    return result;
  }
}
