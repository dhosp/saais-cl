// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/school_bill_payment.dart';
import 'package:saais_client/models/school_period.dart';

class SchoolBill {
  int id;
  int month;
  num amount;
  num paid_amount;
  String status;
  String? note;
  SchoolPeriod? school_period;
  List<SchoolBillPayment>? school_bill_payments;

  SchoolBill(
      {this.id = 0,
      this.month = 0,
      this.amount = 0,
      this.paid_amount = 0,
      this.status = "due",
      this.note,
      this.school_period,
      this.school_bill_payments});

  factory SchoolBill.fromJson(Map<String, dynamic> json) {
    SchoolPeriod? school_period;
    List<SchoolBillPayment> school_bill_payments = [];

    if (json["school_period"] != null) {
      school_period = SchoolPeriod.fromJson(json["school_period"]);
    }

    if (json["school_bill_payments"] != null) {
      for (var item in json["school_bill_payments"]) {
        school_bill_payments.add(SchoolBillPayment.fromJson(item));
      }
    }

    return SchoolBill(
        id: json["id"],
        month: json["month"] ?? 0,
        paid_amount: num.tryParse(json["paid_amount"] ?? "") ?? 0,
        amount: num.tryParse(json["amount"] ?? "") ?? 0,
        status: json["status"] ?? "due",
        note: json["note"],
        school_period: school_period,
        school_bill_payments: school_bill_payments);
  }
}
