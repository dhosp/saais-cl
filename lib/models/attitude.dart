// ignore_for_file: non_constant_identifier_names

class Attitude {
  int id;
  String type;
  String description;

  Attitude({this.id = 0, this.description = "", this.type = ""});

  factory Attitude.fromJson(Map<String, dynamic> json) {
    return Attitude(
      id: json["id"],
      description: json["description"],
      type: json["type"],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "description": description,
      "type": type,
    };

    return result;
  }
}
