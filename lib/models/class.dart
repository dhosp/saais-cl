// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/student_class.dart';

class Class {
  int id;
  String name;
  String grade;
  int major_id;
  int? teacher_id;
  String? teacher_name;
  String? major_name;
  List<StudentClass>? student_classes;

  Class(
      {this.id = 0,
      this.name = "",
      this.grade = "",
      this.major_id = 0,
      this.teacher_id,
      this.teacher_name,
      this.major_name,
      this.student_classes});

  factory Class.fromJson(Map<String, dynamic> json) {
    List<StudentClass>? student_classes = [];

    if (json["student_classes"] != null &&
        json["student_classes"].length >= 0) {
      for (var item in json["student_classes"]) {
        student_classes.add(StudentClass.fromJson(item));
      }
    }

    return Class(
      id: json["id"],
      name: json["name"],
      grade: json["grade"],
      major_id: json["major_id"],
      teacher_id: json["teacher_id"],
      major_name: json["major_name"] ?? "",
      teacher_name: json["teacher_name"],
      student_classes: student_classes,
    );
  }

  static String getDropdownLabel(Class classroom) {
    if (classroom.id == 0) {
      return "Semua Kelas";
    } else {
      return "${classroom.grade.toUpperCase()} - ${classroom.major_id == 1 ? "" : classroom.major_name} ${classroom.name}";
    }
  }
}
