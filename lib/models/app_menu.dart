import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppMenu {
  Icon? icon;
  String? title;
  List<Tab>? tabs;
  List<GetView>? pages;
  TabController? tabController;

  AppMenu({this.icon, this.title, this.tabs, this.pages, this.tabController});
}
