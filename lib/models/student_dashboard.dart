// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/schedule.dart';
import 'package:saais_client/models/school_attendance.dart';
import 'package:saais_client/models/user.dart';

class StudentDashboard {
  SchoolAttendance? school_attendance;
  User? user;
  num skill_score_avg;
  num knowledge_score_avg;
  List<Schedule>? class_schedules;

  StudentDashboard(
      {this.school_attendance,
      this.user,
      this.skill_score_avg = 0,
      this.knowledge_score_avg = 0,
      this.class_schedules});

  factory StudentDashboard.fromJson(Map<String, dynamic> json) {
    SchoolAttendance? school_attendance;
    User? user;
    List<Schedule>? class_schedules = [];

    if (json["school_attendance"] != null) {
      school_attendance = SchoolAttendance.fromJson(json["school_attendance"]);
    }

    if (json["user"] != null) {
      user = User.fromJson(json["user"]);
    }

    if (json["class_schedules"] != null) {
      for (var item in json["class_schedules"]) {
        class_schedules.add(Schedule.fromJson(item));
      }
    }

    return StudentDashboard(
        skill_score_avg: num.tryParse(json["skill_score_avg"] ?? "") ?? 0,
        knowledge_score_avg:
            num.tryParse(json["knowledge_score_avg"] ?? "") ?? 0,
        school_attendance: school_attendance,
        class_schedules: class_schedules,
        user: user);
  }
}
