// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/attitude.dart';

class AttitudeScoreDetail {
  int id;
  String type;
  Attitude? attitude;

  AttitudeScoreDetail({this.id = 0, this.type = "", this.attitude});

  factory AttitudeScoreDetail.fromJson(Map<String, dynamic> json) {
    Attitude? attitude;

    if (json["attitude"] != null) {
      attitude = Attitude.fromJson(json["attitude"]);
    }

    return AttitudeScoreDetail(
        id: json["id"], type: json["type"], attitude: attitude);
  }
}
