// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/subject.dart';

class Schedule {
  int id;
  int day;
  String start_time;
  String finish_time;
  Subject? subject;

  Schedule(
      {this.id = 0,
      this.day = 0,
      this.start_time = "",
      this.finish_time = "",
      this.subject});

  factory Schedule.fromJson(Map<String, dynamic> json) {
    return Schedule(
        id: json["id"],
        day: json["day"],
        start_time: json["start_time"],
        finish_time: json["finish_time"],
        subject: Subject.fromJson(json["subject"]));
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "day": day,
      "start_time": start_time,
      "finish_time": finish_time,
    };

    if (subject != null) {
      result["subject_id"] = subject!.id;
    }

    return result;
  }

  static String getDayDropdownLabel(int day) {
    switch (day) {
      case 0:
        return "Minggu";
      case 1:
        return "Senin";
      case 2:
        return "Selasa";
      case 3:
        return "Rabu";
      case 4:
        return "Kamis";
      case 5:
        return "Jumat";
      case 6:
        return "Sabtu";
      default:
        return "";
    }
  }
}
