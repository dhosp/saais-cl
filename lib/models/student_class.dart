// ignore_for_file: non_constant_identifier_names

class StudentClass {
  int student_id;
  String? name;
  String? username;
  String? class_major_name;
  String? class_name;
  String? class_grade;

  StudentClass(
      {this.student_id = 0,
      this.name,
      this.username,
      this.class_major_name,
      this.class_name,
      this.class_grade});

  factory StudentClass.fromJson(Map<String, dynamic> json) {
    var result = StudentClass(
      student_id: json["student_id"] ?? 0,
      name: json["name"] ?? "",
      username: json["username"] ?? "",
      class_major_name: json["class_major_name"] ?? "",
      class_name: json["class_name"] ?? "",
      class_grade: json["class_grade"] ?? "",
    );

    return result;
  }
}
