// ignore_for_file: non_constant_identifier_names

class TeacherNote {
  int id;
  String description;

  TeacherNote({
    this.id = 0,
    this.description = "",
  });

  factory TeacherNote.fromJson(Map<String, dynamic> json) {
    return TeacherNote(
      id: json["id"],
      description: json["description"],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "description": description,
    };

    return result;
  }
}
