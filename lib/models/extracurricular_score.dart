// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/user.dart';

class ExtracurricularScore {
  int? id;
  String grade;
  User? user;

  ExtracurricularScore({this.id, this.grade = "", this.user});

  factory ExtracurricularScore.fromJson(Map<String, dynamic> json) {
    return ExtracurricularScore(
        id: json["id"],
        grade: json["grade"],
        user: User.fromJson(json["user"]));
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {"student_id": user!.id, "grade": grade};

    return result;
  }
}
