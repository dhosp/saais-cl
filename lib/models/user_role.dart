// ignore_for_file: non_constant_identifier_names

class UserRole {
  String code;

  UserRole({required this.code});

  factory UserRole.fromJson(Map<String, dynamic> json) => UserRole(
        code: json["code"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
      };
}
