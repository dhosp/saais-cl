// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/user.dart';

class Subject {
  int id;
  String name;
  String code;
  int class_id;
  String class_name;
  String class_grade;
  int class_major_id;
  String class_major_name;
  int pass_score;
  int teacher_id;
  String teacher_name;
  User? user;

  Subject({
    this.id = 0,
    this.name = "",
    this.code = "",
    this.class_id = 0,
    this.class_name = "",
    this.class_grade = "",
    this.class_major_id = 0,
    this.class_major_name = "",
    this.pass_score = 0,
    this.teacher_id = 0,
    this.teacher_name = "",
    this.user,
  });

  factory Subject.fromJson(Map<String, dynamic> json) {
    User? user;

    if (json["user"] != null) {
      user = User.fromJson(json["user"]);
    }

    return Subject(
      id: json["id"],
      name: json["name"],
      code: json["code"],
      class_id: json["class_id"] ?? 0,
      class_name: json["class_name"] ?? "",
      class_grade: json["class_grade"] ?? "",
      class_major_id: json["class_major_id"] ?? 0,
      class_major_name: json["class_major_name"] ?? "",
      pass_score: json["pass_score"],
      teacher_id: json["teacher_id"] ?? 0,
      teacher_name: json["teacher_name"] ?? "",
      user: user,
    );
  }

  static String getDropdownLabel(Subject subject) {
    return "${subject.code} - ${subject.name}";
  }
}
