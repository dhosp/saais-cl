class MajorEnum {
  static List<String> getDropdownItems({includeEmpty = false}) {
    List<String> major = ["1", "2", "3"];
    if (includeEmpty) {
      major.insert(0, "");
    }

    return major;
  }

  static String getDropdownLabel(
      {required String major, bool isDropdown = false}) {
    if (major == "") {
      return isDropdown ? "Semua Tingkat" : "-";
    } else if (major == "1") {
      return "Umum";
    } else if (major == "2") {
      return "IPA";
    } else if (major == "3") {
      return "IPS";
    } else {
      return "-";
    }
  }
}
