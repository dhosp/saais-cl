// ignore_for_file: non_constant_identifier_names

import 'package:intl/intl.dart';

class Month {
  static String getDropdownLabel(int month) {
    return DateFormat("MMM").format(
        DateTime.tryParse("2023-${month < 10 ? "0$month" : month}-01 00:00")!);
  }
}
