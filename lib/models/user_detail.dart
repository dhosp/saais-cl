// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/utils/formatter.dart';

class UserDetail {
  String? nisn;
  String? nik;
  String? address;
  int? place_of_birth_id;
  String? birth_city_name;
  String? birth_city_province_name;
  DateTime? date_of_birth;
  String? gender;
  String? religion;
  String? education;
  String? phone_num;
  String? status_in_family;
  int? order_in_family;
  String? origin_school;
  DateTime? join_date;
  String? join_class;
  String? father_name;
  String? father_occupation;
  String? mother_name;
  String? mother_occupation;
  String? parent_phone_num;
  String? parent_address;
  String? alumni_job;
  DateTime? graduated_at;
  num? bill_amount;
  int? bill_due_total;
  int? attendance_h_total;
  int? attendance_i_total;
  int? attendance_a_total;
  int? attendance_s_total;
  int? achievement_total;
  String? attitude_spiritual;
  String? attitude_sosial;

  UserDetail({
    this.nisn,
    this.nik,
    this.address,
    this.place_of_birth_id,
    this.birth_city_name,
    this.birth_city_province_name,
    this.date_of_birth,
    this.gender,
    this.religion,
    this.education,
    this.phone_num,
    this.status_in_family,
    this.order_in_family,
    this.origin_school,
    this.join_date,
    this.join_class,
    this.father_name,
    this.father_occupation,
    this.mother_name,
    this.mother_occupation,
    this.parent_phone_num,
    this.parent_address,
    this.alumni_job,
    this.graduated_at,
    this.bill_amount,
    this.bill_due_total,
    this.attendance_h_total,
    this.attendance_i_total,
    this.attendance_a_total,
    this.attendance_s_total,
    this.achievement_total,
    this.attitude_sosial,
    this.attitude_spiritual,
  });

  factory UserDetail.fromJson(Map<String, dynamic> json) {
    return UserDetail(
      nisn: json["nisn"],
      nik: json["nik"],
      address: json["address"] ?? "",
      place_of_birth_id: json["place_of_birth_id"],
      birth_city_name: json["birth_city_name"] ?? "",
      birth_city_province_name: json["birth_city_province_name"] ?? "",
      date_of_birth: DateTime.tryParse(json["date_of_birth"] ?? ""),
      gender: json["gender"] ?? "",
      religion: json["religion"] ?? "",
      education: json["education"] ?? "",
      phone_num: json["phone_num"] ?? "",
      status_in_family: json["status_in_family"] ?? "",
      order_in_family: int.tryParse(json["order_in_family"].toString()),
      origin_school: json["origin_school"] ?? "",
      join_date: DateTime.tryParse(json["join_date"] ?? ""),
      join_class: json["join_class"] ?? "",
      father_name: json["father_name"] ?? "",
      father_occupation: json["father_occupation"] ?? "",
      mother_name: json["mother_name"] ?? "",
      mother_occupation: json["mother_occupation"] ?? "",
      parent_phone_num: json["parent_phone_num"] ?? "",
      parent_address: json["parent_address"] ?? "",
      alumni_job: json["alumni_job"] ?? "",
      graduated_at: DateTime.tryParse(
        json["graduated_at"] ?? "",
      ),
      bill_amount: num.tryParse(json["bill_amount"] ?? ""),
      bill_due_total: json["bill_due_total"],
      attendance_h_total: json["attendance_h_total"],
      attendance_i_total: json["attendance_i_total"],
      attendance_a_total: json["attendance_a_total"],
      attendance_s_total: json["attendance_s_total"],
      achievement_total: json["achievement_total"],
      attitude_spiritual: json["attitude_spiritual"],
      attitude_sosial: json["attitude_sosial"],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "nisn": nisn ?? "",
      "nik": nik ?? "",
      "address": address ?? "",
      "place_of_birth_id": place_of_birth_id ?? "",
      "date_of_birth": date_of_birth != null
          ? Formatter.dateFormatter(
              date: date_of_birth!,
            )
          : "",
      "gender": gender ?? "",
      "religion": religion ?? "",
      "education": education ?? "",
      "phone_num": phone_num ?? "",
      "status_in_family": status_in_family ?? "",
      "order_in_family": order_in_family ?? "",
      "origin_school": origin_school ?? "",
      "join_date": join_date != null
          ? Formatter.dateFormatter(
              date: join_date!,
            )
          : "",
      "join_class": join_class ?? "",
      "father_name": father_name ?? "",
      "father_occupation": father_occupation ?? "",
      "mother_name": mother_name ?? "",
      "mother_occupation": mother_occupation ?? "",
      "parent_phone_num": parent_phone_num ?? "",
      "parent_address": parent_address ?? "",
      "alumni_job": alumni_job ?? "",
      "graduated_at": graduated_at != null
          ? Formatter.dateFormatter(
              date: graduated_at!,
            )
          : "",
    };

    return result;
  }
}
