// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/user.dart';

class Extracurricular {
  int id;
  String name;
  User? user;

  Extracurricular({this.id = 0, this.name = "", this.user});

  factory Extracurricular.fromJson(Map<String, dynamic> json) {
    User? user;

    if (json["user"] != null) {
      user = User.fromJson(json["user"]);
    }

    return Extracurricular(id: json["id"], name: json["name"], user: user);
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "name": name,
      "coach_id": user?.id ?? "",
    };

    return result;
  }

  static String getDropdownLabel(Extracurricular extracurricular) {
    return extracurricular.name;
  }
}
