class GenderEnum {
  static List<String> getDropdownItems({includeEmpty = false}) {
    List<String> gender = [
      "male",
      "female",
    ];
    if (includeEmpty) {
      gender.insert(0, "");
    }

    return gender;
  }

  static String getDropdownLabel(
      {required String gender, bool isDropdown = false}) {
    if (gender == "") {
      return isDropdown ? "Semua Jenis Kelamin" : "-";
    } else if (gender == "male") {
      return "Laki-laki";
    } else if (gender == "female") {
      return "Perempuan";
    } else {
      return "-";
    }
  }
}
