// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/class_attendance.dart';
import 'package:saais_client/models/student_class.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';

class ClassSession {
  int id;
  String lesson_note;
  DateTime? started_at;
  DateTime? finished_at;
  Subject? subject;
  User? user;
  List<ClassAttendance> class_attendences;

  ClassSession(
      {required this.id,
      this.lesson_note = "",
      this.started_at,
      this.finished_at,
      this.subject,
      this.user,
      required this.class_attendences});

  factory ClassSession.fromJson(Map<String, dynamic> json) {
    List<ClassAttendance> class_attendences = [];

    if (json["class_attendances"] != null) {
      for (var item in json["class_attendances"]) {
        class_attendences.add(ClassAttendance.fromJson(item));
      }
    }

    return ClassSession(
        id: json["id"],
        lesson_note: json["lesson_note"],
        started_at: DateTime.tryParse(json["started_at"]),
        finished_at: DateTime.tryParse(json["finished_at"]),
        subject: Subject.fromJson(json["subject"]),
        user: User.fromJson(json["user"]),
        class_attendences: class_attendences);
  }
}
