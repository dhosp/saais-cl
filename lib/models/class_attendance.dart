// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/student_class.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';

class ClassAttendance {
  int id;
  int student_id;
  User? user;

  ClassAttendance({this.id = 0, this.student_id = 0, this.user});

  factory ClassAttendance.fromJson(Map<String, dynamic> json) {
    return ClassAttendance(
        id: json["id"],
        student_id: json["student_id"],
        user: User.fromJson(json["user"]));
  }
}
