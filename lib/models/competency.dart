// ignore_for_file: non_constant_identifier_names

class Competency {
  int id;
  String type;
  String code;
  String description;

  Competency(
      {this.id = 0, this.type = "", this.code = "", this.description = ""});

  factory Competency.fromJson(Map<String, dynamic> json) {
    return Competency(
      id: json["id"],
      type: json["type"],
      code: json["code"],
      description: json["description"],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "type": type,
      "code": code,
      "description": description,
    };

    return result;
  }

  static String getDropdownLabel(Competency competency) {
    return competency.code;
  }
}
