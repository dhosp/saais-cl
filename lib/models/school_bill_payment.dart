// ignore_for_file: non_constant_identifier_names


class SchoolBillPayment {
  int id;
  num amount;
  DateTime paid_at;
  String? operator_name;

  SchoolBillPayment({
    this.id = 0,
    this.amount = 0,
    required this.paid_at,
    this.operator_name,
  });

  factory SchoolBillPayment.fromJson(Map<String, dynamic> json) {
    return SchoolBillPayment(
      id: json["id"],
      amount: num.tryParse(json["amount"] ?? "") ?? 0,
      paid_at: DateTime.tryParse(
            json["paid_at"] ?? "",
          ) ??
          DateTime.now(),
      operator_name: json["operator_name"],
    );
  }
}
