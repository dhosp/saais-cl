// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/user.dart';

class Achievement {
  int id;
  String type;
  String description;
  User? user;

  Achievement({this.id = 0, this.type = "", this.description = "", this.user});

  factory Achievement.fromJson(Map<String, dynamic> json) {
    return Achievement(
      id: json["id"],
      type: json["type"],
      description: json["description"],
    );
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = {
      "type": type,
      "description": description,
      "user_id": user?.id
    };

    return result;
  }
}
