// ignore_for_file: non_constant_identifier_names

class SchoolPeriod {
  int id;
  int start_year;
  int end_year;
  String semester;

  SchoolPeriod({
    this.id = 0,
    this.start_year = 0,
    this.end_year = 0,
    this.semester = "",
  });

  factory SchoolPeriod.fromJson(Map<String, dynamic> json) => SchoolPeriod(
        id: json["id"],
        start_year: json["start_year"],
        end_year: json["end_year"],
        semester: json["semester"],
      );
}
