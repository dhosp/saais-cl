// ignore_for_file: non_constant_identifier_names

import 'package:saais_client/models/attitude_score_detail.dart';

class AttitudeScore {
  int id;
  String type;
  String grade;
  List<AttitudeScoreDetail>? attitude_score_details;

  AttitudeScore(
      {this.id = 0,
      this.type = "",
      this.grade = "",
      this.attitude_score_details});

  factory AttitudeScore.fromJson(Map<String, dynamic> json) {
    List<AttitudeScoreDetail> attitude_score_details = [];
    if (json["attitude_score_details"] != null) {
      for (var item in json["attitude_score_details"]) {
        attitude_score_details.add(AttitudeScoreDetail.fromJson(item));
      }
    }

    return AttitudeScore(
        id: json["id"],
        type: json["type"],
        grade: json["grade"],
        attitude_score_details: attitude_score_details);
  }
}
