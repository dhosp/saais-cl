// ignore_for_file: non_constant_identifier_names

class SchoolAttendance {
  int id;
  DateTime? clock_in_date;
  DateTime? clock_in_at;
  DateTime? clock_out_at;
  String type;
  String? reason;

  SchoolAttendance(
      {this.id = 0,
      this.clock_in_date,
      this.clock_in_at,
      this.clock_out_at,
      this.type = "",
      this.reason});

  factory SchoolAttendance.fromJson(Map<String, dynamic> json) {
    return SchoolAttendance(
      id: json["id"],
      clock_in_date: DateTime.tryParse(
        json["clock_in_date"],
      ),
      clock_in_at: DateTime.tryParse(
        json["clock_in_at"],
      ),
      clock_out_at: DateTime.tryParse(
        json["clock_out_at"] ?? "",
      ),
      type: json["type"],
      reason: json["reason"],
    );
  }
}
