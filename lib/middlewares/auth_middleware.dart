import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/home/home_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/user_data.dart';

class AuthMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    String returnUrl = Uri.encodeFull(route ?? '');
    var targetRoute = returnUrl.split("?")[0];

    var token = GetStorage().read("token");

    if (targetRoute == "/login") {
      if (token != null) {
        return const RouteSettings(name: Routes.home);
      } else {
        return null;
      }
    } else {
      if (token != null) {
        User user = UserData.getUser();

        var isAttendance = false;
        var isStudent = false;

        for (var item in user.user_roles) {
          if (item.code == "attendance_system") {
            isAttendance = true;
            break;
          } else if (item.code == "student") {
            isStudent = true;
            break;
          }
        }

        var allowedStudentRoutes = [
          Routes.home,
          Routes.profile,
          Routes.resetPass
        ];

        if (isAttendance && targetRoute != Routes.realtimeAttendance) {
          return const RouteSettings(name: Routes.realtimeAttendance);
        } else if (isStudent && !allowedStudentRoutes.contains(targetRoute)) {
          return const RouteSettings(
            name: Routes.home,
          );
        } else {
          if (targetRoute == Routes.home) {
            var menu = int.tryParse(Get.parameters["menu"] ?? "") ?? 0;
            if (menu > 0) {
              HomeController.activeMenu.value = menu;
              var tab = int.tryParse(Get.parameters["tab"] ?? "") ?? 0;
              if (tab > 0) {
                HomeController.activeTab.value = tab;
              }
            }
          }

          return null;
        }
      } else {
        return const RouteSettings(name: Routes.login);
      }
    }
    return null;
  }
}
