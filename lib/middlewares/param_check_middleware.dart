import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ParamCheckMiddleware extends GetMiddleware {
  final String? paramToCheck;

  ParamCheckMiddleware({this.paramToCheck});

  @override
  RouteSettings? redirect(String? route) {
    if (paramToCheck != null) {
      if (Get.parameters[paramToCheck] == null) {
        return const RouteSettings(name: "/");
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}
