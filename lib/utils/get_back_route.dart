import 'package:get/get.dart';
import 'package:saais_client/routes/app_pages.dart';

class GetBackRoute {
  static void getBack({String? namedRoute, Map<String, String>? parameters}) {
    if (namedRoute != null) {
      Get.offNamed(namedRoute, parameters: parameters);
    } else if (Get.previousRoute != "" ||
        Get.previousRoute == Get.currentRoute) {
      Get.close(1);
      // Get.back();
    } else {
      Get.offNamed(Routes.home);
    }
  }
}
