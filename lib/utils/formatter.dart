import 'package:intl/intl.dart';
import 'package:saais_client/values/constant_values.dart';

class Formatter {
  static String dateFormatter({required DateTime date, String? type}) {
    if (type == "display") {
      return DateFormat(ConstantValues.displayDateFormat, 'id').format(date);
    } else if (type == "displayDateTime") {
      return DateFormat(ConstantValues.displayDateTimeFormat, 'id')
          .format(date);
    } else if (type == "displayTime") {
      return DateFormat(ConstantValues.displayTimeFormat, 'id').format(date);
    } else {
      return DateFormat(ConstantValues.apiDateFormat).format(date);
    }
  }

  static String numberFormatter({required dynamic number}) {
    return NumberFormat.currency(
            symbol: "Rp ", decimalDigits: 0, locale: "id_ID")
        .format(num.tryParse(number.toString()));
  }
}
