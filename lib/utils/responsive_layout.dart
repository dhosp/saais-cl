import 'package:flutter/material.dart';
import 'package:saais_client/custom_widgets/custom_app_bar.dart';
import 'package:saais_client/utils/get_back_route.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/constant_values.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget potraitLayout;
  final Widget landscapeLayout;
  final bool addPadding; //set false for layout that already have padding
  final bool
      includeAppBar; //set true for layout that need app bar with back action
  final String appBarTitle;
  final bool greySpace; //set false for potrait layout without grey background
  final bool? includeScroll;
  final Function? onRefresh;

  const ResponsiveLayout(
      {super.key,
      required this.potraitLayout,
      required this.landscapeLayout,
      this.addPadding = true,
      this.includeAppBar = false,
      this.appBarTitle = "",
      this.greySpace = true,
      this.includeScroll,
      this.onRefresh});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      if (includeAppBar) {
        return SafeArea(
          child: Scaffold(
              appBar: CustomAppBar(
                title: appBarTitle,
                leading: BackButton(onPressed: () {
                  GetBackRoute.getBack();
                }),
              ),
              body: (includeScroll ?? true)
                  ? onRefresh != null
                      ? RefreshIndicator(
                          onRefresh: () async {
                            onRefresh!.call();
                          },
                          child: SingleChildScrollView(
                              physics: const AlwaysScrollableScrollPhysics(),
                              child: MediaQuery.of(context).size.width <=
                                      ConstantValues.maxPotraitWidth
                                  ? _potraitAdjusted(constraints, potraitLayout)
                                  : _landscapeAdjusted(
                                      constraints, landscapeLayout)),
                        )
                      : SingleChildScrollView(
                          child: MediaQuery.of(context).size.width <=
                                  ConstantValues.maxPotraitWidth
                              ? _potraitAdjusted(constraints, potraitLayout)
                              : _landscapeAdjusted(
                                  constraints, landscapeLayout))
                  : MediaQuery.of(context).size.width <=
                          ConstantValues.maxPotraitWidth
                      ? _potraitAdjusted(constraints, potraitLayout)
                      : _landscapeAdjusted(constraints, landscapeLayout)),
        );
      } else {
        return SafeArea(
          child: (includeScroll ?? false)
              ? onRefresh != null
                  ? RefreshIndicator(
                      onRefresh: () async {
                        onRefresh!.call();
                      },
                      child: SingleChildScrollView(
                          physics: const AlwaysScrollableScrollPhysics(),
                          child: MediaQuery.of(context).size.width <=
                                  ConstantValues.maxPotraitWidth
                              ? _potraitAdjusted(constraints, potraitLayout)
                              : _landscapeAdjusted(
                                  constraints, landscapeLayout)),
                    )
                  : SingleChildScrollView(
                      child: MediaQuery.of(context).size.width <=
                              ConstantValues.maxPotraitWidth
                          ? _potraitAdjusted(constraints, potraitLayout)
                          : _landscapeAdjusted(constraints, landscapeLayout))
              : MediaQuery.of(context).size.width <=
                      ConstantValues.maxPotraitWidth
                  ? _potraitAdjusted(constraints, potraitLayout)
                  : _landscapeAdjusted(constraints, landscapeLayout),
        );
      }
    });
  }

  Widget _landscapeAdjusted(
      BoxConstraints constraints, Widget landscapeLayout) {
    if (addPadding) {
      return Container(
        padding: const EdgeInsets.fromLTRB(32, 32, 32, 32),
        color: ColorValues.greyBackground,
        child: Center(
          child: Container(
              constraints: BoxConstraints(
                  minHeight: constraints.maxHeight -
                      (includeScroll == true
                          ? kToolbarHeight + 16
                          : kToolbarHeight - 32 - 32)),
              child: landscapeLayout),
        ),
      );
    } else {
      return landscapeLayout;
    }
  }

  Widget _potraitAdjusted(BoxConstraints constraints, Widget potraitLayout) {
    if (addPadding) {
      return Container(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
        color: greySpace ? ColorValues.greyBackground : ColorValues.white,
        child: Center(
          child: Container(
              constraints: BoxConstraints(
                  minHeight: constraints.maxHeight -
                      (includeScroll == true
                          ? kToolbarHeight - 24
                          : kToolbarHeight - 16 - 16)),
              child: potraitLayout),
        ),
      );
    } else {
      return potraitLayout;
    }
  }
}
