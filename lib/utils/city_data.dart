import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/apis/city_api.dart';
import 'package:saais_client/models/city.dart';

class CityData {
  final _cityAPI = Get.find<CityAPI>();

  Future<List<dynamic>> getCityDropdown(String? filter) async {
    var localData = await GetStorage().read("cities");
    var result = [];

    if (localData != null && localData?.length > 0) {
      result = localData;
    } else {
      var findAllResult = await _cityAPI.findAll();
      if (findAllResult.body?["status"] ?? false) {
        result = findAllResult.body["data"];
        await GetStorage().write("cities", result);
      }
    }

    List<City> cities = [];

    for (var item in result) {
      cities.add(City.fromJson(item));
    }

    return cities;
  }
}
