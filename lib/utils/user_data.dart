import 'package:get_storage/get_storage.dart';
import 'package:saais_client/models/user.dart';

class UserData {
  static User getUser() {
    return User.fromJson(GetStorage().read("user"));
  }
}
