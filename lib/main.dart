import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:flutter_web_plugins/url_strategy.dart';

void main() {
  GetStorage.init();
  usePathUrlStrategy();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Saais',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: ColorValues.bluePrimary),
      ),
      getPages: AppPages.routes,
      initialRoute: Routes.splash,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [Locale('id')],
    );
  }
}
