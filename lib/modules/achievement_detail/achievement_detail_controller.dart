import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/achievement_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/achievement.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class AchievementDetailController extends GetxController {
  final _achievementAPI = Get.find<AchievementAPI>();
  final _userAPI = Get.find<UserAPI>();

  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var isAddNew = false;
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;

  final descController = TextEditingController(text: "");
  List<Achievement> achievements = List<Achievement>.empty(growable: true).obs;
  var user = User(id: 0, name: "", username: "", user_roles: []).obs;
  var type = "academic".obs;
  var selectedId = 0.obs;

  void beforeStart() {
    if (_userId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _userAPI.findOne(_userId);
      if (result.body?["status"] ?? false) {
        user.value = User.fromJson(result.body["data"]);
      }
    } else {
      user.value = Get.arguments;
    }

    getAchievements(type: type.value, user_id: user.value.id);
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result;
    if (isAddNew) {
      result = await _achievementAPI.create(
          achievement: Achievement(
              type: type.value,
              description: descController.text,
              user: user.value));
    } else {
      result = await _achievementAPI.update(
          achievement: Achievement(
              id: selectedId.value,
              type: type.value,
              description: descController.text,
              user: user.value));
    }
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      selectedId.value = 0;
      descController.text = "";
      getAchievements(type: type.value, user_id: user.value.id);
    }
  }

  void removeData(int achievementId) async {
    CustomLoading.showLoading();
    Response result = await _achievementAPI.remove(achievementId);

    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      getAchievements(type: type.value, user_id: user.value.id);
    }
  }

  void getAchievements(
      {required String type, required int user_id, String? filter}) async {
    var findAllResult = await _achievementAPI.findAll(
        perPage: -1, type: type, search: filter ?? "", user_id: user_id);

    if (findAllResult.body?["status"] ?? false) {
      totalData.value = findAllResult.body["total"];
      List<dynamic> resultData = findAllResult.body["data"];
      achievements.clear();
      for (var item in resultData) {
        achievements.add(Achievement.fromJson(item));
      }
    }
  }
}
