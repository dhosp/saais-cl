import 'package:get/get.dart';
import 'package:saais_client/apis/achievement_api.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/extracurricular_score_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/achievement_detail/achievement_detail_controller.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_controller.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';

class AchievementDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AchievementAPI());
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => AchievementDetailController());
  }
}
