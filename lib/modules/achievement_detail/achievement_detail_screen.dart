import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/achievement_detail/achievement_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class AchievementDetailScreen extends GetView<AchievementDetailController> {
  const AchievementDetailScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(),
      landscapeLayout: _landscapeLayout(),
      includeAppBar: true,
      greySpace: false,
      includeScroll: false,
    );
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    final GlobalKey<FormState> modalFormKey = GlobalKey<FormState>();
    return Container(
        constraints: const BoxConstraints(maxWidth: 1000),
        child: Column(children: [
          Obx(
            () => CustomContentHeader(
              title: "Daftar Prestasi ${controller.user.value.name}",
              totalData: controller.totalData.value,
              totalDisplayedData: controller.displayedData,
              onRefresh: () {
                controller.getAchievements(
                    type: controller.type.value,
                    user_id: controller.user.value.id);
              },
              actions: [
                const CustomButton(
                  icon: Icons.folder_open,
                  buttonType: "secondary",
                ),
                const SizedBox(
                  width: 16,
                ),
                const CustomButton(
                  icon: Icons.file_download_outlined,
                  buttonType: "secondary",
                ),
                const SizedBox(
                  width: 16,
                ),
                CustomButton(
                  icon: Icons.add,
                  title: "Tambah",
                  buttonType: "secondary",
                  action: () => {
                    CustomModal.showModal(
                        onInit: () {
                          controller.isAddNew = true;
                        },
                        mainContent: _addAttendanceModalContent(modalFormKey),
                        confirmAction: () {
                          if (modalFormKey.currentState!.validate()) {
                            controller.saveData();
                            controller.isAddNew = false;
                          }
                        },
                        dismissAction: () {
                          controller.descController.text = "";
                          controller.selectedId.value = 0;
                          controller.isAddNew = false;
                        })
                  },
                )
              ],
              filters: [
                Expanded(
                  child: CustomDropdown(
                    hint: "Tipe Prestasi",
                    items: const ["academic", "nonacademic"],
                    itemToString: (value) {
                      switch (value) {
                        case "academic":
                          return "Akademik";
                        case "nonacademic":
                          return "Nonakademik";
                        default:
                          return "";
                      }
                    },
                    onChanged: (value) {
                      controller.type.value = value;
                      controller.getAchievements(
                          type: value, user_id: controller.user.value.id);
                    },
                    selectedItem: controller.type.value,
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                const Expanded(
                  child: CustomTextInput(
                    label: "Cari Prestasi",
                    suffixIcon: Icon(Icons.search),
                  ),
                ),
              ],
            ),
          ),
          Obx(
            () => Expanded(
              child: controller.totalData > 0
                  ? Column(children: [
                      Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Tipe Prestasi",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Deskripsi",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            const Icon(Icons.more_vert,
                                color: Colors.transparent),
                            const SizedBox(
                              width: 16,
                            ),
                            const Icon(Icons.more_vert,
                                color: Colors.transparent)
                          ],
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: controller.achievements.length,
                          itemBuilder: (context, i) {
                            var achievement = controller.achievements[i];
                            return Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: ColorValues.white,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                            achievement.type == "academic"
                                                ? "Akademik"
                                                : achievement.type ==
                                                        "nonacademic"
                                                    ? "Nonakademik"
                                                    : "-",
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            achievement.description.length > 40
                                                ? achievement.description
                                                    .substring(0, 40)
                                                : achievement.description,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          CustomModal.showModal(
                                              onInit: () {
                                                controller.selectedId.value =
                                                    achievement.id;
                                                controller.descController.text =
                                                    achievement.description;
                                                controller.type.value =
                                                    achievement.type;
                                              },
                                              mainContent:
                                                  _addAttendanceModalContent(
                                                      modalFormKey),
                                              confirmAction: () {
                                                if (modalFormKey.currentState!
                                                    .validate()) {
                                                  controller.saveData();
                                                }
                                              },
                                              dismissAction: () {
                                                controller.descController.text =
                                                    "";
                                                controller.selectedId.value = 0;
                                              });
                                        },
                                        child: const Icon(Icons.edit_rounded,
                                            color: ColorValues.bluePrimary),
                                      ),
                                      const SizedBox(
                                        width: 16,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          CustomDialog.showDialog(
                                              title:
                                                  "Konfirmasi Hapus Prestasi",
                                              message:
                                                  "Kamu akan menghapus data prestasi. Apakah kamu yakin?",
                                              confirmText: "Ya, yakin",
                                              confirmAction: () {
                                                controller
                                                    .removeData(achievement.id);
                                              });
                                        },
                                        child: const Icon(
                                            Icons.delete_forever_rounded,
                                            color: ColorValues.red),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ])
                  : const CustomEmptyState(),
            ),
          ),
        ]));
  }

  Widget _addAttendanceModalContent(modalFormKey) {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Detail Prestasi",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Form(
          key: modalFormKey,
          child: Column(
            children: [
              CustomDropdown(
                hint: "Tipe Prestasi",
                items: const ["academic", "nonacademic"],
                itemToString: (value) {
                  switch (value) {
                    case "academic":
                      return "Akademik";
                    case "nonacademic":
                      return "Nonakademik";
                    default:
                      return "";
                  }
                },
                validator: (value) {
                  if (value == null) {
                    return "Wajib diisi";
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  controller.type.value = value;
                  controller.getAchievements(
                      type: value, user_id: controller.user.value.id);
                },
                selectedItem: controller.type.value,
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextInput(
                controller: controller.descController,
                label: "Deskripsi",
                maxLines: 3,
                validator: (value) {
                  value ??= "";
                  if (value == "") {
                    return "Wajib diisi";
                  } else {
                    return null;
                  }
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
