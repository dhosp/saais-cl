import 'package:get/get.dart';
import 'package:saais_client/apis/attitude_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/attitude_detail/attitude_detail_controller.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_controller.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';

class AttitudeDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AttitudeAPI());
    Get.lazyPut(() => AttitudeDetailController());
  }
}
