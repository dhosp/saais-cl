import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/attitude_api.dart';
import 'package:saais_client/models/attitude.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class AttitudeDetailController extends GetxController {
  final _attitudeAPI = Get.find<AttitudeAPI>();

  final _attitudeId =
      int.tryParse(Get.parameters["attitudeId"].toString()) ?? 0;
  var isAddNew = false;
  var attitude = Attitude().obs;

  final descController = TextEditingController(text: "");
  var type = "spiritual".obs;

  void beforeStart() {
    if (_attitudeId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _attitudeAPI.findOne(_attitudeId);
      if (result.body?["status"] ?? false) {
        attitude.value = Attitude.fromJson(result.body["data"]);
      }
    } else {
      attitude.value = Get.arguments;
    }

    type.value = attitude.value.type;
    descController.text = attitude.value.description;
  }

  void saveData() async {
    Response result;
    if (isAddNew) {
      result = await _attitudeAPI.create(
          attitude: Attitude(
        type: type.value,
        description: descController.text,
      ));
    } else {
      result = await _attitudeAPI.update(
          id: _attitudeId,
          attitude:
              Attitude(type: type.value, description: descController.text));
    }

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "3", "tab": "2"});
    }
  }
}
