import 'package:get/get.dart';
import 'package:saais_client/apis/school_attendance_api.dart';
import 'package:saais_client/modules/realtime_attendance/realtime_attendance_controller.dart';

class RealtimeAttendanceBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SchoolAttendanceAPI());
    Get.lazyPut(() => RealtimeAttendanceController());
  }
}
