import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_snackbar.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/realtime_attendance/realtime_attendance_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class RealtimeAttendanceScreen extends GetView<RealtimeAttendanceController> {
  const RealtimeAttendanceScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return Scaffold(
      body: ResponsiveLayout(
        potraitLayout: _mainContent(context),
        landscapeLayout: _mainContent(context),
        addPadding: false,
        includeScroll: false,
      ),
    );
  }

  Widget _mainContent(context) {
    return Obx(
      () => Container(
          color: ColorValues.white,
          padding: const EdgeInsets.all(16),
          child: controller.isSuccess.value == true
              ? _successContent()
              : controller.isManual.value == true
                  ? _manualContent()
                  : _scanQRContent(context)),
    );
  }

  Widget _scanQRContent(context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: CustomDropdown(
                items: const ["clock_in", "clock_out"],
                itemToString: (item) {
                  if (item == "clock_in") {
                    return "Presensi Masuk";
                  } else {
                    return "Presensi Pulang";
                  }
                },
                selectedItem: controller.type.value,
                onChanged: (value) {
                  controller.type.value = value;
                },
              ),
            ),
            Expanded(child: Container()),
            InkWell(
              child: const Icon(
                Icons.logout_rounded,
                color: ColorValues.grey,
              ),
              onTap: () {
                GetStorage().erase();
                Get.offAllNamed(Routes.login);
              },
            )
          ],
        ),
        Expanded(child: Container()),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Selamat Datang di Saais",
              style: TextStyleValues.textUltraBold,
            ),
            Obx(
              () => Text(
                Formatter.dateFormatter(
                    date: controller.currentTime.value, type: "displayTime"),
                style: TextStyleValues.textUltraBold
                    .copyWith(color: ColorValues.bluePrimary),
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Smaisga Academic Information System",
              style:
                  TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
            ),
            Obx(
              () => Text(
                Formatter.dateFormatter(
                    date: controller.currentTime.value, type: "display"),
                style: TextStyleValues.textRegular
                    .copyWith(color: ColorValues.grey),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 64,
        ),
        GetPlatform.isAndroid
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 2,
                child: MobileScanner(
                  onDetect: (capture) {
                    Barcode barcode = capture.barcodes.last;
                    var value = barcode.rawValue.toString();
                    if (value != controller.lastScanned.value) {
                      if (value != "") {
                        controller.lastScanned.value = value;
                        controller.username.value = value;
                        controller.clockInOut();

                        Future.delayed(const Duration(seconds: 5), () {
                          controller.lastScanned.value = "";
                        });
                      } else {
                        CustomSnackBar.showErrorToast("QR tidak valid");
                      }
                    }
                  },
                  controller: MobileScannerController(
                    detectionSpeed: DetectionSpeed.normal,
                    facing: CameraFacing.front,
                  ),
                ),
              )
            : Container(),
        const SizedBox(
          height: 16,
        ),
        Center(
          child: Text(
            GetPlatform.isAndroid
                ? "Scan Kartu Pelajar Untuk Pencatatan Presensi"
                : "Harap Gunakan Perangkat Android Untuk Scan Kartu Pelajar",
            style: TextStyleValues.textMedium.copyWith(color: ColorValues.grey),
          ),
        ),
        Expanded(child: Container()),
        Center(
          child: InkWell(
            onTap: () {
              controller.isManual.value = true;
            },
            child: Text(
              "Masukkan NIPD Manual",
              style: TextStyleValues.textMedium
                  .copyWith(color: ColorValues.bluePrimary),
            ),
          ),
        )
      ],
    );
  }

  Widget _manualContent() {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: CustomDropdown(
                items: const ["clock_in", "clock_out"],
                itemToString: (item) {
                  if (item == "clock_in") {
                    return "Presensi Masuk";
                  } else {
                    return "Presensi Pulang";
                  }
                },
                selectedItem: controller.type.value,
                onChanged: (value) {
                  controller.type.value = value;
                },
              ),
            ),
            Expanded(child: Container()),
            InkWell(
              child: const Icon(
                Icons.logout_rounded,
                color: ColorValues.grey,
              ),
              onTap: () {
                GetStorage().erase();
                Get.offAllNamed(Routes.login);
              },
            )
          ],
        ),
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Selamat Datang di Saais",
                    style: TextStyleValues.textUltraBold,
                  ),
                  Obx(
                    () => Text(
                      Formatter.dateFormatter(
                          date: controller.currentTime.value,
                          type: "displayTime"),
                      style: TextStyleValues.textUltraBold
                          .copyWith(color: ColorValues.bluePrimary),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Smaisga Academic Information System",
                    style: TextStyleValues.textRegular
                        .copyWith(color: ColorValues.grey),
                  ),
                  Obx(
                    () => Text(
                      Formatter.dateFormatter(
                          date: controller.currentTime.value, type: "display"),
                      style: TextStyleValues.textRegular
                          .copyWith(color: ColorValues.grey),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 64,
              ),
              Text(
                "Masukkan NIPD/Username untuk melanjutkan",
                style: TextStyleValues.textMedium
                    .copyWith(color: ColorValues.grey),
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextInput(
                controller: controller.usernameController,
                label: 'NIPD/Username',
                prefixIcon: const Icon(Icons.person),
                validator: (value) {
                  value = value ?? "";
                  if (value.length < 4) {
                    return "Diisi minimal 4 karakter";
                  } else {
                    return null;
                  }
                },
              ),
              const SizedBox(height: 16),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    onTap: () {
                      controller.isManual.value = false;
                    },
                    child: Text(
                      "Scan QR NIPD",
                      style: TextStyleValues.textMedium
                          .copyWith(color: ColorValues.bluePrimary),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        CustomButton(
          title: "Catat Kehadiran",
          action: () {
            if (controller.usernameController.text != "") {
              controller.username.value = controller.usernameController.text;
              controller.clockInOut();
            }
          },
        ),
      ],
    );
  }

  Widget _successContent() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Hi ${controller.user.value.name} (${controller.user.value.username})',
            style: TextStyleValues.textUltraBold,
          ),
          const SizedBox(
            height: 16,
          ),
          Text("Presensi berhasil tercatat pada",
              style:
                  TextStyleValues.textMedium.copyWith(color: ColorValues.grey)),
          const SizedBox(
            height: 16,
          ),
          Text(
            Formatter.dateFormatter(
                date: controller.type.value == "clock_in"
                    ? controller.attendance.value.clock_in_at ?? DateTime.now()
                    : controller.attendance.value.clock_out_at ??
                        DateTime.now(),
                type: "displayTime"),
            style: TextStyleValues.textUltraBold
                .copyWith(color: ColorValues.bluePrimary),
          ),
        ],
      ),
    );
  }
}
