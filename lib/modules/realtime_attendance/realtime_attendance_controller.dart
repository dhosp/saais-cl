import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/school_attendance_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/school_attendance.dart';
import 'package:saais_client/models/user.dart';

class RealtimeAttendanceController extends GetxController {
  final _attendanceAPI = Get.find<SchoolAttendanceAPI>();
  var currentTime = DateTime.now().obs;
  var isManual = false.obs;
  var isSuccess = false.obs;
  final usernameController = TextEditingController();
  var username = "".obs;
  var type = "clock_in".obs;
  var lastScanned = "".obs;

  var user = User(id: 0, name: "", user_roles: []).obs;
  var attendance = SchoolAttendance().obs;

  beforeStart() {
    Timer.periodic(const Duration(minutes: 1), (Timer t) {
      currentTime.value = DateTime.now();
    });
  }

  void clockInOut() async {
    CustomLoading.showLoading();
    var result = await _attendanceAPI.clockInOut(
        username: username.value, type: type.value);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      isSuccess.value = true;
      user.value = User.fromJson(result.body["data"]["user"]);
      attendance.value =
          SchoolAttendance.fromJson(result.body["data"]["school_attendance"]);
      await Future.delayed(const Duration(seconds: 3), () {
        isSuccess.value = false;
        isManual.value = false;
        user.value = User(id: 0, name: "", user_roles: []);
        usernameController.text = "";
        username.value = "";
        attendance.value = SchoolAttendance();
      });
    }
  }
}
