import 'package:get/get.dart';
import 'package:saais_client/apis/school_attendance_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/teacher_attendance_detail/teacher_attendance_detail_controller.dart';

class TeacherAttendanceDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => SchoolAttendanceAPI());
    Get.lazyPut(() => TeacherAttendanceDetailController());
  }
}
