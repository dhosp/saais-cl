import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/school_attendance_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/school_attendance.dart';
import 'package:saais_client/values/constant_values.dart';

class TeacherAttendanceDetailController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final _attendanceAPI = Get.find<SchoolAttendanceAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var userName = "".obs;
  List<SchoolAttendance> attendances =
      List<SchoolAttendance>.empty(growable: true).obs;

  // var for modal
  var attendanceType = "".obs;
  final reasonController = TextEditingController(text: "");

  final clockInController = TextEditingController(text: "");
  var clockIn = DateTime.tryParse("").obs;

  final clockOutController = TextEditingController(text: "");
  var clockOut = DateTime.tryParse("").obs;
  // end var for modal

  void beforeStart() {
    if (_userId > 0) {
      getUser();
      getList();
    }
  }

  void getUser() async {
    var result = await _userAPI.findOne(_userId);
    if (result.body?["status"] ?? false) {
      userName.value = result.body["data"]["name"];
    }
  }

  void getList() async {
    // refresh modal
    attendanceType.value = "";
    reasonController.text = "";
    clockInController.text = "";
    clockIn.value = null;
    clockOutController.text = "";
    clockOut.value = null;
    // end refresh modal

    var findAllResult =
        await _attendanceAPI.findAll(perPage: displayedData, user_id: _userId);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      attendances.clear();
      for (var item in resultData) {
        attendances.add(SchoolAttendance.fromJson(item));
      }
    }
  }

// func for modal
  void saveAttendanceData() async {
    CustomLoading.showLoading();
    Response result = await _attendanceAPI.manualCreate(
        user_id: _userId,
        type: attendanceType.value,
        clock_in_at: clockIn.value,
        clock_out_at: clockOut.value,
        reason: reasonController.text);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      getList();

      Get.close(1);
    }
  }

  void removeAttendanceData(int attendanceId) async {
    CustomLoading.showLoading();
    Response result = await _attendanceAPI.remove(attendance_id: attendanceId);

    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      getList();
      Get.close(1);
    }
  }

  clockInCallback(DateTime date) {
    clockIn.value = date;
  }

  clockOutCallback(DateTime date) {
    clockOut.value = date;
  }
  // end func for modal
}
