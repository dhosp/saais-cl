import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/city.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/models/user_detail.dart';
import 'package:saais_client/models/user_role.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/get_back_route.dart';

class TeacherDetailController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final usernameController = TextEditingController(text: "");
  final nameController = TextEditingController(text: "");
  final name = "".obs;

  final nikController = TextEditingController(text: "");
  final addressController = TextEditingController(text: "");
  var birthPlace = City(id: 0, name: "", province_name: "").obs;
  final birthDateController = TextEditingController(text: "");
  var birthDate = DateTime.tryParse("").obs;
  var gender = "".obs;
  final religionController = TextEditingController(text: "");
  final phoneController = TextEditingController(text: "");
  var roles = [].obs;

  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var isAddNew = false;
  late User user;
  late UserDetail userDetail;

  void beforeStart() {
    if (_userId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _userAPI.findOne(_userId);
      if (result.body?["status"] ?? false) {
        user = User.fromJson(result.body["data"]);
      }
    } else {
      user = Get.arguments;
    }

    user.user_detail ??= UserDetail();

    usernameController.text = user.username;
    nameController.text = user.name;
    name.value = user.name;

    nikController.text = user.user_detail!.nik?.toString() ?? "";
    addressController.text = user.user_detail!.address ?? "";
    if (user.user_detail!.place_of_birth_id != null) {
      birthPlace.value = City(
          id: user.user_detail!.place_of_birth_id!,
          name: user.user_detail!.birth_city_name!,
          province_name: user.user_detail!.birth_city_province_name!);
    }
    if (user.user_detail!.date_of_birth != null) {
      birthDateController.text = Formatter.dateFormatter(
          date: user.user_detail!.date_of_birth!, type: "display");
      birthDate.value = user.user_detail!.date_of_birth!;
    }
    gender.value = user.user_detail!.gender ?? "";
    religionController.text = user.user_detail!.religion ?? "";
    phoneController.text = user.user_detail!.phone_num ?? "";

    roles.clear();

    for (var item in user.user_roles) {
      roles.add(item.code);
    }
  }

  void saveData() async {
    CustomLoading.showLoading();
    if (isAddNew) {
      user = User(
          id: 0,
          name: nameController.text,
          username: usernameController.text,
          user_roles: [],
          user_detail: UserDetail());
    }

    user.username = usernameController.text;
    user.name = nameController.text;
    name.value = nameController.text;

    user.user_detail!.nik = nikController.text;

    user.user_detail!.address = addressController.text;
    user.user_detail!.place_of_birth_id =
        birthPlace.value.id > 0 ? birthPlace.value.id : null;
    user.user_detail!.date_of_birth = birthDate.value;
    user.user_detail!.gender = gender.value;
    user.user_detail!.religion = religionController.text;
    user.user_detail!.phone_num = phoneController.text;

    user.user_roles = [];

    for (var item in roles) {
      user.user_roles.add(UserRole(code: item));
    }

    Response result;
    if (isAddNew) {
      result = await _userAPI.create(user);
    } else {
      result = await _userAPI.update(id: _userId, user: user);
    }
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "1", "tab": "1"});
    }
  }

  birthDateCallback(DateTime date) {
    birthDate.value = date;
  }
}
