import 'package:get/get.dart';
import 'package:saais_client/apis/city_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/teacher_detail/teacher_detail_controller.dart';

class TeacherDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => CityAPI());
    Get.lazyPut(() => TeacherDetailController());
  }
}
