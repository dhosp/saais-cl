import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_date_picker.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_multiselect.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/city.dart';
import 'package:saais_client/models/gender_enum.dart';
import 'package:saais_client/modules/teacher_detail/teacher_detail_controller.dart';
import 'package:saais_client/utils/city_data.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class TeacherDetailScreen extends GetView<TeacherDetailController> {
  const TeacherDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Pengguna",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(
                () => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: ColorValues.greyBackground),
                        constraints: const BoxConstraints(
                          maxHeight: 250,
                          maxWidth: 250,
                        ),
                        child: Center(
                          child: Text(
                              controller.name.value.isNotEmpty
                                  ? controller.name.value[0]
                                  : "",
                              style: TextStyleValues.textUltraBold
                                  .copyWith(color: ColorValues.bluePrimary)),
                        )),
                    const SizedBox(height: 32),
                    Form(
                      key: formKey,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: _formGroup1()),
                          const SizedBox(width: 16),
                          Expanded(child: _formGroup2()),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 32),
              _actionButton(formKey),
            ],
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomTextInput(
          label: "Username",
          controller: controller.usernameController,
          autoValidateMode: AutovalidateMode.always,
          validator: (value) {
            value = value ?? "";
            if (value.length < 4) {
              return "Diisi minimal 4 karakter";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nama Lengkap",
          controller: controller.nameController,
          autoValidateMode: AutovalidateMode.always,
          validator: (value) {
            value = value ?? "";
            if (value == "") {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "NIK",
          controller: controller.nikController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value = value ?? "";
            if ((value != "" && int.tryParse(value) == null) ||
                (value != "" && value.length != 16)) {
              return "Diisi 16 angka";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Agama",
          controller: controller.religionController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nomor HP",
          controller: controller.phoneController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value = value ?? "";
            if ((value != "" && int.tryParse(value) == null) ||
                (value != "" && value.length < 10) ||
                (value != "" && value.substring(0, 2) != "08")) {
              return "Diisi minimal 10 angka, dengan format 08XX";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        CustomDropdown(
          hint: "Tempat Lahir",
          showSearchBox: true,
          asyncItems: (String filter) => CityData().getCityDropdown(filter),
          selectedItem: controller.birthPlace.value.id == 0
              ? null
              : controller.birthPlace.value,
          itemToString: (item) => City.getDropdownLabel(item),
          onChanged: (value) => {controller.birthPlace.value = value},
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              child: CustomDatePicker(
                label: "Tanggal Lahir",
                controller: controller.birthDateController,
                dateValueCallback: controller.birthDateCallback,
                date: controller.birthDate.value,
                type: "date",
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: CustomDropdown(
                hint: "Jenis Kelamin",
                items: GenderEnum.getDropdownItems(),
                itemToString: (item) =>
                    GenderEnum.getDropdownLabel(gender: item),
                selectedItem: controller.gender.value == ""
                    ? null
                    : controller.gender.value,
                onChanged: (value) => {controller.gender.value = value},
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Alamat",
          controller: controller.addressController,
          inputType: TextInputType.multiline,
          maxLines: 3,
        ),
        const SizedBox(
          height: 16,
        ),
        CustomMultiselect(
          hint: "Hak Akses",
          items: const ["teacher", "homeroom_teacher", "principal", "finance"],
          itemToString: (item) {
            switch (item) {
              case "teacher":
                return "Guru";
              case "homeroom_teacher":
                return "Wali Kelas";
              case "principal":
                return "Kepala Sekolah";
              case "finance":
                return "Bendahara";
              default:
                return "";
            }
          },
          selectedItems: controller.roles.isNotEmpty ? controller.roles : null,
          validator: (value) {
            if (value?.length == 0) {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
          onChanged: (value) {
            controller.roles.value = value;
          },
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () => {
              if (formKey.currentState!.validate()) {controller.saveData()}
            },
          ),
        ),
      ],
    );
  }
}
