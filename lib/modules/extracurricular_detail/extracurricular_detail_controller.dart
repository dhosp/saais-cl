import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class ExtracurricularDetailController extends GetxController {
  final _extracurricularAPI = Get.find<ExtracurricularAPI>();
  final _userAPI = Get.find<UserAPI>();

  final nameController = TextEditingController(text: "");

  var teacher = User(id: 0, name: "", username: "", user_roles: []).obs;

  final _exculId = int.tryParse(Get.parameters["exculId"].toString()) ?? 0;
  var isAddNew = false;
  var extracurricular = Extracurricular().obs;

  void beforeStart() {
    if (_exculId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _extracurricularAPI.findOne(_exculId);
      if (result.body?["status"] ?? false) {
        extracurricular.value = Extracurricular.fromJson(result.body["data"]);
      }
    } else {
      extracurricular.value = Get.arguments;
    }

    nameController.text = extracurricular.value.name;

    teacher.value = User(
        id: extracurricular.value.user!.id,
        name: extracurricular.value.user!.name,
        username: "",
        user_roles: []);
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result;
    if (isAddNew) {
      result = await _extracurricularAPI.create(
          extracurricular: Extracurricular(
              name: nameController.text,
              user: User(
                  id: teacher.value.id,
                  name: teacher.value.name,
                  user_roles: [])));
    } else {
      result = await _extracurricularAPI.update(
          id: _exculId,
          extracurricular: Extracurricular(
              name: nameController.text,
              user: User(
                  id: teacher.value.id,
                  name: teacher.value.name,
                  user_roles: [])));
    }

    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "3", "tab": "1"});
    }
  }

  Future<List<dynamic>> getTeachers(String? filter) async {
    List<User> teachers = [];

    var teacherFindAllResult =
        await _userAPI.findAll(role: "teacher", search: filter ?? "");

    if (teacherFindAllResult.body != null &&
            teacherFindAllResult.body?["status"] ??
        false) {
      List<dynamic> resultData = teacherFindAllResult.body["data"];

      for (var item in resultData) {
        teachers.add(User.fromJson(item));
      }
    }

    return teachers;
  }
}
