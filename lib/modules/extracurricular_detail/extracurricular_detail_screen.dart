import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/major_enum.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_controller.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';

class ExtracurricularDetailScreen
    extends GetView<ExtracurricularDetailController> {
  const ExtracurricularDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Ekstrakurikuler",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(() => Form(
                    key: formKey,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: _formGroup1()),
                      ],
                    ),
                  )),
              const SizedBox(height: 32),
              _actionButton(formKey)
            ],
          ),
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () {
              if (formKey.currentState!.validate()) {
                controller.saveData();
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomTextInput(
          label: "Nama",
          controller: controller.nameController,
          validator: (value) {
            if ((value ??= "").length < 4) {
              return "Minimal diisi 4 karakter";
            } else {
              return null;
            }
          },
        ),
        SizedBox(
          height: 16,
        ),
        CustomDropdown(
            hint: "Pembina",
            showSearchBox: true,
            asyncItems: (String filter) => controller.getTeachers(filter),
            itemToString: (item) => User.getDropdownLabel(item),
            selectedItem: controller.teacher.value.id == 0
                ? null
                : controller.teacher.value,
            onChanged: (value) {
              controller.teacher.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
      ],
    );
  }
}
