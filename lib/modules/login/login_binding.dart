import 'package:get/get.dart';
import 'package:saais_client/apis/auth_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/login/login_controller.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthAPI());
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => LoginController());
  }
}
