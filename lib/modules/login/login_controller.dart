import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/apis/auth_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/home/home_controller.dart';
import 'package:saais_client/routes/app_pages.dart';

class LoginController extends GetxController {
  final _authAPI = Get.find<AuthAPI>();
  final _userAPI = Get.find<UserAPI>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final hideTextPassword = true.obs;
  final errorTextUsername = "".obs;
  final errorTextPassword = "".obs;

  void login() async {
    CustomLoading.showLoading();
    var result =
        await _authAPI.login(usernameController.text, passwordController.text);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      CustomLoading.showLoading();

      var token = result.body["data"]["token"];
      await GetStorage().write("token", token);

      var userId = result.body["data"]["user"]["id"];
      var userResult = await _userAPI.findOne(userId);
      CustomLoading.hideLoading();

      if (userResult.body?["status"] ?? false) {
        User user = User.fromJson(userResult.body["data"]);
        await GetStorage().write("user", user.toJson());
        HomeController.activeMenu.value = 0;
        HomeController.activeTab.value = 0;
        Get.offAllNamed(
          Routes.home,
        );
      }
    }
  }
}
