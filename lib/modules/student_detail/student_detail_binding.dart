import 'package:get/get.dart';
import 'package:saais_client/apis/city_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/student_detail/student_detail_controller.dart';

class StudentDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => CityAPI());
    Get.lazyPut(() => StudentDetailController());
  }
}
