import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/city.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/models/user_detail.dart';
import 'package:saais_client/models/user_role.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/get_back_route.dart';

class StudentDetailController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final usernameController = TextEditingController(text: "");
  final nameController = TextEditingController(text: "");
  final name = "".obs;

  final nisnController = TextEditingController(text: "");
  final nikController = TextEditingController(text: "");
  final addressController = TextEditingController(text: "");
  var birthPlace = City(id: 0, name: "", province_name: "").obs;
  final birthDateController = TextEditingController(text: "");
  var birthDate = DateTime.tryParse("").obs;
  var gender = "".obs;
  final religionController = TextEditingController(text: "");
  final phoneController = TextEditingController(text: "");
  final familyStatusController = TextEditingController(text: "");
  final familyOrderController = TextEditingController(text: "");
  final schoolOriginController = TextEditingController(text: "");
  final joinDateController = TextEditingController(text: "");
  var joinDate = DateTime.tryParse("").obs;
  var joinGrade = "".obs;
  final fatherController = TextEditingController(text: "");
  final fatherJobController = TextEditingController(text: "");
  final motherController = TextEditingController(text: "");
  final motherJobController = TextEditingController(text: "");
  final parentPhoneController = TextEditingController(text: "");
  final parentAddressController = TextEditingController(text: "");

  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var isAddNew = false;
  late User user;
  late UserDetail userDetail;

  void beforeStart() {
    if (_userId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _userAPI.findOne(_userId);
      if (result.body?["status"] ?? false) {
        user = User.fromJson(result.body["data"]);
      }
    } else {
      user = Get.arguments;
    }

    user.user_detail ??= UserDetail();

    usernameController.text = user.username;
    nameController.text = user.name;
    name.value = user.name;

    nisnController.text = user.user_detail!.nisn?.toString() ?? "";
    nikController.text = user.user_detail!.nik?.toString() ?? "";
    addressController.text = user.user_detail!.address ?? "";
    if (user.user_detail!.place_of_birth_id != null) {
      birthPlace.value = City(
          id: user.user_detail!.place_of_birth_id!,
          name: user.user_detail!.birth_city_name!,
          province_name: user.user_detail!.birth_city_province_name!);
    }
    if (user.user_detail!.date_of_birth != null) {
      birthDateController.text = Formatter.dateFormatter(
          date: user.user_detail!.date_of_birth!, type: "display");
      birthDate.value = user.user_detail!.date_of_birth!;
    }
    gender.value = user.user_detail!.gender ?? "";
    religionController.text = user.user_detail!.religion ?? "";
    phoneController.text = user.user_detail!.phone_num ?? "";
    familyStatusController.text = user.user_detail!.status_in_family ?? "";
    familyOrderController.text =
        user.user_detail!.order_in_family?.toString() ?? "";
    schoolOriginController.text = user.user_detail!.origin_school ?? "";
    if (user.user_detail!.join_date != null) {
      birthDateController.text = Formatter.dateFormatter(
          date: user.user_detail!.join_date!, type: "display");
      joinDate.value = user.user_detail!.join_date!;
    }
    joinGrade.value = user.user_detail!.join_class ?? "";
    fatherController.text = user.user_detail!.father_name ?? "";
    fatherJobController.text = user.user_detail!.father_occupation ?? "";
    motherController.text = user.user_detail!.mother_name ?? "";
    motherJobController.text = user.user_detail!.mother_occupation ?? "";
    parentPhoneController.text =
        user.user_detail!.parent_phone_num?.toString() ?? "";
    parentAddressController.text = user.user_detail!.parent_address ?? "";
  }

  void saveData() async {
    CustomLoading.showLoading();
    if (isAddNew) {
      user = User(
          id: 0,
          name: nameController.text,
          username: usernameController.text,
          user_roles: [UserRole(code: "student")],
          user_detail: UserDetail());
    }

    user.username = usernameController.text;
    user.name = nameController.text;
    name.value = nameController.text;

    user.user_detail!.nisn = nisnController.text;
    user.user_detail!.nik = nikController.text;

    user.user_detail!.address = addressController.text;
    user.user_detail!.place_of_birth_id =
        birthPlace.value.id > 0 ? birthPlace.value.id : null;
    user.user_detail!.date_of_birth = birthDate.value;
    user.user_detail!.gender = gender.value;
    user.user_detail!.religion = religionController.text;
    user.user_detail!.phone_num = phoneController.text;
    user.user_detail!.status_in_family = familyStatusController.text;
    user.user_detail!.order_in_family =
        int.tryParse(familyOrderController.text);
    user.user_detail!.origin_school = schoolOriginController.text;
    user.user_detail!.join_date = joinDate.value;
    user.user_detail!.join_class = joinGrade.value;
    user.user_detail!.father_name = fatherController.text;
    user.user_detail!.father_occupation = fatherJobController.text;
    user.user_detail!.mother_name = motherController.text;
    user.user_detail!.mother_occupation = motherJobController.text;
    user.user_detail!.parent_address = parentAddressController.text;
    user.user_detail!.parent_phone_num = parentPhoneController.text;

    Response result;
    if (isAddNew) {
      result = await _userAPI.create(user);
    } else {
      result = await _userAPI.update(id: _userId, user: user);
    }
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "1", "tab": "0"});
    }
  }

  void resetPass() async {
    user.is_reset = true;

    var result = await _userAPI.resetPass(_userId);

    if (result.body?["status"] ?? false) {
      CustomDialog.showDialog(
          title: "Kode Reset ${user.name} Berlaku 2 Menit",
          message: result.body["data"]?["code"] ?? "");
    }
  }

  birthDateCallback(DateTime date) {
    birthDate.value = date;
  }

  joinDateCallback(DateTime date) {
    joinDate.value = date;
  }
}
