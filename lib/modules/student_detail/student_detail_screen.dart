import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_date_picker.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/city.dart';
import 'package:saais_client/models/gender_enum.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/modules/student_detail/student_detail_controller.dart';
import 'package:saais_client/utils/city_data.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class StudentDetailScreen extends GetView<StudentDetailController> {
  const StudentDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Pengguna",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(
                () => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: ColorValues.greyBackground),
                        constraints: const BoxConstraints(
                          maxHeight: 250,
                          maxWidth: 250,
                        ),
                        child: Center(
                          child: Text(
                              controller.name.value.isNotEmpty
                                  ? controller.name.value[0]
                                  : "",
                              style: TextStyleValues.textUltraBold
                                  .copyWith(color: ColorValues.bluePrimary)),
                        )),
                    const SizedBox(height: 32),
                    Form(
                      key: formKey,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: _formGroup1()),
                          const SizedBox(width: 16),
                          Expanded(child: _formGroup2()),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 32),
              _actionButton(formKey),
            ],
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomTextInput(
          label: "NIPD Sebagai Username",
          controller: controller.usernameController,
          isInputDigit: true,
          inputType: TextInputType.number,
          autoValidateMode: AutovalidateMode.always,
          validator: (value) {
            value = value ?? "";
            if (value.length < 4 || int.tryParse(value) == null) {
              return "Diisi minimal 4 angka";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nama Lengkap",
          controller: controller.nameController,
          autoValidateMode: AutovalidateMode.always,
          validator: (value) {
            value = value ?? "";
            if (value == "") {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "NISN",
          controller: controller.nisnController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value = value ?? "";
            if ((value != "" && int.tryParse(value) == null) ||
                value != "" && value.length != 10) {
              return "Diisi 10 angka";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "NIK",
          controller: controller.nikController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value = value ?? "";
            if ((value != "" && int.tryParse(value) == null) ||
                (value != "" && value.length != 16)) {
              return "Diisi 16 angka";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomDropdown(
          hint: "Tempat Lahir",
          showSearchBox: true,
          asyncItems: (String filter) => CityData().getCityDropdown(filter),
          selectedItem: controller.birthPlace.value.id == 0
              ? null
              : controller.birthPlace.value,
          itemToString: (item) => City.getDropdownLabel(item),
          onChanged: (value) => {controller.birthPlace.value = value},
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: CustomDatePicker(
                label: "Tanggal Lahir",
                controller: controller.birthDateController,
                dateValueCallback: controller.birthDateCallback,
                date: controller.birthDate.value,
                type: "date",
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: CustomDropdown(
                hint: "Jenis Kelamin",
                items: GenderEnum.getDropdownItems(),
                itemToString: (item) =>
                    GenderEnum.getDropdownLabel(gender: item),
                selectedItem: controller.gender.value == ""
                    ? null
                    : controller.gender.value,
                onChanged: (value) => {controller.gender.value = value},
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Agama",
          controller: controller.religionController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nomor HP",
          controller: controller.phoneController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value = value ?? "";
            if ((value != "" && int.tryParse(value) == null) ||
                (value != "" && value.length < 10) ||
                (value != "" && value.substring(0, 2) != "08")) {
              return "Diisi minimal 10 angka, dengan format 08XX";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Alamat",
          controller: controller.addressController,
          inputType: TextInputType.multiline,
          maxLines: 3,
        ),
        const SizedBox(height: 16),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              flex: 3,
              child: CustomTextInput(
                label: "Status Dalam keluarga",
                controller: controller.familyStatusController,
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: CustomTextInput(
                label: "Anak Ke",
                controller: controller.familyOrderController,
                isInputDigit: true,
                inputType: TextInputType.number,
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Asal Sekolah",
          controller: controller.schoolOriginController,
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: CustomDatePicker(
                label: "Diterima Pada Tanggal",
                controller: controller.joinDateController,
                dateValueCallback: controller.joinDateCallback,
                date: controller.joinDate.value,
                type: "date",
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: CustomDropdown(
                hint: "Diterima Di Kelas",
                items: GradeEnum.getDropdownItems(),
                itemToString: (item) => GradeEnum.getDropdownLabel(grade: item),
                selectedItem: controller.joinGrade.value == ""
                    ? null
                    : controller.joinGrade.value,
                onChanged: (value) => {controller.joinGrade.value = value},
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nama Ayah",
          controller: controller.fatherController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Pekerjaan Ayah",
          controller: controller.fatherJobController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nama Ibu",
          controller: controller.motherController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Pekerjaan Ibu",
          controller: controller.motherJobController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nomor HP Orang Tua",
          controller: controller.parentPhoneController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value = value ?? "";
            if ((value != "" && int.tryParse(value) == null) ||
                (value != "" && value.length < 10) ||
                (value != "" && value.substring(0, 2) != "08")) {
              return "Diisi minimal 10 angka, dengan format 08XX";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Alamat Orang Tua",
          inputType: TextInputType.multiline,
          maxLines: 3,
          controller: controller.parentAddressController,
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: controller.isAddNew ? 3 : 2, child: Container()),
        controller.isAddNew
            ? Container()
            : Expanded(
                child: CustomButton(
                  title: "Reset Password",
                  action: () => {controller.resetPass()},
                ),
              ),
        const SizedBox(
          width: 16,
        ),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () => {
              if (formKey.currentState!.validate()) {controller.saveData()}
            },
          ),
        ),
      ],
    );
  }
}
