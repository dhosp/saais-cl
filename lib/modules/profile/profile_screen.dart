import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/profile/profile_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class ProfileScreen extends GetView<ProfileController> {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(),
      landscapeLayout: _landscapeLayout(),
      includeAppBar: true,
      appBarTitle: "Profil",
      greySpace: false,
    );
  }

  Widget _potraitLayout() {
    return Container();
    // return Column(
    //   crossAxisAlignment: CrossAxisAlignment.center,
    //   children: [
    //     Container(
    //         decoration: BoxDecoration(
    //             borderRadius: BorderRadius.circular(4),
    //             color: ColorValues.greyBackground),
    //         constraints: const BoxConstraints(
    //           maxHeight: 250,
    //           maxWidth: 250,
    //         ),
    //         child: Center(
    //           child: Text(
    //               controller.nameController.text.isNotEmpty
    //                   ? controller.nameController.text[0]
    //                   : "",
    //               style: TextStyleValues.textUltraBold
    //                   .copyWith(color: ColorValues.bluePrimary)),
    //         )),
    //     const SizedBox(height: 32),
    //     _formGroup1(),
    //     const SizedBox(
    //       height: 16,
    //     ),
    //     _formGroup2()
    //   ],
    // );
  }

  Widget _landscapeLayout() {
    return Container(
      padding: const EdgeInsets.all(32),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: ColorValues.white,
      ),
      constraints: const BoxConstraints(maxWidth: 1000),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: ColorValues.greyBackground),
              constraints: const BoxConstraints(
                maxHeight: 250,
                maxWidth: 250,
              ),
              child: Center(
                child: Text(
                    controller.nameController.text.isNotEmpty
                        ? controller.nameController.text[0]
                        : "",
                    style: TextStyleValues.textUltraBold
                        .copyWith(color: ColorValues.bluePrimary)),
              )),
          const SizedBox(height: 32),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(child: _formGroup1()),
              const SizedBox(width: 16),
              Expanded(child: _formGroup2()),
            ],
          ),
        ],
      ),
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomTextInput(
          label: "Username",
          readOnly: true,
          controller: controller.usernameController,
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Nama Lengkap",
          readOnly: true,
          controller: controller.nameController,
        ),
        const SizedBox(height: 16),
        const CustomTextInput(label: "NIK"),
        const SizedBox(height: 16),
        const CustomTextInput(label: "NISN"),
        const SizedBox(height: 16),
        const CustomTextInput(label: "Tempat Lahir"),
      ],
    );
  }

  Widget _formGroup2() {
    return const Column(
      children: [
        CustomTextInput(label: "Password"),
        SizedBox(height: 16),
        CustomTextInput(label: "Alamat"),
        SizedBox(height: 16),
        CustomTextInput(label: "Tanggal Lahir"),
        SizedBox(height: 16),
        CustomTextInput(label: "Agama"),
        SizedBox(height: 16),
        CustomTextInput(label: "Jenis Kelamin"),
      ],
    );
  }
}
