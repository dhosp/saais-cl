import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/profile/profile_controller.dart';

class ProfileBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => ProfileController());
  }
}
