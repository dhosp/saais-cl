import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/user.dart';

class ProfileController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final usernameController = TextEditingController(text: "");
  final nameController = TextEditingController(text: "");
  final passwordController = TextEditingController;

  late User user;

  void beforeStart() {
    getProfile();
  }

  void getProfile() async {
    user = User.fromJson(await GetStorage().read("user"));
    var result = await _userAPI.findOne(int.parse(user.id.toString()));

    if (result.body?["status"] ?? false) {
      user = User.fromJson(result.body["data"]);

      usernameController.text = user.username;
      nameController.text = user.name;

      await GetStorage().write("user", user.toJson());
    }
  }
}
