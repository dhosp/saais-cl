import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/attitude_api.dart';
import 'package:saais_client/apis/attitude_report_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/attitude.dart';
import 'package:saais_client/models/attitude_score.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class AttitudeReportDetailController extends GetxController {
  // final _classAPI = Get.find<ClassAPI>();
  final _userAPI = Get.find<UserAPI>();
  final _attitudeAPI = Get.find<AttitudeAPI>();
  final _attitudeScoreAPI = Get.find<AttitudeReportAPI>();

  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var isAddNew = false;

  var student = User(id: 0, name: "", username: "", user_roles: []).obs;
  var nameController = TextEditingController(text: "");

  List<Attitude> spiritualAttitudes = List<Attitude>.empty(growable: true).obs;
  List<int> alwaysDoneSpiritualAttitudes = List<int>.empty(growable: true).obs;
  List<int> improveSpiritualAttitudes = List<int>.empty(growable: true).obs;
  List<Attitude> sosialAttitudes = List<Attitude>.empty(growable: true).obs;
  List<int> alwaysDoneSosialAttitudes = List<int>.empty(growable: true).obs;
  List<int> improveSosialAttitudes = List<int>.empty(growable: true).obs;
  var spiritualGrade = "".obs;
  var sosialGrade = "".obs;

  void beforeStart() {
    if (_userId > 0) {
      getData();
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _userAPI.findOne(_userId);
      if (result.body?["status"] ?? false) {
        student.value = User.fromJson(result.body["data"]);
      }
    } else {
      student.value = Get.arguments;
    }

    nameController.text = student.value.name;
    getAllAttitudes();
    getAttitudeScores();
  }

  void getAttitudeScores() async {
    var findAllResult =
        await _attitudeScoreAPI.findAll(perPage: -1, studentId: _userId);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      for (var item in resultData) {
        var currentItem = AttitudeScore.fromJson(item);
        if (currentItem.type == "spiritual") {
          spiritualGrade.value = currentItem.grade;
          for (var item in currentItem.attitude_score_details!) {
            if (item.type == "always_done") {
              alwaysDoneSpiritualAttitudes.add(item.attitude!.id);
            } else {
              improveSpiritualAttitudes.add(item.attitude!.id);
            }
          }
        } else {
          for (var item in currentItem.attitude_score_details!) {
            if (item.type == "always_done") {
              alwaysDoneSosialAttitudes.add(item.attitude!.id);
            } else {
              improveSosialAttitudes.add(item.attitude!.id);
            }
          }
          sosialGrade.value = currentItem.grade;
        }
      }
    }
  }

  void getAllAttitudes() async {
    var findAllResult = await _attitudeAPI.findAll(perPage: -1);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      for (var item in resultData) {
        var currentItem = Attitude.fromJson(item);
        if (currentItem.type == "spiritual") {
          spiritualAttitudes.add(Attitude.fromJson(item));
        } else {
          sosialAttitudes.add(Attitude.fromJson(item));
        }
      }
    }
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result = await _attitudeScoreAPI.create(
        student_id: _userId,
        sosial_grade: sosialGrade.value,
        spiritual_grade: spiritualGrade.value,
        always_done_sosial_attitudes: alwaysDoneSosialAttitudes,
        improve_sosial_attitudes: improveSosialAttitudes,
        always_done_spiritual_attitudes: alwaysDoneSpiritualAttitudes,
        improve_spiritual_attitudes: improveSpiritualAttitudes);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "7", "tab": "2"});
    }
  }

  void selectAlwaysDoneSpiritualAttitude(int id) {
    int index = alwaysDoneSpiritualAttitudes.indexOf(id);
    if (index >= 0) {
      alwaysDoneSpiritualAttitudes.removeAt(index);
    } else {
      alwaysDoneSpiritualAttitudes.add(id);
    }
  }

  void selectImproveSpiritualAttitude(int id) {
    int index = improveSpiritualAttitudes.indexOf(id);
    if (index >= 0) {
      improveSpiritualAttitudes.removeAt(index);
    } else {
      improveSpiritualAttitudes.add(id);
    }
  }

  void selectAlwaysDoneSosialAttitude(int id) {
    int index = alwaysDoneSosialAttitudes.indexOf(id);
    if (index >= 0) {
      alwaysDoneSosialAttitudes.removeAt(index);
    } else {
      alwaysDoneSosialAttitudes.add(id);
    }
  }

  void selectImproveSosialAttitude(int id) {
    int index = improveSosialAttitudes.indexOf(id);
    if (index >= 0) {
      improveSosialAttitudes.removeAt(index);
    } else {
      improveSosialAttitudes.add(id);
    }
  }
}
