import 'package:get/get.dart';
import 'package:saais_client/apis/attitude_api.dart';
import 'package:saais_client/apis/attitude_report_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/attitude_report_detail/attitude_report_detail_controller.dart';

class AttitudeReportDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => AttitudeReportAPI());
    Get.lazyPut(() => AttitudeAPI());
    Get.lazyPut(() => AttitudeReportDetailController());
  }
}
