import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/attitude_report_detail/attitude_report_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class AttitudeReportDetailScreen
    extends GetView<AttitudeReportDetailController> {
  const AttitudeReportDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Nilai Sikap",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return const Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // children: [
        //   _formGroup1(),
        //   const SizedBox(
        //     height: 16,
        //   ),
        //   _formGroup2()
        // ],
        );
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(() => Form(
                    key: formKey,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: _formGroup1()),
                      ],
                    ),
                  )),
              const SizedBox(height: 32),
              _actionButton(formKey)
            ],
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      CustomTextInput(
        label: "Nama Siswa",
        controller: controller.nameController,
        readOnly: true,
      ),
      const SizedBox(height: 16),
      const Text(
        "Sikap Spiritual",
        style: TextStyleValues.textBold,
      ),
      const SizedBox(height: 16),
      CustomDropdown(
          hint: "Predikat Spiritual",
          items: const ['SB', 'B', 'C', 'K'],
          selectedItem: controller.spiritualGrade.value == ""
              ? null
              : controller.spiritualGrade.value,
          onChanged: (value) {
            controller.spiritualGrade.value = value;
          },
          validator: (value) {
            value ??= "";
            if (value == "") {
              return "Wajib diisi";
            } else {
              return null;
            }
          }),
      const SizedBox(
        height: 16,
      ),
      Text(
        "Selalu Dilakukan",
        style: TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
      ),
      const SizedBox(height: 8),
      Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.all(width: 1.5, color: ColorValues.greyLight),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Column(
          children: [
            controller.spiritualAttitudes.isNotEmpty
                ? ListView.builder(
                    itemCount: controller.spiritualAttitudes.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      var spiritualAttitude = controller.spiritualAttitudes[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(spiritualAttitude.description,
                                      style: TextStyleValues.textRegular),
                                ),
                                Obx(
                                  () => InkWell(
                                    onTap: controller.improveSpiritualAttitudes
                                            .contains(spiritualAttitude.id)
                                        ? null
                                        : () {
                                            controller
                                                .selectAlwaysDoneSpiritualAttitude(
                                                    spiritualAttitude.id!);
                                          },
                                    child: controller.improveSpiritualAttitudes
                                            .contains(spiritualAttitude.id)
                                        ? const Icon(
                                            Icons
                                                .indeterminate_check_box_rounded,
                                            color: ColorValues.grey,
                                          )
                                        : controller
                                                .alwaysDoneSpiritualAttitudes
                                                .contains(spiritualAttitude.id)
                                            ? const Icon(
                                                Icons.check_box_rounded,
                                                color: ColorValues.bluePrimary,
                                              )
                                            : const Icon(
                                                Icons
                                                    .check_box_outline_blank_rounded,
                                                color: ColorValues.grey,
                                              ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    })
                : const CustomEmptyState(),
          ],
        ),
      ),
      const SizedBox(
        height: 16,
      ),
      Text(
        "Mulai Berkembang",
        style: TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
      ),
      const SizedBox(height: 8),
      Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.all(width: 1.5, color: ColorValues.greyLight),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Column(
          children: [
            controller.spiritualAttitudes.isNotEmpty
                ? ListView.builder(
                    itemCount: controller.spiritualAttitudes.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      var spiritualAttitude = controller.spiritualAttitudes[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(spiritualAttitude.description,
                                      style: TextStyleValues.textRegular),
                                ),
                                Obx(
                                  () => InkWell(
                                    onTap: controller
                                            .alwaysDoneSpiritualAttitudes
                                            .contains(spiritualAttitude.id)
                                        ? null
                                        : () {
                                            controller
                                                .selectImproveSpiritualAttitude(
                                                    spiritualAttitude.id!);
                                          },
                                    child: controller
                                            .alwaysDoneSpiritualAttitudes
                                            .contains(spiritualAttitude.id)
                                        ? const Icon(
                                            Icons
                                                .indeterminate_check_box_rounded,
                                            color: ColorValues.grey,
                                          )
                                        : controller.improveSpiritualAttitudes
                                                .contains(spiritualAttitude.id)
                                            ? const Icon(
                                                Icons.check_box_rounded,
                                                color: ColorValues.bluePrimary,
                                              )
                                            : const Icon(
                                                Icons
                                                    .check_box_outline_blank_rounded,
                                                color: ColorValues.grey,
                                              ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    })
                : const CustomEmptyState(),
          ],
        ),
      ),
      const SizedBox(height: 32),
      const Text(
        "Sikap Sosial",
        style: TextStyleValues.textBold,
      ),
      const SizedBox(height: 16),
      CustomDropdown(
          hint: "Predikat Sosial",
          items: const ['SB', 'B', 'C', 'K'],
          selectedItem: controller.sosialGrade.value == ""
              ? null
              : controller.sosialGrade.value,
          onChanged: (value) {
            controller.sosialGrade.value = value;
          },
          validator: (value) {
            value ??= "";
            if (value == "") {
              return "Wajib diisi";
            } else {
              return null;
            }
          }),
      const SizedBox(
        height: 16,
      ),
      Text(
        "Selalu Dilakukan",
        style: TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
      ),
      const SizedBox(height: 8),
      Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.all(width: 1.5, color: ColorValues.greyLight),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Column(
          children: [
            controller.sosialAttitudes.isNotEmpty
                ? ListView.builder(
                    itemCount: controller.sosialAttitudes.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      var sosialAttitude = controller.sosialAttitudes[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(sosialAttitude.description,
                                      style: TextStyleValues.textRegular),
                                ),
                                Obx(
                                  () => InkWell(
                                    onTap: controller.improveSosialAttitudes
                                            .contains(sosialAttitude.id)
                                        ? null
                                        : () {
                                            controller
                                                .selectAlwaysDoneSosialAttitude(
                                                    sosialAttitude.id!);
                                          },
                                    child: controller.improveSosialAttitudes
                                            .contains(sosialAttitude.id)
                                        ? const Icon(
                                            Icons
                                                .indeterminate_check_box_rounded,
                                            color: ColorValues.grey,
                                          )
                                        : controller.alwaysDoneSosialAttitudes
                                                .contains(sosialAttitude.id)
                                            ? const Icon(
                                                Icons.check_box_rounded,
                                                color: ColorValues.bluePrimary,
                                              )
                                            : const Icon(
                                                Icons
                                                    .check_box_outline_blank_rounded,
                                                color: ColorValues.grey,
                                              ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    })
                : const CustomEmptyState(),
          ],
        ),
      ),
      const SizedBox(height: 16),
      Text(
        "Mulai Berkembang",
        style: TextStyleValues.textRegular.copyWith(color: ColorValues.grey),
      ),
      const SizedBox(height: 8),
      Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.all(width: 1.5, color: ColorValues.greyLight),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Column(
          children: [
            controller.sosialAttitudes.isNotEmpty
                ? ListView.builder(
                    itemCount: controller.sosialAttitudes.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      var sosialAttitude = controller.sosialAttitudes[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(sosialAttitude.description,
                                      style: TextStyleValues.textRegular),
                                ),
                                Obx(
                                  () => InkWell(
                                    onTap: controller.alwaysDoneSosialAttitudes
                                            .contains(sosialAttitude.id)
                                        ? null
                                        : () {
                                            controller
                                                .selectImproveSosialAttitude(
                                                    sosialAttitude.id!);
                                          },
                                    child: controller.alwaysDoneSosialAttitudes
                                            .contains(sosialAttitude.id)
                                        ? const Icon(
                                            Icons
                                                .indeterminate_check_box_rounded,
                                            color: ColorValues.grey,
                                          )
                                        : controller.improveSosialAttitudes
                                                .contains(sosialAttitude.id)
                                            ? const Icon(
                                                Icons.check_box_rounded,
                                                color: ColorValues.bluePrimary,
                                              )
                                            : const Icon(
                                                Icons
                                                    .check_box_outline_blank_rounded,
                                                color: ColorValues.grey,
                                              ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    })
                : const CustomEmptyState(),
          ],
        ),
      ),
    ]);
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () {
              if (formKey.currentState!.validate()) {
                controller.saveData();
              }
            },
          ),
        ),
      ],
    );
  }
}
