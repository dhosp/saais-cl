import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/reset_pass/reset_pass_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class ResetPassScreen extends GetView<ResetPassController> {
  const ResetPassScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveLayout(
        potraitLayout: _potraitLayout(),
        landscapeLayout: _landscapeLayout(context),
        greySpace: false,
      ),
    );
  }

  Widget _potraitLayout() {
    return _loginForm();
  }

  Widget _landscapeLayout(context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4), color: ColorValues.white),
        constraints: const BoxConstraints(
          maxWidth: 700,
        ),
        padding: const EdgeInsets.all(32),
        child: _loginForm());
  }

  Widget _loginForm() {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return Column(
      children: [
        Expanded(
            child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // const Text(
              //   'Selamat Datang di Saais',
              //   style: TextStyleValues.textUltraBold,
              // ),
              // const Text(
              //   'Smaisga Academic Information System',
              //   style: TextStyleValues.textMedium,
              // ),
              // const SizedBox(height: 64),
              const Text(
                'Hubungi admin untuk mendapat kode dan melanjutkan',
                style: TextStyleValues.textRegular,
              ),
              const SizedBox(height: 16),
              CustomTextInput(
                controller: controller.usernameController,
                label: 'NIPD/Username',
                prefixIcon: const Icon(Icons.person),
                validator: (value) {
                  value = value ?? "";
                  if (value.length < 4) {
                    return "Diisi minimal 4 karakter";
                  } else {
                    return null;
                  }
                },
              ),
              const SizedBox(height: 16),
              CustomTextInput(
                controller: controller.resetCodeController,
                label: 'Kode Reset',
                prefixIcon: const Icon(Icons.pin),
                validator: (value) {
                  value = value ?? "";
                  if (value == "") {
                    return "Wajib Diisi";
                  } else {
                    return null;
                  }
                },
              ),
              const SizedBox(height: 16),
              Obx(
                () => CustomTextInput(
                  controller: controller.passwordController,
                  isHideText: controller.hideTextPassword.value,
                  label: 'Password Baru',
                  prefixIcon: const Icon(Icons.lock_rounded),
                  suffixIcon: controller.hideTextPassword.value
                      ? const Icon(Icons.visibility_off_rounded)
                      : const Icon(Icons.visibility_rounded),
                  onTapSuffixIcon: () => controller.hideTextPassword.value =
                      !controller.hideTextPassword.value,
                  validator: (value) {
                    value = value ?? "";
                    if (value.length < 6) {
                      return "Diisi minimal 6 karakter";
                    } else {
                      return null;
                    }
                  },
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                      onTap: () {
                        Get.toNamed(Routes.login);
                      },
                      child: Text(
                        "Masuk Ke Sistem",
                        style: TextStyleValues.textRegular
                            .copyWith(color: ColorValues.bluePrimary),
                      ))
                ],
              )
            ],
          ),
        )),
        CustomButton(
          title: "Lanjutkan",
          action: () {
            if (formKey.currentState!.validate()) {
              controller.resetPass();
            }
          },
        ),
      ],
    );
  }
}
