import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/auth_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/routes/app_pages.dart';

class ResetPassController extends GetxController {
  final _authAPI = Get.find<AuthAPI>();
  final usernameController = TextEditingController();
  final resetCodeController = TextEditingController();
  final passwordController = TextEditingController();
  final hideTextPassword = true.obs;

  void resetPass() async {
    CustomLoading.showLoading();
    var result = await _authAPI.resetPass(usernameController.text,
        resetCodeController.text, passwordController.text);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      Get.offAllNamed(Routes.login);
    }
  }
}
