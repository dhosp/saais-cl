import 'package:get/get.dart';
import 'package:saais_client/apis/auth_api.dart';
import 'package:saais_client/modules/reset_pass/reset_pass_controller.dart';

class ResetPassBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthAPI());
    Get.lazyPut(() => ResetPassController());
  }
}
