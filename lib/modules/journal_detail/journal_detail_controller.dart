import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/journal_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/class_attendance.dart';
import 'package:saais_client/models/class_session.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/get_back_route.dart';

class JournalDetailController extends GetxController {
  final _classAPI = Get.find<ClassAPI>();
  final _journalAPI = Get.find<JournalAPI>();
  final _userAPI = Get.find<UserAPI>();

  final _journalId = int.tryParse(Get.parameters["journalId"].toString()) ?? 0;
  var journal = ClassSession(id: 0, class_attendences: []).obs;
  var classroom = Class(id: 0, name: "", grade: "", major_id: 0).obs;
  var teacher = User(id: 0, name: "", user_roles: []).obs;
  final noteController = TextEditingController(text: "");

  final startedAtController = TextEditingController(text: "");
  var startedAt = DateTime.tryParse("").obs;

  final finishedAtController = TextEditingController(text: "");
  var finishedAt = DateTime.tryParse("").obs;

  // var for modal
  final searchDialogController = TextEditingController(text: "");
  List<ClassAttendance> allStudents =
      List<ClassAttendance>.empty(growable: true).obs;
  List<ClassAttendance> currentStudents =
      List<ClassAttendance>.empty(growable: true).obs;

  var selectedStudentIds = [0].obs;
  var tempSelectedStudentIds = [0].obs;
  // end modal var

  void beforeStart() {
    if (_journalId > 0) {
      getData();
    }
  }

  void getData() async {
    selectedStudentIds.clear();
    tempSelectedStudentIds.clear();
    allStudents.clear();
    currentStudents.clear();
    if (Get.arguments == null) {
      var result = await _journalAPI.findOne(_journalId);
      if (result.body?["status"] ?? false) {
        journal.value = ClassSession.fromJson(result.body["data"]);
      }
    } else {
      journal.value = Get.arguments;
    }

    var classResult = await _classAPI.findOne(journal.value.subject!.class_id);
    if (classResult.body?["status"] ?? false) {
      classroom.value = Class.fromJson(classResult.body["data"]);
    }

    teacher.value = journal.value.user!;
    noteController.text = journal.value.lesson_note;

    if (journal.value.started_at != null) {
      startedAtController.text = Formatter.dateFormatter(
          date: journal.value.started_at!, type: "displayDateTime");
      startedAt.value = journal.value.started_at;
    }

    if (journal.value.finished_at != null) {
      finishedAtController.text = Formatter.dateFormatter(
          date: journal.value.finished_at!, type: "displayDateTime");
      finishedAt.value = journal.value.finished_at;
    }

    currentStudents.assignAll(journal.value.class_attendences);
    for (var item in currentStudents) {
      selectedStudentIds.add(item.student_id);
    }

    tempSelectedStudentIds.assignAll(selectedStudentIds);
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result = await _journalAPI.update(
        id: _journalId,
        teacher_id: teacher.value.id,
        lesson_note: noteController.text,
        started_at: startedAt.value!,
        finished_at: finishedAt.value!);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "6", "tab": "0"});
    }
  }

  Future<List<dynamic>> getTeachers(String? filter) async {
    List<User> teachers = [];
    var findAllResult =
        await _userAPI.findAll(role: "teacher", search: filter ?? "");
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        teachers.add(User.fromJson(item));
      }
    }

    return teachers;
  }

  startedAtCallback(DateTime date) {
    startedAt.value = date;
  }

  finishedAtCallback(DateTime date) {
    finishedAt.value = date;
  }

  // func for modal
  void getAllStudents({String search = ""}) async {
    var allStudentFindAllResult = await _userAPI.findAll(
        role: "student", search: search, class_id: classroom.value.id);

    List<dynamic> resultData = allStudentFindAllResult.body["data"];

    allStudents.clear();
    for (var item in resultData) {
      if (!selectedStudentIds.contains(item["id"])) {
        item["student_id"] = item["id"];
        item["user"] = item;
        item["id"] = 0;
        allStudents.add(ClassAttendance.fromJson(item));
      }
    }
  }

  void selectStudentIds(int id) {
    int index = tempSelectedStudentIds.indexOf(id);
    if (index >= 0) {
      tempSelectedStudentIds.removeAt(index);
    } else {
      tempSelectedStudentIds.add(id);
    }
  }

  void updateStudent() async {
    CustomLoading.showLoading();
    Response result = await _journalAPI.updateStudents(
        id: _journalId, student_ids: tempSelectedStudentIds);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      getData();
      Get.close(1);
    }
  }

  void clearTempSelectedStudent() {
    tempSelectedStudentIds.assignAll(selectedStudentIds);
  }

  void removeStudent(int id) async {
    CustomLoading.showLoading();
    selectedStudentIds.remove(id);

    Response result = await _journalAPI.updateStudents(
        id: _journalId, student_ids: selectedStudentIds);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      getData();
      Get.close(1);
    }
  }
  // end func modal
}
