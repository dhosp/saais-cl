import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_date_picker.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/major_enum.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/journal_detail/journal_detail_controller.dart';
import 'package:saais_client/modules/class_detail/class_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class JournalDetailScreen extends GetView<JournalDetailController> {
  const JournalDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Jurnal Kelas",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return const Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // children: [
        //   _formGroup1(),
        //   const SizedBox(
        //     height: 16,
        //   ),
        //   _formGroup2()
        // ],
        );
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(
                () => Form(
                    key: formKey,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: _formGroup1()),
                        const SizedBox(width: 16),
                        Expanded(child: _formGroup2()),
                      ],
                    )),
              ),
              const SizedBox(height: 32),
              _actionButton(formKey),
            ],
          ),
        ),
        Column(
          children: [
            const SizedBox(height: 16),
            Container(
              padding: const EdgeInsets.all(32),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: ColorValues.white,
              ),
              constraints: const BoxConstraints(maxWidth: 1000),
              child: _listStudent(),
            )
          ],
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomDropdown(
          selectedItem: controller.classroom.value,
          itemToString: (item) => Class.getDropdownLabel(item),
          enabled: false,
        ),
        const SizedBox(
          height: 16,
        ),
        CustomDropdown(
          selectedItem: controller.journal.value.subject,
          itemToString: (item) => Subject.getDropdownLabel(item),
          enabled: false,
        ),
        const SizedBox(
          height: 16,
        ),
        CustomDropdown(
          hint: "Guru",
          asyncItems: (String filter) => controller.getTeachers(filter),
          isFilterOnline: true,
          showSearchBox: true,
          selectedItem: controller.teacher.value,
          itemToString: (item) => User.getDropdownLabel(item),
          onChanged: (value) {
            controller.teacher = value;
          },
          validator: (value) {
            if (value == null) {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: CustomDatePicker(
                  label: "Mulai Pada",
                  controller: controller.startedAtController,
                  dateValueCallback: controller.startedAtCallback,
                  date: controller.startedAt.value,
                  validator: (value) {
                    if (value == "") {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDatePicker(
                  label: "Selesai Pada",
                  controller: controller.finishedAtController,
                  dateValueCallback: controller.finishedAtCallback,
                  date: controller.finishedAt.value,
                  validator: (value) {
                    if (value == "") {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        CustomTextInput(
            controller: controller.noteController,
            label: "Catatan Materi",
            maxLines: 5,
            validator: (value) {
              if ((value ??= "") == "") {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
      ],
    );
  }

  Widget _listStudent() {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Daftar Siswa",
            style: TextStyleValues.textBold,
          ),
          CustomButton(
            title: "Tambah Siswa",
            icon: Icons.add,
            action: () {
              CustomModal.showModal(
                  mainContent: _addStudentModalContent(),
                  onInit: controller.getAllStudents,
                  confirmAction: () {
                    if (!listEquals(controller.tempSelectedStudentIds,
                        controller.selectedStudentIds)) {
                      controller.updateStudent();
                      controller.searchDialogController.clear();
                    }
                  },
                  dismissAction: () {
                    controller.clearTempSelectedStudent();
                    controller.searchDialogController.clear();
                  });
            },
          )
        ],
      ),
      const SizedBox(height: 16),
      Row(
        children: [
          Expanded(
            child: CustomTextInput(
              label: "Cari NIPD/NISN/Nama Siswa",
              suffixIcon: const Icon(Icons.search),
              onTapSuffixIcon: () {},
              onFieldSubmitted: (value) {},
            ),
          ),
        ],
      ),
      const SizedBox(
        height: 16,
      ),
      Column(
        children: [
          Obx(() => controller.currentStudents.isNotEmpty
              ? ListView.builder(
                  itemCount: controller.currentStudents.length,
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                    "${controller.currentStudents[i].user!.username} - ${controller.currentStudents[i].user!.name} ",
                                    style: TextStyleValues.textRegular),
                              ),
                              InkWell(
                                onTap: () {
                                  CustomDialog.showDialog(
                                      title: "Konfirmasi Hapus Siswa",
                                      message:
                                          "Menghapus siswa berarti menghapus semua riwayat pembelajaran. Apakah kamu yakin?",
                                      confirmText: "Ya, yakin",
                                      confirmAction: () {
                                        controller.removeStudent(controller
                                            .currentStudents[i].student_id);
                                      });
                                },
                                child: const Icon(
                                  Icons.delete_forever_rounded,
                                  color: ColorValues.red,
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        )
                      ],
                    );
                  })
              : const CustomEmptyState()),
        ],
      )
    ]);
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () => {
              if (formKey.currentState!.validate()) {controller.saveData()}
            },
          ),
        ),
      ],
    );
  }

  Widget _addStudentModalContent() {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Siswa",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              flex: 3,
              child: CustomTextInput(
                  controller: controller.searchDialogController,
                  label: "Cari NIPD/Nama Siswa",
                  suffixIcon: const Icon(Icons.search),
                  onTapSuffixIcon: () {
                    controller.getAllStudents(
                        search: controller.searchDialogController.text);
                  },
                  onFieldSubmitted: (value) {
                    controller.getAllStudents(search: value);
                  }),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Obx(() => controller.allStudents.isNotEmpty
            ? ListView.builder(
                itemCount: controller.allStudents.length,
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          controller.selectStudentIds(
                              controller.allStudents[i].student_id);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                    "${controller.allStudents[i].user!.username} - ${controller.allStudents[i].user!.name} ",
                                    style: TextStyleValues.textRegular),
                              ),
                              Obx(() => controller.tempSelectedStudentIds
                                      .contains(
                                          controller.allStudents[i].student_id)
                                  ? const Icon(
                                      Icons.check_box_rounded,
                                      color: ColorValues.bluePrimary,
                                    )
                                  : const Icon(
                                      Icons.check_box_outline_blank_rounded,
                                      color: ColorValues.grey,
                                    ))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      )
                    ],
                  );
                })
            : const CustomEmptyState()),
      ],
    );
  }
}
