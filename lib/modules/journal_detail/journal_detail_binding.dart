import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/journal_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/journal_detail/journal_detail_controller.dart';
import 'package:saais_client/modules/class_detail/class_detail_controller.dart';

class JournalDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ClassAPI());
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => JournalAPI());
    Get.lazyPut(() => JournalDetailController());
  }
}
