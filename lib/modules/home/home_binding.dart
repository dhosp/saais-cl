import 'package:get/get.dart';
import 'package:saais_client/apis/attitude_api.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/competency_api.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/extracurricular_score_api.dart';
import 'package:saais_client/apis/journal_api.dart';
import 'package:saais_client/apis/report_api.dart';
import 'package:saais_client/apis/schedule_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/teacher_note_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/home/achievement/achievement_controller.dart';
import 'package:saais_client/modules/home/attitude/attitude_controller.dart';
import 'package:saais_client/modules/home/attitude_report/attitude_report_controller.dart';
import 'package:saais_client/modules/home/class/class_controller.dart';
import 'package:saais_client/modules/home/extracurricular/extracurricular_controller.dart';
import 'package:saais_client/modules/home/extracurricular_score/extracurricular_score_controller.dart';
import 'package:saais_client/modules/home/home_controller.dart';
import 'package:saais_client/modules/home/journal/journal_controller.dart';
import 'package:saais_client/modules/home/main_report/main_report_controller.dart';
import 'package:saais_client/modules/home/schedule/schedule_controller.dart';
import 'package:saais_client/modules/home/school_bill/school_bill_controller.dart';
import 'package:saais_client/modules/home/student/student_controller.dart';
import 'package:saais_client/modules/home/student_attendance/student_attendance_controller.dart';
import 'package:saais_client/modules/home/student_dashboard/student_dashboard_controller.dart';
import 'package:saais_client/modules/home/subject/subject_controller.dart';
import 'package:saais_client/modules/home/subject_score/subject_score_controller.dart';
import 'package:saais_client/modules/home/teacher/teacher_controller.dart';
import 'package:saais_client/modules/home/teacher_attendance/teacher_attendance_controller.dart';
import 'package:saais_client/modules/home/teacher_note/teacher_note_controller.dart';
import 'package:saais_client/modules/home/teacher_note_report/teacher_note_report_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => ClassAPI());
    Get.lazyPut(() => SubjectAPI());
    Get.lazyPut(() => JournalAPI());
    Get.lazyPut(() => ExtracurricularAPI());
    Get.lazyPut(() => AttitudeAPI());
    Get.lazyPut(() => ScheduleAPI());
    Get.lazyPut(() => TeacherNoteAPI());
    Get.lazyPut(() => CompetencyAPI());
    Get.lazyPut(() => ExtracurricularScoreAPI());
    Get.lazyPut(() => ReportAPI());
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => StudentController());
    Get.lazyPut(() => TeacherController());
    Get.lazyPut(() => ClassController());
    Get.lazyPut(() => ScheduleController());
    Get.lazyPut(() => SubjectController());
    Get.lazyPut(() => ExtracurricularController());
    Get.lazyPut(() => AttitudeController());
    Get.lazyPut(() => TeacherNoteController());
    Get.lazyPut(() => StudentAttendanceController());
    Get.lazyPut(() => TeacherAttendanceController());
    Get.lazyPut(() => JournalController());
    Get.lazyPut(() => MainReportController());
    Get.lazyPut(() => SubjectScoreController());
    Get.lazyPut(() => AttitudeReportController());
    Get.lazyPut(() => ExtracurricularScoreController());
    Get.lazyPut(() => AchievementController());
    Get.lazyPut(() => TeacherNoteReportController());
    Get.lazyPut(() => SchoolBillController());
    Get.lazyPut(() => StudentDashboardController());
  }
}
