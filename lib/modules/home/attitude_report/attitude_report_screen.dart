import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/home/attitude_report/attitude_report_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class AttitudeReportScreen extends GetView<AttitudeReportController> {
  final String title = "Kelola Nilai Sikap";

  const AttitudeReportScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: controller.getUserList,
          actions: const [
            CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            // const SizedBox(
            //   width: 16,
            // ),
            // CustomButton(
            //   icon: Icons.add,
            //   title: "Tambah",
            //   buttonType: "secondary",
            //   action: () => {
            //     Get.toNamed(
            //       Routes.attitudeReport,
            //       parameters: {
            //         "userId": "0",
            //         "addNew": "true",
            //       },
            //     )
            //   },
            // )
          ],
          filters: [
            Expanded(
                child: CustomDropdown(
              items: const [
                DropdownMenuItem(value: 1, child: Text("X-1")),
                DropdownMenuItem(value: 1, child: Text("X-2")),
              ],
              hint: "Semua Kelas",
              onChanged: (value) => {},
            )),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                items: const [
                  DropdownMenuItem(value: 1, child: Text("Ali")),
                  DropdownMenuItem(value: 1, child: Text("Ahmad")),
                ],
                hint: "Semua Guru",
                onChanged: (value) => {},
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                items: const [
                  DropdownMenuItem(value: 1, child: Text("Matematika")),
                  DropdownMenuItem(value: 1, child: Text("Bahasa Indonesia")),
                ],
                hint: "Semua Mata Pelajaran",
                onChanged: (value) => {},
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: Column(children: [
        Container(
          padding: const EdgeInsets.all(16),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  "Siswa",
                  textAlign: TextAlign.center,
                  style: TextStyleValues.textMedium
                      .copyWith(color: ColorValues.grey),
                ),
              ),
              Expanded(
                child: Text(
                  "Predikat Spiritual",
                  textAlign: TextAlign.center,
                  style: TextStyleValues.textMedium
                      .copyWith(color: ColorValues.grey),
                ),
              ),
              Expanded(
                child: Text(
                  "Predikat Sosial",
                  textAlign: TextAlign.center,
                  style: TextStyleValues.textMedium
                      .copyWith(color: ColorValues.grey),
                ),
              ),
              const Icon(Icons.more_vert, color: Colors.transparent)
            ],
          ),
        ),
        Expanded(
          child: Obx(
            () => ListView.builder(
              itemCount: controller.users.length,
              itemBuilder: (context, i) {
                return InkWell(
                  onTap: () => {
                    Get.toNamed(Routes.attitudeReport,
                        parameters: {
                          "userId": controller.users[i].id.toString(),
                        },
                        arguments: controller.users[i])
                  },
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: ColorValues.white,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                  User.getDropdownLabel(controller.users[i]),
                                  textAlign: TextAlign.center,
                                  style: TextStyleValues.textMedium),
                            ),
                            Expanded(
                              child: Text(
                                  controller.users[i].user_detail
                                          ?.attitude_spiritual ??
                                      "-",
                                  textAlign: TextAlign.center,
                                  style: TextStyleValues.textMedium),
                            ),
                            Expanded(
                              child: Text(
                                  controller.users[i].user_detail
                                          ?.attitude_sosial ??
                                      "-",
                                  textAlign: TextAlign.center,
                                  style: TextStyleValues.textMedium),
                            ),
                            const Icon(Icons.more_vert, color: ColorValues.grey)
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 16,
                      )
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ]))
    ]);
  }
}
