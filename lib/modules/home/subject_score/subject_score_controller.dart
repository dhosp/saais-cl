import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/competency_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/competency.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/constant_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SubjectScoreController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final _classAPI = Get.find<ClassAPI>();
  final _subjectAPI = Get.find<SubjectAPI>();
  final _competencyAPI = Get.find<CompetencyAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  List<User> users = List<User>.empty(growable: true).obs;
  List<Competency> competencies = List<Competency>.empty(growable: true).obs;
  var tableHeaderColumn = [
    Text(
      "Siswa",
      textAlign: TextAlign.center,
      style: TextStyleValues.textMedium.copyWith(color: ColorValues.grey),
    )
  ].obs;

  var classroom = Class().obs;
  var subject = Subject().obs;
  var competencyType = "knowledge".obs;

  void beforeStart() {
    getClasses("");
  }

  void getList() async {
    getCompetencies("");

    var findAllResult = await _userAPI.findAll(
        perPage: displayedData,
        includeScore: true,
        class_id: classroom.value.id,
        subject_id: subject.value.id,
        role: "student");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData.value = findAllResult.body["total"];
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    List<Class> classes = [];

    var findAllResult = await _classAPI.findAll(search: filter ?? "");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classes.add(Class.fromJson(item));
      }

      if (classroom.value.id == 0) {
        classroom.value = classes[0];
        getSubjects("");
      }
    }

    return classes;
  }

  Future<List<dynamic>> getSubjects(String? filter) async {
    List<Subject> subjects = [];

    if (classroom.value.id > 0) {
      var findAllResult = await _subjectAPI.findAll(
          search: filter ?? "", class_id: classroom.value.id);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];

        for (var item in resultData) {
          subjects.add(Subject.fromJson(item));
        }

        if (subject.value.id == 0) {
          subject.value = subjects[0];
          getList();
        }
      }
    }

    return subjects;
  }

  void getCompetencies(String? filter) async {
    if (subject.value.id > 0) {
      var findAllResult = await _competencyAPI.findAll(
          perPage: -1,
          search: filter ?? "",
          subject_id: subject.value.id,
          type: competencyType.value);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];
        competencies.clear();
        tableHeaderColumn.value = [
          Text(
            "Siswa",
            textAlign: TextAlign.center,
            style: TextStyleValues.textMedium.copyWith(color: ColorValues.grey),
          )
        ];
        for (var item in resultData) {
          var currentItem = Competency.fromJson(item);
          competencies.add(currentItem);
          tableHeaderColumn.add(
            Text(
              currentItem.code,
              textAlign: TextAlign.center,
              style:
                  TextStyleValues.textMedium.copyWith(color: ColorValues.grey),
            ),
          );
        }
      }
    }
  }
}
