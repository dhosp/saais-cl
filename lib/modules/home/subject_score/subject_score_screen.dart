import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/modules/home/subject_score/subject_score_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SubjectScoreScreen extends GetView<SubjectScoreController> {
  final String title = "Kelola Nilai Mata Pelajaran";

  const SubjectScoreScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Obx(
      () => Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: controller.getList,
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah/Ubah",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.subjectScore,
                  parameters: {
                    "subjectId": controller.subject.value.id.toString(),
                    "type": controller.competencyType.value,
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: [
            Expanded(
              child: CustomDropdown(
                  hint: "Kelas",
                  showSearchBox: true,
                  isFilterOnline: true,
                  asyncItems: (String filter) => controller.getClasses(filter),
                  itemToString: (item) => Class.getDropdownLabel(item),
                  selectedItem: controller.classroom.value.id == 0
                      ? null
                      : controller.classroom.value,
                  onChanged: (value) {
                    controller.classroom.value = value;
                    controller.users.clear();
                    controller.subject.value = Subject();
                  },
                  validator: (value) {
                    if (value?.id == null) {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                  hint: "Mata Pelajaran",
                  showSearchBox: true,
                  isFilterOnline: true,
                  asyncItems: (String filter) => controller.getSubjects(filter),
                  itemToString: (item) => Subject.getDropdownLabel(item),
                  selectedItem: controller.subject.value.id == 0
                      ? null
                      : controller.subject.value,
                  onChanged: (value) {
                    controller.subject.value = value;
                    controller.getList();
                  },
                  validator: (value) {
                    if (value?.id == null) {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                  hint: "Tipe Kompetensi",
                  items: const ["knowledge", "skill"],
                  selectedItem: controller.competencyType.value,
                  itemToString: (item) {
                    switch (item) {
                      case "knowledge":
                        return "Pengetahuan";
                      case "skill":
                        return "Keterampilan";
                      default:
                        return "";
                    }
                  },
                  onChanged: (value) {
                    controller.competencyType.value = value;
                    controller.getList();
                  },
                  validator: (value) {
                    if (value == "") {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
          ],
        ),
        Expanded(
            child: controller.totalData > 0
                ? Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.all(16),
                          child: Table(
                            columnWidths: const <int, TableColumnWidth>{
                              0: FixedColumnWidth(200),
                            },
                            children: [
                              TableRow(children: controller.tableHeaderColumn)
                            ],
                          )),
                      Expanded(
                        child: ListView.builder(
                          itemCount: controller.users.length,
                          itemBuilder: (context, i) {
                            var currentUser = controller.users[i];

                            var tableContentColumns = [
                              Center(
                                child: Text(currentUser.name,
                                    style: TextStyleValues.textMedium),
                              ),
                            ];

                            for (var item in controller.competencies) {
                              var currentScore = currentUser.competency_scores!
                                  .firstWhereOrNull((scoreItem) =>
                                      scoreItem.competency!.id == item.id);
                              tableContentColumns.add(Center(
                                  child: Text(
                                      currentScore?.score.toString() ?? "-",
                                      style: TextStyleValues.textMedium)));
                            }

                            if (tableContentColumns.length == 1) {
                              tableContentColumns.add(const Center());
                            }

                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: ColorValues.white,
                                  ),
                                  child: Table(
                                    columnWidths: const <int, TableColumnWidth>{
                                      0: FixedColumnWidth(200),
                                    },
                                    children: [
                                      TableRow(children: tableContentColumns)
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  )
                : const CustomEmptyState()),
      ]),
    );
  }
}
