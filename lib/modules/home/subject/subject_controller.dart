import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/values/constant_values.dart';

class SubjectController extends GetxController {
  final _subjectAPI = Get.find<SubjectAPI>();
  final _classAPI = Get.find<ClassAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<Subject> subjects = List<Subject>.empty(growable: true).obs;

  // var for filter
  var classroom = Class(id: 0, name: "", grade: "", major_id: 0).obs;
  // end var for filter

  void beforeStart() {
    getClasses("");
  }

  void getList({int classId = 0}) async {
    isLoading.value = true;
    var findAllResult =
        await _subjectAPI.findAll(perPage: displayedData, class_id: classId);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      subjects.clear();
      for (var item in resultData) {
        subjects.add(Subject.fromJson(item));
      }
    }
    isLoading.value = false;
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    var findAllResult = await _classAPI.findAll(search: filter ?? "");
    List<Class> classrooms = [];

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classrooms.add(Class.fromJson(item));
      }

      if (classroom.value.id == 0 && classrooms.isNotEmpty) {
        classroom.value = classrooms[0];
      }
      getList(classId: classroom.value.id);
    }

    return classrooms;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _subjectAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
