import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/modules/home/subject/subject_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SubjectScreen extends GetView<SubjectController> {
  final String title = "Kelola Mata Pelajaran";

  const SubjectScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: () {
            controller.getList(classId: controller.classroom.value.id);
          },
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Generate",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.subject,
                  parameters: {
                    "subjectId": "0",
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: [
            Expanded(
                child: CustomDropdown(
              asyncItems: (String filter) => controller.getClasses(filter),
              isFilterOnline: true,
              showSearchBox: true,
              selectedItem: controller.classroom.value,
              itemToString: (item) => Class.getDropdownLabel(item),
              onChanged: (value) {
                controller.classroom.value = value;
                controller.getList(classId: value.id);
              },
            )),
            const SizedBox(
              width: 16,
            ),
            const Expanded(
              child: CustomTextInput(
                label: "Cari Mata Pelajaran",
                suffixIcon: Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(() => controller.totalData > 0
              ? Column(children: [
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Nama",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Kode",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Guru",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "KKM",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        const Icon(Icons.more_vert, color: Colors.transparent)
                      ],
                    ),
                  ),
                  Expanded(
                    child: Obx(
                      () => ListView.builder(
                        itemCount: controller.subjects.length,
                        itemBuilder: (context, i) {
                          return InkWell(
                            onTap: () => {
                              CustomMenuDialog.showDialog(menus: [
                                CustomButton(
                                  title: "Lihat Detail",
                                  buttonType: "secondary",
                                  action: () {
                                    Get.back();
                                    Get.toNamed(Routes.subject,
                                        parameters: {
                                          "subjectId": controller.subjects[i].id
                                              .toString(),
                                        },
                                        arguments: controller.subjects[i]);
                                  },
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                CustomButton(
                                  title: "Hapus",
                                  buttonType: "danger",
                                  action: () {
                                    Get.back();
                                    CustomDialog.showDialog(
                                        title:
                                            "Konfirmasi Hapus Mata Pelajaran",
                                        message: "Apakah kamu yakin?",
                                        confirmText: "Ya, yakin",
                                        confirmAction: () {
                                          controller.destroy(
                                              controller.subjects[i].id);
                                        },
                                        dismissAction: () {
                                          Get.back();
                                        });
                                  },
                                ),
                              ])
                            },
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: ColorValues.white,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(controller.subjects[i].name,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(controller.subjects[i].code,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.subjects[i].teacher_name,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.subjects[i].pass_score
                                                .toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      const Icon(Icons.more_vert,
                                          color: ColorValues.grey)
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ])
              : CustomEmptyState(
                  action: () => {
                    Get.toNamed(
                      Routes.subject,
                      parameters: {
                        "subjectId": "0",
                        "addNew": "true",
                      },
                    )
                  },
                )))
    ]);
  }
}
