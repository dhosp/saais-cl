import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class TeacherNoteReportController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final _classAPI = Get.find<ClassAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  List<User> users = List<User>.empty(growable: true).obs;
  var classroom = Class().obs;

  void beforeStart() {
    getClasses("");
  }

  void getList({required int classId}) async {
    var findAllResult = await _userAPI.findAll(
        perPage: displayedData, role: "student", class_id: classId);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    var findAllResult = await _classAPI.findAll(search: filter ?? "");
    List<Class> classrooms = [];

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classrooms.add(Class.fromJson(item));
      }

      if (classroom.value.id == 0 && classrooms.isNotEmpty) {
        classroom.value = classrooms[0];
      }
      getList(classId: classroom.value.id);
    }

    return classrooms;
  }
}
