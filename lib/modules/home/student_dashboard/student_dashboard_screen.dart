import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/modules/home/student_dashboard/student_dashboard_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/utils/user_data.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class StudentDashboardScreen extends GetView<StudentDashboardController> {
  const StudentDashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(),
      landscapeLayout: _potraitLayout(),
      includeScroll: true,
      onRefresh: controller.beforeStart,
    );
  }

  Widget _potraitLayout() {
    return Obx(() {
      var dashboard = controller.dashboard.value;
      return Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              const Text(
                "Informasi Kehadiran",
                style: TextStyleValues.textMedium,
              ),
              Text(
                "Selengkapnya",
                style: TextStyleValues.textRegular
                    .copyWith(color: ColorValues.grey),
              )
            ],
          ),
          const SizedBox(height: 16),
          Row(
            children: [
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.white),
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                      children: [
                        Text(
                          dashboard.school_attendance?.clock_in_at != null
                              ? Formatter.dateFormatter(
                                  date:
                                      dashboard.school_attendance!.clock_in_at!,
                                  type: "displayTime")
                              : "--:--",
                          style: TextStyleValues.textUltraBold,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Jam Masuk",
                          style: TextStyleValues.textRegular
                              .copyWith(color: ColorValues.grey),
                        )
                      ],
                    )),
                    const Icon(
                      Icons.login_rounded,
                      color: ColorValues.grey,
                      size: 48,
                    )
                  ],
                ),
              )),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.white),
                child: Row(
                  children: [
                    Expanded(
                        child: Column(
                      children: [
                        Text(
                          dashboard.school_attendance?.clock_out_at != null
                              ? Formatter.dateFormatter(
                                  date: dashboard
                                      .school_attendance!.clock_out_at!,
                                  type: "displayTime")
                              : "--:--",
                          style: TextStyleValues.textUltraBold,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Jam Pulang",
                          style: TextStyleValues.textRegular
                              .copyWith(color: ColorValues.grey),
                        )
                      ],
                    )),
                    const Icon(
                      Icons.logout_rounded,
                      color: ColorValues.grey,
                      size: 48,
                    )
                  ],
                ),
              )),
            ],
          ),
          const SizedBox(
            height: 32,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              const Text(
                "Jadwal Hari Ini",
                style: TextStyleValues.textMedium,
              ),
              Text(
                "Selengkapnya",
                style: TextStyleValues.textRegular
                    .copyWith(color: ColorValues.grey),
              )
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          dashboard.class_schedules?.isEmpty ?? true
              ? SizedBox(
                  height: 64,
                  child: Center(
                      child: Text(
                    "Belum ada jadwal",
                    style: TextStyleValues.textRegular
                        .copyWith(color: ColorValues.grey),
                  )),
                )
              : ListView.builder(
                  itemCount: dashboard.class_schedules?.length ?? 0,
                  shrinkWrap: true,
                  itemBuilder: ((context, i) {
                    var schedule = dashboard.class_schedules?[i];
                    return Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(16),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: ColorValues.white),
                          child: Row(children: [
                            Expanded(
                                child: Text(
                              "${schedule?.start_time.substring(0, 5)} - ${schedule?.finish_time.substring(0, 5)}",
                              style: TextStyleValues.textRegular,
                            )),
                            const SizedBox(
                              width: 16,
                            ),
                            Expanded(
                                flex: 3,
                                child: Text(
                                  schedule?.subject != null
                                      ? Subject.getDropdownLabel(
                                          schedule!.subject!)
                                      : "-",
                                  style: TextStyleValues.textMedium,
                                ))
                          ]),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                      ],
                    );
                  })),
          const SizedBox(
            height: 32,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              const Text(
                "Hasil Belajar",
                style: TextStyleValues.textMedium,
              ),
              Text(
                "Selengkapnya",
                style: TextStyleValues.textRegular
                    .copyWith(color: ColorValues.grey),
              )
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.white),
                child: Column(
                  children: [
                    Text(
                      dashboard.knowledge_score_avg.toString(),
                      style: TextStyleValues.textUltraBold,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Rata-Rata Pengetahuan",
                      style: TextStyleValues.textRegular
                          .copyWith(color: ColorValues.grey),
                    )
                  ],
                ),
              )),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.white),
                child: Column(
                  children: [
                    Text(
                      dashboard.skill_score_avg.toString(),
                      style: TextStyleValues.textUltraBold,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Rata-Rata Keterampilan",
                      style: TextStyleValues.textRegular
                          .copyWith(color: ColorValues.grey),
                    )
                  ],
                ),
              )),
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          CustomButton(
            title: "Unduh Rapor",
            action: () {
              Get.toNamed(
                Routes.mainReport,
                parameters: {
                  "userId": UserData.getUser().id.toString(),
                },
              );
            },
          ),
          const SizedBox(
            height: 32,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              const Text(
                "Tagihan Keuangan",
                style: TextStyleValues.textMedium,
              ),
              Text(
                "Selengkapnya",
                style: TextStyleValues.textRegular
                    .copyWith(color: ColorValues.grey),
              )
            ],
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            children: [
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.white),
                child: Column(
                  children: [
                    Text(
                      Formatter.numberFormatter(
                        number: dashboard.user?.user_detail?.bill_amount ?? 0,
                      ),
                      style: TextStyleValues.textUltraBold,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Tagihan Bulan Ini",
                      style: TextStyleValues.textRegular
                          .copyWith(color: ColorValues.grey),
                    )
                  ],
                ),
              )),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: ColorValues.white),
                child: Column(
                  children: [
                    Text(
                      dashboard.user?.user_detail?.bill_due_total.toString() ??
                          "-",
                      style: TextStyleValues.textUltraBold,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Tagihan Belum Dibayar",
                      style: TextStyleValues.textRegular
                          .copyWith(color: ColorValues.grey),
                    )
                  ],
                ),
              )),
            ],
          ),
        ],
      );
    });
  }
}
