import 'package:get/get.dart';
import 'package:saais_client/apis/report_api.dart';
import 'package:saais_client/models/student_dashboard.dart';

class StudentDashboardController extends GetxController {
  final _reportAPI = Get.find<ReportAPI>();
  var dashboard = StudentDashboard().obs;

  void beforeStart() {
    getData();
  }

  void getData() async {
    var result = await _reportAPI.getStudentDashboard();
    if (result.body?["status"] ?? false) {
      dashboard.value = StudentDashboard.fromJson(result.body["data"]);
    }
  }
}
