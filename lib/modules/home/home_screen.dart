import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_app_bar.dart';
import 'package:saais_client/modules/home/home_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/utils/user_data.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/image_values.dart';
import 'package:saais_client/values/text_style_values.dart';
import "package:universal_html/html.dart" as html;

class HomeScreen extends GetView<HomeController> {
  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();

    return Obx(
      () => ResponsiveLayout(
        potraitLayout: _potraitLayout(context),
        landscapeLayout: _landscapeLayout(),
        addPadding: false,
      ),
    );
  }

  Widget _potraitLayout(context) {
    final GlobalKey<ScaffoldState> drawerscaffoldkey =
        GlobalKey<ScaffoldState>();

    return Scaffold(
      appBar: CustomAppBar(
        title: "Saais",
        leading: IconButton(
          onPressed: () {
            if (drawerscaffoldkey.currentState!.isDrawerOpen) {
              Navigator.pop(context);
            } else {
              drawerscaffoldkey.currentState!.openDrawer();
            }
          },
          icon: const Icon(Icons.menu),
        ),
      ),
      body: Scaffold(
          key: drawerscaffoldkey,
          drawer: leftDrawer(),
          body: Column(
            children: [
              _mainContent(),
            ],
          )),
    );
  }

  Widget _landscapeLayout() {
    return Scaffold(
        appBar: CustomAppBar(
          title: "Saais - Smaisga Academic Information System",
          leading: Container(
            margin: const EdgeInsets.only(left: 16),
            child: SvgPicture.asset(
              ImageValues.icLogo,
            ),
          ),
        ),
        body: Row(
          children: [leftDrawer(), _mainContent()],
        ));
  }

  Widget _mainContent() {
    var tabs = controller.appMenus[HomeController.activeMenu.value].tabs ?? [];
    var pages =
        controller.appMenus[HomeController.activeMenu.value].pages ?? [];
    var tabController =
        controller.appMenus[HomeController.activeMenu.value].tabController;

    if (tabController != null &&
        HomeController.activeTab <
            (controller
                    .appMenus[HomeController.activeMenu.value].tabs?.length ??
                0)) {
      tabController.animateTo(HomeController.activeTab.value);
    }

    if (tabs.length > 1) {
      return Expanded(
        flex: 1,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: ColorValues.white,
            leading: Container(),
            flexibleSpace: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TabBar(
                  physics: const NeverScrollableScrollPhysics(),
                  controller: tabController,
                  labelColor: ColorValues.black,
                  indicatorColor: ColorValues.bluePrimary,
                  tabs: tabs,
                  onTap: (value) {
                    html.window.history.pushState(null, 'home',
                        '/home?menu=${HomeController.activeMenu.value}&tab=$value');
                    HomeController.activeTab.value = value;
                  },
                ),
              ],
            ),
          ),
          body: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: tabController,
              children: pages),
        ),
      );
    } else if (tabs.length == 1) {
      return Expanded(child: pages[0]);
    } else {
      return Container();
    }
  }

  Widget leftDrawer() {
    return Drawer(
        child: SingleChildScrollView(
            child: Column(
      children: [
        InkWell(
          onTap: () {
            Get.toNamed(Routes.profile);
          },
          child: Container(
            color: ColorValues.white,
            padding: const EdgeInsets.all(32),
            child: Obx(
              () => Row(
                children: [
                  CircleAvatar(
                      backgroundColor: ColorValues.bluePrimary,
                      minRadius: 20,
                      child: Text(controller.user.value.name[0],
                          style: TextStyleValues.textBold
                              .copyWith(color: ColorValues.white))),
                  const SizedBox(width: 16),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(controller.user.value.name,
                          style: TextStyleValues.textMedium),
                      Text(controller.user.value.username,
                          style: TextStyleValues.textRegular),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        const Divider(
          height: 1,
          color: ColorValues.greyLight,
        ),
        ListView.builder(
            shrinkWrap: true,
            itemCount: controller.appMenus.length,
            itemBuilder: (context, i) {
              var menu = controller.appMenus[i];
              var selected =
                  controller.appMenus.indexWhere((element) => element == menu);

              if (menu.title == null && menu.icon == null) {
                return const Divider(height: 1, color: ColorValues.greyLight);
              } else {
                return Obx(() => ListTile(
                    title: Text(menu.title!),
                    titleTextStyle: TextStyleValues.textMedium,
                    leading: menu.icon!,
                    contentPadding: const EdgeInsets.symmetric(horizontal: 32),
                    tileColor: HomeController.activeMenu.value == selected
                        ? ColorValues.bluePrimary
                        : ColorValues.white,
                    textColor: HomeController.activeMenu.value == selected
                        ? ColorValues.white
                        : ColorValues.black,
                    iconColor: HomeController.activeMenu.value == selected
                        ? ColorValues.white
                        : ColorValues.grey,
                    onTap: () {
                      if (selected == controller.appMenus.length - 1) {
                        controller.logout();
                      } else {
                        HomeController.activeMenu.value = selected;
                        HomeController.activeTab.value = 0;

                        html.window.history.pushState(
                            null, 'home', '/home?menu=$selected&tab=0');
                      }
                    }));
              }
            })
      ],
    )));
  }
}
