import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class MainReportController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  List<User> users = List<User>.empty(growable: true).obs;

  void beforeStart() {
    getUserList();
  }

  void getUserList() async {
    var findAllResult =
        await _userAPI.findAll(perPage: displayedData, role: "student");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }
  }
}
