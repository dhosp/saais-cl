import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:saais_client/models/app_menu.dart';
import 'package:saais_client/modules/home/achievement/achievement_screen.dart';
import 'package:saais_client/modules/home/attitude/attitude_screen.dart';
import 'package:saais_client/modules/home/attitude_report/attitude_report_screen.dart';
import 'package:saais_client/modules/home/class/class_screen.dart';
import 'package:saais_client/modules/home/dashboard/dashboard_screen.dart';
import 'package:saais_client/modules/home/extracurricular/extracurricular_screen.dart';
import 'package:saais_client/modules/home/extracurricular_score/extracurricular_score_screen.dart';
import 'package:saais_client/modules/home/journal/journal_screen.dart';
import 'package:saais_client/modules/home/main_report/main_report_screen.dart';
import 'package:saais_client/modules/home/schedule/schedule_screen.dart';
import 'package:saais_client/modules/home/school_bill/school_bill_screen.dart';
import 'package:saais_client/modules/home/student/student_screen.dart';
import 'package:saais_client/modules/home/student_attendance/student_attendance_screen.dart';
import 'package:saais_client/modules/home/student_dashboard/student_dashboard_screen.dart';
import 'package:saais_client/modules/home/subject/subject_screen.dart';
import 'package:saais_client/modules/home/subject_score/subject_score_screen.dart';
import 'package:saais_client/modules/home/teacher/teacher_screen.dart';
import 'package:saais_client/modules/home/teacher_attendance/teacher_attendance_screen.dart';
import 'package:saais_client/modules/home/teacher_note/teacher_note_screen.dart';
import 'package:saais_client/modules/home/teacher_note_report/teacher_note_report_screen.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/user_data.dart';

class HomeController extends GetxController with GetTickerProviderStateMixin {
  late TickerProvider vsync;
  static final activeMenu = 0.obs;
  static final activeTab = 0.obs;
  final user = UserData.getUser().obs;

  late List<AppMenu> appMenus = [];

  void beforeStart() {
    vsync = this;

    var isStudent = false;
    var isSuperadmin = false;

    for (var item in user.value.user_roles) {
      if (item.code == "student") {
        isStudent = true;
        break;
      } else if (item.code == "superadmin") {
        isSuperadmin = true;
        break;
      }
    }

    if (isSuperadmin) {
      appMenus = [
        AppMenu(
            icon: const Icon(Icons.dashboard_rounded),
            title: "Dashboard",
            tabs: [
              const Tab(text: "Dashboard"),
            ],
            pages: [
              const DashboardScreen()
            ]),
        AppMenu(
            icon: const Icon(Icons.co_present),
            title: "Data Pengguna",
            tabs: [
              const Tab(text: "Siswa"),
              const Tab(text: "Guru dan Karyawan")
            ],
            pages: [const StudentScreen(), const TeacherScreen()],
            tabController: TabController(length: 2, vsync: vsync)),
        AppMenu(
            icon: const Icon(Icons.calendar_month),
            title: "Kelas dan Jadwal",
            tabs: [const Tab(text: "Kelas"), const Tab(text: "Jadwal")],
            pages: [const ClassScreen(), const ScheduleScreen()],
            tabController: TabController(length: 2, vsync: vsync)),
        AppMenu(
            icon: const Icon(Icons.collections_bookmark_rounded),
            title: "Mapel dan Kurikulum",
            tabs: [
              const Tab(text: "Mata Pelajaran"),
              const Tab(text: "Ekstrakurikuler"),
              const Tab(text: "Sikap"),
              const Tab(text: "Catatan Wali Kelas")
            ],
            pages: [
              const SubjectScreen(),
              const ExtracurricularScreen(),
              const AttitudeScreen(),
              const TeacherNoteScreen(),
            ],
            tabController: TabController(length: 4, vsync: vsync)),
        AppMenu(),
        AppMenu(
            icon: const Icon(Icons.access_time),
            title: "Kehadiran Harian",
            tabs: [
              const Tab(text: "Kehadiran Siswa"),
              const Tab(text: "Kehadiran Guru dan Karyawan")
            ],
            pages: [
              const StudentAttendanceScreen(),
              const TeacherAttendanceScreen()
            ],
            tabController: TabController(length: 2, vsync: vsync)),
        AppMenu(
            icon: const Icon(Icons.note_alt_outlined),
            title: "Jurnal Kelas",
            tabs: [const Tab(text: "Jurnal Kelas")],
            pages: [const JournalScreen()]),
        AppMenu(
            icon: const Icon(Icons.auto_graph),
            title: "Penilaian",
            tabs: [
              const Tab(text: "Rapor"),
              const Tab(text: "Nilai Mapel"),
              const Tab(text: "Nilai Sikap"),
              const Tab(text: "Nilai Ekstrakurikuler"),
              const Tab(text: "Prestasi"),
              const Tab(text: "Catatan Wali Kelas")
            ],
            pages: [
              const MainReportScreen(),
              const SubjectScoreScreen(),
              const AttitudeReportScreen(),
              const ExtracurricularScoreScreen(),
              const AchievementScreen(),
              const TeacherNoteReportScreen()
            ],
            tabController: TabController(length: 6, vsync: vsync)),
        AppMenu(
            icon: const Icon(Icons.payments_outlined),
            title: "Tagihan Keuangan",
            tabs: [const Tab(text: "Tagihan Keuangan")],
            pages: [const SchoolBillScreen()]),
        AppMenu(),
        AppMenu(icon: const Icon(Icons.settings_outlined), title: "Pengaturan"),
        AppMenu(icon: const Icon(Icons.logout_rounded), title: "Keluar")
      ];
    } else if (isStudent) {
      appMenus = [
        AppMenu(
            icon: const Icon(Icons.dashboard_rounded),
            title: "Dashboard",
            tabs: [
              const Tab(text: "Dashboard"),
            ],
            pages: [
              const StudentDashboardScreen()
            ]),
        // AppMenu(
        //     icon: const Icon(Icons.access_time),
        //     title: "Kehadiran Harian",
        //     tabs: [
        //       const Tab(text: "Kehadiran Siswa"),
        //       const Tab(text: "Kehadiran Guru dan Karyawan")
        //     ],
        //     pages: [
        //       const StudentAttendanceScreen(),
        //       const TeacherAttendanceScreen()
        //     ],
        //     tabController: TabController(length: 2, vsync: vsync)),
        // AppMenu(
        //     icon: const Icon(Icons.calendar_month),
        //     title: "Kelas dan Jadwal",
        //     tabs: [const Tab(text: "Kelas"), const Tab(text: "Jadwal")],
        //     pages: [const ClassScreen(), const ScheduleScreen()],
        //     tabController: TabController(length: 2, vsync: vsync)),
        // AppMenu(
        //     icon: const Icon(Icons.auto_graph),
        //     title: "Penilaian",
        //     tabs: [
        //       const Tab(text: "Rapor"),
        //       const Tab(text: "Nilai Mapel"),
        //       const Tab(text: "Nilai Sikap"),
        //       const Tab(text: "Nilai Ekstrakurikuler"),
        //       const Tab(text: "Prestasi"),
        //       const Tab(text: "Catatan Wali Kelas")
        //     ],
        //     pages: [
        //       const MainReportScreen(),
        //       const SubjectScoreScreen(),
        //       const AttitudeReportScreen(),
        //       const ExtracurricularScoreScreen(),
        //       const AchievementScreen(),
        //       const TeacherNoteReportScreen()
        //     ],
        //     tabController: TabController(length: 6, vsync: vsync)),
        // AppMenu(
        //     icon: const Icon(Icons.payments_outlined),
        //     title: "Tagihan Keuangan",
        //     tabs: [const Tab(text: "Tagihan Keuangan")],
        //     pages: [const SchoolBillScreen()]),
        AppMenu(),
        // AppMenu(icon: const Icon(Icons.settings_outlined), title: "Pengaturan"),
        AppMenu(icon: const Icon(Icons.logout_rounded), title: "Keluar")
      ];
    }
  }

  void logout() {
    GetStorage().erase();
    Get.offAllNamed(Routes.login);
  }
}
