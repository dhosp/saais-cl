import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class TeacherController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<User> users = List<User>.empty(growable: true).obs;

  void beforeStart() {
    getUserList();
  }

  void getUserList() async {
    isLoading.value = true;
    var findAllResult = await _userAPI.findAll(
        perPage: displayedData,
        role: "teacher,principal,homeroom_teacher,finance");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }

    isLoading.value = false;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _userAPI.destroy(id);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
