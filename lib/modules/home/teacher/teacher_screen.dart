import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/gender_enum.dart';
import 'package:saais_client/modules/home/teacher/teacher_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class TeacherScreen extends GetView<TeacherController> {
  final String title = "Kelola Guru dan Karyawan";

  const TeacherScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: controller.getUserList,
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.teacher,
                  parameters: {
                    "userId": "0",
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: const [
            Expanded(
              child: CustomTextInput(
                label: "Cari Nama Guru",
                suffixIcon: Icon(Icons.search),
              ),
            ),
            // const SizedBox(
            //   width: 16,
            // ),
            // Expanded(
            //   child: CustomDropdown(
            //     items: const [
            //       DropdownMenuItem(value: 1, child: Text("X-1")),
            //       DropdownMenuItem(value: 1, child: Text("X-2")),
            //     ],
            //     hint: "Semua Kelas",
            //     onChanged: (value) => {},
            //   ),
            // ),
            // const SizedBox(
            //   width: 16,
            // ),
            // Expanded(
            //   child: CustomDropdown(
            //     items: const [
            //       DropdownMenuItem(value: 1, child: Text("X-1")),
            //       DropdownMenuItem(value: 1, child: Text("X-2")),
            //     ],
            //     hint: "Siswa Aktif",
            //     onChanged: (value) => {},
            //   ),
            // ),
          ],
        ),
      ),
      Expanded(
          child: Obx(() => controller.totalData > 0
              ? Column(children: [
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Nama",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Username",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),

                        Expanded(
                          child: Text(
                            "Jenis Kelamin",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        // Expanded(
                        //   child: Text(
                        //     "Kelas",
                        //     textAlign: TextAlign.center,
                        //     style: TextStyleValues.textMedium.copyWith(
                        //       color: ColorValues.grey,
                        //     ),
                        //   ),
                        // ),
                        const Icon(Icons.more_vert, color: Colors.transparent)
                      ],
                    ),
                  ),
                  Expanded(
                    child: Obx(
                      () => ListView.builder(
                        itemCount: controller.users.length,
                        itemBuilder: (context, i) {
                          return InkWell(
                            onTap: () => {
                              CustomMenuDialog.showDialog(menus: [
                                CustomButton(
                                  title: "Lihat Detail",
                                  buttonType: "secondary",
                                  action: () {
                                    Get.back();
                                    Get.toNamed(Routes.teacher,
                                        parameters: {
                                          "userId":
                                              controller.users[i].id.toString(),
                                        },
                                        arguments: controller.users[i]);
                                  },
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                CustomButton(
                                  title: "Hapus",
                                  buttonType: "danger",
                                  action: () {
                                    Get.back();
                                    CustomDialog.showDialog(
                                        title: "Konfirmasi Hapus Guru",
                                        message: "Apakah kamu yakin?",
                                        confirmText: "Ya, yakin",
                                        confirmAction: () {
                                          controller
                                              .destroy(controller.users[i].id);
                                        },
                                        dismissAction: () {
                                          Get.back();
                                        });
                                  },
                                ),
                              ])
                            },
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: ColorValues.white,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(controller.users[i].name,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.users[i].username,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            GenderEnum.getDropdownLabel(
                                                gender: controller.users[i]
                                                        .user_detail?.gender ??
                                                    ""),
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      const Icon(Icons.more_vert,
                                          color: ColorValues.grey)
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ])
              : CustomEmptyState(
                  action: () => {
                    Get.toNamed(
                      Routes.teacher,
                      parameters: {
                        "userId": "0",
                        "addNew": "true",
                      },
                    )
                  },
                )))
    ]);
  }
}
