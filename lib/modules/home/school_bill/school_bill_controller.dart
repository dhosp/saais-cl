import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class SchoolBillController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<User> students = List<User>.empty(growable: true).obs;

  void beforeStart() {
    getList();
  }

  void getList() async {
    isLoading.value = true;
    var findAllResult = await _userAPI.findAll(
        perPage: displayedData, role: "student", includeBill: true);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      students.clear();
      for (var item in resultData) {
        students.add(User.fromJson(item));
      }
    }
    isLoading.value = false;
  }
}
