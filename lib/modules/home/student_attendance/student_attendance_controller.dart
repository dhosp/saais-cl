import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class StudentAttendanceController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final _classAPI = Get.find<ClassAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<User> users = List<User>.empty(growable: true).obs;

  // var for filter
  var classroom = Class(id: 0, name: "", grade: "", major_id: 0).obs;
  // end var for filter

  void beforeStart() {
    classroom.value.id = 0;
    getClasses("");
  }

  void getList({int classId = 0}) async {
    isLoading.value = true;
    var findAllResult = await _userAPI.findAll(
        perPage: displayedData,
        role: "student",
        includeAttendance: true,
        class_id: classId);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }
    isLoading.value = false;
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    var findAllResult = await _classAPI.findAll(search: filter ?? "");
    List<Class> classrooms = [];

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classrooms.add(Class.fromJson(item));
      }

      if (classroom.value.id == 0 && classrooms.isNotEmpty) {
        classroom.value = classrooms[0];
      }

      getList(classId: classroom.value.id);
    }

    return classrooms;
  }
}
