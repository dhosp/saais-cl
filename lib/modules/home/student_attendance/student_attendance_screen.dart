import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/modules/home/student_attendance/student_attendance_controller.dart';
import 'package:saais_client/routes/app_pages.dart';

import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class StudentAttendanceScreen extends GetView<StudentAttendanceController> {
  final String title = "Kelola Kehadiran Siswa";

  const StudentAttendanceScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: () {
            controller.getList(classId: controller.classroom.value.id);
          },
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            // const SizedBox(
            //   width: 16,
            // ),
            // CustomButton(
            //   icon: Icons.add,
            //   title: "Tambah",
            //   buttonType: "secondary",
            //   action: () => {
            //     Get.toNamed(
            //       Routes.student,
            //       parameters: {
            //         "userId": "0",
            //         "addNew": "true",
            //       },
            //     )
            //   },
            // )
          ],
          filters: [
            Expanded(
                child: CustomDropdown(
              asyncItems: (String filter) => controller.getClasses(filter),
              isFilterOnline: true,
              showSearchBox: true,
              selectedItem: controller.classroom.value,
              itemToString: (item) => Class.getDropdownLabel(item),
              onChanged: (value) {
                controller.classroom.value = value;
                controller.getList(classId: value.id);
              },
            )),
            const SizedBox(
              width: 16,
            ),
            const Expanded(
              child: CustomTextInput(
                label: "Cari Nama Siswa",
                suffixIcon: Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(
        () => controller.totalData > 0
            ? Column(children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Nama",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Hadir",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Izin",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Sakit",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Tanpa Keterangan",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      const Icon(Icons.more_vert, color: Colors.transparent)
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: controller.users.length,
                    itemBuilder: (context, i) {
                      return InkWell(
                        onTap: () => {
                          Get.toNamed(Routes.studentAttendance,
                              parameters: {
                                "userId": controller.users[i].id.toString(),
                              },
                              arguments: controller.users[i])
                        },
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: ColorValues.white,
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(controller.users[i].name,
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  Expanded(
                                    child: Text(
                                        controller.users[i].user_detail
                                                ?.attendance_h_total
                                                .toString() ??
                                            "-",
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  Expanded(
                                    child: Text(
                                        controller.users[i].user_detail
                                                ?.attendance_i_total
                                                .toString() ??
                                            "-",
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  Expanded(
                                    child: Text(
                                        controller.users[i].user_detail
                                                ?.attendance_s_total
                                                .toString() ??
                                            "-",
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  Expanded(
                                    child: Text(
                                        controller.users[i].user_detail
                                                ?.attendance_a_total
                                                .toString() ??
                                            "-",
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  const Icon(Icons.more_vert,
                                      color: ColorValues.grey)
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ])
            : CustomEmptyState(),
      ))
    ]);
  }
}
