import 'package:get/get.dart';
import 'package:saais_client/apis/teacher_note_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/teacher_note.dart';
import 'package:saais_client/values/constant_values.dart';

class TeacherNoteController extends GetxController {
  final _teacherNoteAPI = Get.find<TeacherNoteAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  List<TeacherNote> teacherNotes = List<TeacherNote>.empty(growable: true).obs;

  void beforeStart() {
    getList();
  }

  void getList() async {
    var findAllResult = await _teacherNoteAPI.findAll(perPage: displayedData);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      teacherNotes.clear();
      for (var item in resultData) {
        teacherNotes.add(TeacherNote.fromJson(item));
      }
    }
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _teacherNoteAPI.destroy(id);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
