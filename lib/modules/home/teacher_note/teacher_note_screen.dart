import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/home/teacher_note/teacher_note_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class TeacherNoteScreen extends GetView<TeacherNoteController> {
  final String title = "Kelola Catatan Wali Kelas";

  const TeacherNoteScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: controller.getList,
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.teacherNote,
                  parameters: {
                    "noteId": "0",
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: const [
            Expanded(
              child: CustomTextInput(
                label: "Cari Catatan Wali Kelas",
                suffixIcon: Icon(Icons.search),
              ),
            ),
            SizedBox(
              width: 16,
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(
        () => controller.totalData > 0
            ? Column(children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  child: const Row(
                    children: [
                      // Expanded(
                      //   child: Text(
                      //     "Sikap",
                      //     textAlign: TextAlign.center,
                      //     style: TextStyleValues.textMedium
                      //         .copyWith(color: ColorValues.grey),
                      //   ),
                      // ),
                      // const Icon(Icons.more_vert, color: Colors.transparent)
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: controller.teacherNotes.length,
                    itemBuilder: (context, i) {
                      return InkWell(
                        onTap: () => {
                          CustomMenuDialog.showDialog(menus: [
                            CustomButton(
                              title: "Lihat Detail",
                              buttonType: "secondary",
                              action: () {
                                Get.back();
                                Get.toNamed(Routes.teacherNote,
                                    parameters: {
                                      "noteId": controller.teacherNotes[i].id
                                          .toString(),
                                    },
                                    arguments: controller.teacherNotes[i]);
                              },
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            CustomButton(
                              title: "Hapus",
                              buttonType: "danger",
                              action: () {
                                Get.back();
                                CustomDialog.showDialog(
                                    title:
                                        "Konfirmasi Hapus Catatan Wali Kelas",
                                    message: "Apakah kamu yakin?",
                                    confirmText: "Ya, yakin",
                                    confirmAction: () {
                                      controller.destroy(
                                          controller.teacherNotes[i].id);
                                    },
                                    dismissAction: () {
                                      Get.back();
                                    });
                              },
                            ),
                          ])
                        },
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: ColorValues.white,
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                        controller.teacherNotes[i].description,
                                        textAlign: TextAlign.left,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  const Icon(Icons.more_vert,
                                      color: ColorValues.grey)
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ])
            : CustomEmptyState(
                action: () {
                  Get.toNamed(
                    Routes.teacherNote,
                    parameters: {
                      "noteId": "0",
                      "addNew": "true",
                    },
                  );
                },
              ),
      ))
    ]);
  }
}
