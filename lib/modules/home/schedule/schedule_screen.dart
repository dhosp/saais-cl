import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/schedule.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/modules/home/schedule/schedule_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class ScheduleScreen extends GetView<ScheduleController> {
  final String title = "Kelola Jadwal";

  const ScheduleScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: controller.getUserList,
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.schedule,
                  parameters: {
                    "scheduleId": "0",
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: [
            Expanded(
                child: CustomDropdown(
              items: const [
                DropdownMenuItem(value: 1, child: Text("X-1")),
                DropdownMenuItem(value: 1, child: Text("X-2")),
              ],
              hint: "Semua Kelas",
              onChanged: (value) => {},
            )),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                items: const [
                  DropdownMenuItem(value: 1, child: Text("Ali")),
                  DropdownMenuItem(value: 1, child: Text("Ahmad")),
                ],
                hint: "Semua Guru",
                onChanged: (value) => {},
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                items: const [
                  DropdownMenuItem(value: 1, child: Text("Matematika")),
                  DropdownMenuItem(value: 1, child: Text("Bahasa Indonesia")),
                ],
                hint: "Semua Mata Pelajaran",
                onChanged: (value) => {},
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(
        () => controller.totalData.value > 0
            ? Column(children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Jam",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Mata Pelajaran",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Kelas",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "Guru",
                          textAlign: TextAlign.center,
                          style: TextStyleValues.textMedium
                              .copyWith(color: ColorValues.grey),
                        ),
                      ),
                      const Icon(Icons.more_vert, color: Colors.transparent)
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: 6,
                      itemBuilder: (context, dayIndex) {
                        if (controller.schedule[dayIndex] != null) {
                          return Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: ColorValues.greyLight,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        Schedule.getDayDropdownLabel(dayIndex),
                                        style: TextStyleValues.textMedium,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              ListView.builder(
                                itemCount:
                                    controller.schedule[dayIndex]!.length,
                                shrinkWrap: true,
                                itemBuilder: (context, i) {
                                  var currentSchedule =
                                      controller.schedule[dayIndex]![i];
                                  return InkWell(
                                    onTap: () => {
                                      CustomMenuDialog.showDialog(menus: [
                                        CustomButton(
                                          title: "Lihat Detail",
                                          buttonType: "secondary",
                                          action: () {
                                            Get.back();
                                            Get.toNamed(Routes.schedule,
                                                parameters: {
                                                  "scheduleId": currentSchedule
                                                      .id
                                                      .toString(),
                                                },
                                                arguments: currentSchedule);
                                          },
                                        ),
                                        const SizedBox(
                                          height: 16,
                                        ),
                                        CustomButton(
                                          title: "Hapus",
                                          buttonType: "danger",
                                          action: () {
                                            Get.back();
                                            CustomDialog.showDialog(
                                                title:
                                                    "Konfirmasi Hapus Jadwal",
                                                message: "Apakah kamu yakin?",
                                                confirmText: "Ya, yakin",
                                                confirmAction: () {
                                                  controller.destroy(
                                                      currentSchedule.id);
                                                },
                                                dismissAction: () {
                                                  Get.back();
                                                });
                                          },
                                        ),
                                      ])
                                    },
                                    child: Column(
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.all(16),
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            color: ColorValues.white,
                                          ),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Text(
                                                    "${currentSchedule.start_time.substring(0, 5)} - ${currentSchedule.finish_time.substring(0, 5)}",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyleValues
                                                        .textMedium),
                                              ),
                                              Expanded(
                                                child: Text(
                                                    Subject.getDropdownLabel(
                                                        currentSchedule
                                                            .subject!),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyleValues
                                                        .textMedium),
                                              ),
                                              Expanded(
                                                child: Text(
                                                    Class.getDropdownLabel(Class(
                                                        id: currentSchedule
                                                            .subject!.class_id,
                                                        name: currentSchedule
                                                            .subject!
                                                            .class_name,
                                                        grade: currentSchedule
                                                            .subject!
                                                            .class_grade,
                                                        major_name: currentSchedule
                                                            .subject!
                                                            .class_major_name)),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyleValues
                                                        .textMedium),
                                              ),
                                              Expanded(
                                                child: Text(
                                                    currentSchedule
                                                        .subject!.user!.name,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyleValues
                                                        .textMedium),
                                              ),
                                              const Icon(Icons.more_vert,
                                                  color: ColorValues.grey)
                                            ],
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 16,
                                        )
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ],
                          );
                        } else {
                          return Container();
                        }
                      }),
                ),
              ])
            : CustomEmptyState(
                action: () {},
              ),
      ))
    ]);
  }
}
