import 'package:get/get.dart';
import 'package:saais_client/apis/schedule_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/schedule.dart';

class ScheduleController extends GetxController {
  final _scheduleAPI = Get.find<ScheduleAPI>();
  final displayedData = -1;
  var totalData = 0.obs;
  var isLoading = false.obs;
  Map<int, List<Schedule>> schedule =
      {0: List<Schedule>.empty(growable: true)}.obs;

  void beforeStart() {
    getUserList();
  }

  void getUserList() async {
    isLoading.value = true;
    var findAllResult = await _scheduleAPI.findAll(perPage: displayedData);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      schedule.clear();
      for (var item in resultData) {
        if (schedule[item["day"]] != null) {
          schedule[item["day"]]!.add(Schedule.fromJson(item));
        } else {
          schedule[item["day"]] = [Schedule.fromJson(item)];
        }
      }
    }
    isLoading.value = false;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _scheduleAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
