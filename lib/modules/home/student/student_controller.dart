import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class StudentController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  final isLoading = false.obs;
  List<User> users = List<User>.empty(growable: true).obs;

  void beforeStart() {
    getList();
  }

  void getList() async {
    isLoading.value = true;
    var findAllResult =
        await _userAPI.findAll(role: "student", perPage: displayedData);

    if (findAllResult.body != null) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }
    isLoading.value = false;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _userAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }

  @override
  dispose() {
    super.dispose();
    users.clear();
  }
}
