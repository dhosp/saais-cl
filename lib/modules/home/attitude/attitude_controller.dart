import 'package:get/get.dart';
import 'package:saais_client/apis/attitude_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/attitude.dart';
import 'package:saais_client/values/constant_values.dart';

class AttitudeController extends GetxController {
  final _attitudeAPI = Get.find<AttitudeAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  List<Attitude> attitudes = List<Attitude>.empty(growable: true).obs;

  // var for filter
  var type = "spiritual".obs;

// end var for filter

  void beforeStart() {
    getList(type.value);
  }

  void getList(String? filter) async {
    var findAllResult =
        await _attitudeAPI.findAll(perPage: displayedData, type: filter ?? "");
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      attitudes.clear();
      for (var item in resultData) {
        attitudes.add(Attitude.fromJson(item));
      }
    }
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _attitudeAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
