import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/home/attitude/attitude_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class AttitudeScreen extends GetView<AttitudeController> {
  final String title = "Kelola Sikap";

  const AttitudeScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: () {
            controller.getList(controller.type.value);
          },
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.attitude,
                  parameters: {
                    "attitudeId": "0",
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: [
            Expanded(
              child: CustomDropdown(
                  hint: "Tipe",
                  items: const ["spiritual", "sosial"],
                  itemToString: (item) {
                    return item.replaceFirst(item[0], item[0].toUpperCase());
                  },
                  selectedItem: controller.type.value,
                  onChanged: (value) {
                    controller.type.value = value;
                    controller.getList(value);
                  },
                  validator: (value) {
                    if (value == null) {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
            const SizedBox(
              width: 16,
            ),
            const Expanded(
              child: CustomTextInput(
                label: "Cari Sikap",
                suffixIcon: Icon(Icons.search),
              ),
            ),
            const SizedBox(
              width: 16,
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(
        () => controller.totalData > 0
            ? Column(children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  // child: Row(
                  // children: [
                  // Expanded(
                  //   child: Text(
                  //     "Sikap",
                  //     textAlign: TextAlign.center,
                  //     style: TextStyleValues.textMedium
                  //         .copyWith(color: ColorValues.grey),
                  //   ),
                  // ),
                  // const Icon(Icons.more_vert, color: Colors.transparent)
                  // ],
                  // ),
                ),
                Expanded(
                  child: Obx(
                    () => ListView.builder(
                      itemCount: controller.attitudes.length,
                      itemBuilder: (context, i) {
                        return InkWell(
                          onTap: () => {
                            CustomMenuDialog.showDialog(menus: [
                              CustomButton(
                                title: "Lihat Detail",
                                buttonType: "secondary",
                                action: () {
                                  Get.back();
                                  Get.toNamed(Routes.attitude,
                                      parameters: {
                                        "attitudeId": controller.attitudes[i].id
                                            .toString(),
                                      },
                                      arguments: controller.attitudes[i]);
                                },
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              CustomButton(
                                title: "Hapus",
                                buttonType: "danger",
                                action: () {
                                  Get.back();
                                  CustomDialog.showDialog(
                                      title: "Konfirmasi Hapus Sikap",
                                      message: "Apakah kamu yakin?",
                                      confirmText: "Ya, yakin",
                                      confirmAction: () {
                                        controller.destroy(
                                            controller.attitudes[i].id);
                                      },
                                      dismissAction: () {
                                        Get.back();
                                      });
                                },
                              ),
                            ])
                          },
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(16),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: ColorValues.white,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                          controller.attitudes[i].description,
                                          textAlign: TextAlign.left,
                                          style: TextStyleValues.textMedium),
                                    ),
                                    const Icon(Icons.more_vert,
                                        color: ColorValues.grey)
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 16,
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ])
            : CustomEmptyState(
                action: () => {
                  Get.toNamed(
                    Routes.attitude,
                    parameters: {
                      "attitudeId": "0",
                      "addNew": "true",
                    },
                  )
                },
              ),
      ))
    ]);
  }
}
