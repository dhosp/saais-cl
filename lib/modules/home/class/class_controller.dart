import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/values/constant_values.dart';

class ClassController extends GetxController {
  final _classAPI = Get.find<ClassAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<Class> classes = List<Class>.empty(growable: true).obs;

  void beforeStart() {
    getList();
  }

  void getList() async {
    isLoading.value = true;
    var findAllResult = await _classAPI.findAll(perPage: displayedData);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      classes.clear();
      for (var item in resultData) {
        classes.add(Class.fromJson(item));
      }
    }
    isLoading.value = false;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _classAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
