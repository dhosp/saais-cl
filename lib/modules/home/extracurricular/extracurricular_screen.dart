import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/home/extracurricular/extracurricular_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class ExtracurricularScreen extends GetView<ExtracurricularController> {
  final String title = "Kelola Ekstrakurikuler";

  const ExtracurricularScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: controller.getList,
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah",
              buttonType: "secondary",
              action: () => {
                Get.toNamed(
                  Routes.extracurricular,
                  parameters: {
                    "exculId": "0",
                    "addNew": "true",
                  },
                )
              },
            )
          ],
          filters: const [
            Expanded(
              child: CustomTextInput(
                label: "Cari Ekstrakurikuler",
                suffixIcon: Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(() => controller.totalData > 0
              ? Column(children: [
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Nama",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Pembina",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        const Icon(Icons.more_vert, color: Colors.transparent)
                      ],
                    ),
                  ),
                  Expanded(
                      child: ListView.builder(
                    itemCount: controller.extracurriculars.length,
                    itemBuilder: (context, i) {
                      return InkWell(
                        onTap: () => {
                          CustomMenuDialog.showDialog(menus: [
                            CustomButton(
                              title: "Lihat Detail",
                              buttonType: "secondary",
                              action: () {
                                Get.back();
                                Get.toNamed(Routes.extracurricular,
                                    parameters: {
                                      "exculId": controller
                                          .extracurriculars[i].id
                                          .toString(),
                                    },
                                    arguments: controller.extracurriculars[i]);
                              },
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            CustomButton(
                              title: "Hapus",
                              buttonType: "danger",
                              action: () {
                                Get.back();
                                CustomDialog.showDialog(
                                    title: "Konfirmasi Hapus Ekstrakurikuler",
                                    message: "Apakah kamu yakin?",
                                    confirmText: "Ya, yakin",
                                    confirmAction: () {
                                      controller.destroy(
                                          controller.extracurriculars[i].id);
                                    },
                                    dismissAction: () {
                                      Get.back();
                                    });
                              },
                            ),
                          ])
                        },
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(16),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: ColorValues.white,
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                        controller.extracurriculars[i].name,
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  Expanded(
                                    child: Text(
                                        controller.extracurriculars[i].user
                                                ?.name ??
                                            "-",
                                        textAlign: TextAlign.center,
                                        style: TextStyleValues.textMedium),
                                  ),
                                  const Icon(Icons.more_vert,
                                      color: ColorValues.grey)
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 16,
                            )
                          ],
                        ),
                      );
                    },
                  )),
                ])
              : CustomEmptyState(
                  action: () => {
                    Get.toNamed(
                      Routes.extracurricular,
                      parameters: {
                        "exculId": "0",
                        "addNew": "true",
                      },
                    )
                  },
                )))
    ]);
  }
}
