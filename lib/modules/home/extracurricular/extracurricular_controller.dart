import 'package:get/get.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/values/constant_values.dart';

class ExtracurricularController extends GetxController {
  final _extracurricularAPI = Get.find<ExtracurricularAPI>();
  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<Extracurricular> extracurriculars =
      List<Extracurricular>.empty(growable: true).obs;

  void beforeStart() {
    getList();
  }

  void getList() async {
    isLoading.value = true;
    var findAllResult =
        await _extracurricularAPI.findAll(perPage: displayedData);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      extracurriculars.clear();
      for (var item in resultData) {
        extracurriculars.add(Extracurricular.fromJson(item));
      }
    }
    isLoading.value = false;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _extracurricularAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
