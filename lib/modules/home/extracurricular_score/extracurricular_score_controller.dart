import 'package:get/get.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/extracurricular_score_api.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/models/extracurricular_score.dart';
import 'package:saais_client/values/constant_values.dart';

class ExtracurricularScoreController extends GetxController {
  final _extracurricularScoreAPI = Get.find<ExtracurricularScoreAPI>();
  final _extracurricular = Get.find<ExtracurricularAPI>();

  var extracurricular = Extracurricular().obs;

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  List<ExtracurricularScore> scores =
      List<ExtracurricularScore>.empty(growable: true).obs;

  void beforeStart() {
    getExtracurriculars("");
  }

  void getList({required int exculId}) async {
    var findAllResult = await _extracurricularScoreAPI.findAll(
        perPage: displayedData, exculId: exculId);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      scores.clear();
      for (var item in resultData) {
        scores.add(ExtracurricularScore.fromJson(item));
      }
    }
  }

  Future<List<dynamic>> getExtracurriculars(String? filter) async {
    var findAllResult = await _extracurricular.findAll(search: filter ?? "");
    List<Extracurricular> extracurriculars = [];

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        extracurriculars.add(Extracurricular.fromJson(item));
      }

      if (extracurricular.value.id == 0 && extracurriculars.isNotEmpty) {
        extracurricular.value = extracurriculars[0];
      }

      getList(exculId: extracurricular.value.id);
    }

    return extracurriculars;
  }
}
