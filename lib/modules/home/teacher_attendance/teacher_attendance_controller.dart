import 'package:get/get.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class TeacherAttendanceController extends GetxController {
  final _userAPI = Get.find<UserAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<User> users = List<User>.empty(growable: true).obs;

  // var for filter
  var classroom = Class(id: 0, name: "", grade: "", major_id: 0).obs;
  // end var for filter

  void beforeStart() {
    getList();
  }

  void getList() async {
    isLoading.value = true;
    var findAllResult = await _userAPI.findAll(
      perPage: displayedData,
      role: "teacher,finance",
      includeAttendance: true,
    );

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      users.clear();
      for (var item in resultData) {
        users.add(User.fromJson(item));
      }
    }
    isLoading.value = false;
  }
}
