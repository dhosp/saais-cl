import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/journal_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/class_session.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/values/constant_values.dart';

class JournalController extends GetxController {
  final _subjectAPI = Get.find<SubjectAPI>();
  final _classAPI = Get.find<ClassAPI>();
  final _userAPI = Get.find<UserAPI>();
  final _journalAPI = Get.find<JournalAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  var isLoading = false.obs;
  List<ClassSession> journals = List<ClassSession>.empty(growable: true).obs;

  // var for filter
  var classroom = Class(id: 0, name: "", grade: "", major_id: 0).obs;
  // end var for filter

  // var for modal
  Subject? subject;
  User? teacher;

  final startedAtController = TextEditingController(text: "");
  var startedAt = DateTime.tryParse("").obs;

  final finishedAtController = TextEditingController(text: "");
  var finishedAt = DateTime.tryParse("").obs;

  final noteController = TextEditingController(text: "");
  // end var for modal

  void beforeStart() {
    classroom.value.id = 0;
    getClasses("");
  }

  void getList({int classId = 0}) async {
    isLoading.value = true;
    var findAllResult =
        await _journalAPI.findAll(perPage: displayedData, class_id: classId);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      journals.clear();
      for (var item in resultData) {
        journals.add(ClassSession.fromJson(item));
      }
    }
    isLoading.value = false;
  }

  void saveJournalData() async {
    Response result = await _journalAPI.create(
        subject_id: subject!.id,
        teacher_id: teacher!.id,
        lesson_note: noteController.text,
        started_at: startedAt.value!,
        finished_at: finishedAt.value!);

    if (result.body?["status"] ?? false) {
      getList(classId: classroom.value.id);

      subject = null;
      teacher = null;
      startedAt.value = null;
      finishedAt.value = null;
      noteController.text = "";

      Get.close(1);
    }
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    var findAllResult = await _classAPI.findAll(search: filter ?? "");
    List<Class> classrooms = [];

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classrooms.add(Class.fromJson(item));
      }

      if (classroom.value.id == 0 && classrooms.isNotEmpty) {
        classroom.value = classrooms[0];
      }

      getList(classId: classroom.value.id);
    }

    return classrooms;
  }

  Future<List<dynamic>> getSubjects(String? filter) async {
    List<Subject> subjects = [];
    if (classroom.value.id > 0) {
      var findAllResult = await _subjectAPI.findAll(
          search: filter ?? "", class_id: classroom.value.id);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];

        for (var item in resultData) {
          subjects.add(Subject.fromJson(item));
        }
      }
    }

    return subjects;
  }

  Future<List<dynamic>> getTeachers(String? filter) async {
    List<User> teachers = [];
    if (classroom.value.id > 0 && subject?.id != null) {
      var findAllResult = await _userAPI.findAll(
          search: filter ?? "", role: "teacher", subject_id: subject?.id);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];

        for (var item in resultData) {
          teachers.add(User.fromJson(item));
        }
      }
    }

    return teachers;
  }

  startedAtCallback(DateTime date) {
    startedAt.value = date;
  }

  finishedAtCallback(DateTime date) {
    finishedAt.value = date;
  }

  void destroy(int id) async {
    CustomLoading.showLoading();
    var result = await _journalAPI.destroy(id);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      beforeStart();
    }
  }
}
