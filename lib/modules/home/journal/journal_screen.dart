import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_date_picker.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_menu_dialog.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/home/journal/journal_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class JournalScreen extends GetView<JournalController> {
  final String title = "Kelola Jurnal Kelas";

  const JournalScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    final GlobalKey<FormState> modalFormKey = GlobalKey<FormState>();
    return Column(children: [
      Obx(
        () => CustomContentHeader(
          title: title,
          totalData: controller.totalData.value,
          totalDisplayedData: controller.displayedData,
          onRefresh: () {
            controller.getList(classId: controller.classroom.value.id);
          },
          actions: [
            const CustomButton(
              icon: Icons.folder_open,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            const CustomButton(
              icon: Icons.file_download_outlined,
              buttonType: "secondary",
            ),
            const SizedBox(
              width: 16,
            ),
            CustomButton(
              icon: Icons.add,
              title: "Tambah",
              buttonType: "secondary",
              action: () => {
                CustomModal.showModal(
                    mainContent: _addJournalModalContent(modalFormKey),
                    // onInit: controller.getAllStudents,
                    confirmAction: () {
                      if (modalFormKey.currentState!.validate()) {
                        controller.saveJournalData();
                      }
                    },
                    dismissAction: () {
                      controller.subject = null;
                      controller.teacher = null;
                      controller.startedAt.value = null;
                      controller.finishedAt.value = null;
                      controller.noteController.text = "";
                    })
              },
            )
          ],
          filters: [
            Expanded(
                child: CustomDropdown(
              asyncItems: (String filter) => controller.getClasses(filter),
              isFilterOnline: true,
              showSearchBox: true,
              selectedItem: controller.classroom.value,
              itemToString: (item) => Class.getDropdownLabel(item),
              onChanged: (value) {
                controller.classroom.value = value;
                controller.getList(classId: value.id);
              },
            )),
            const SizedBox(
              width: 16,
            ),
            const Expanded(
              child: CustomTextInput(
                label: "Cari Mata Pelajaran",
                suffixIcon: Icon(Icons.search),
              ),
            ),
          ],
        ),
      ),
      Expanded(
          child: Obx(() => controller.totalData > 0
              ? Column(children: [
                  Container(
                    padding: const EdgeInsets.all(16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "Mata Pelajaran",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Guru",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Mulai Pada",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Selesai Pada",
                            textAlign: TextAlign.center,
                            style: TextStyleValues.textMedium
                                .copyWith(color: ColorValues.grey),
                          ),
                        ),
                        const Icon(Icons.more_vert, color: Colors.transparent)
                      ],
                    ),
                  ),
                  Expanded(
                    child: Obx(
                      () => ListView.builder(
                        itemCount: controller.journals.length,
                        itemBuilder: (context, i) {
                          return InkWell(
                            onTap: () => {
                              CustomMenuDialog.showDialog(menus: [
                                CustomButton(
                                  title: "Lihat Detail",
                                  buttonType: "secondary",
                                  action: () {
                                    Get.back();
                                    Get.toNamed(Routes.journal,
                                        parameters: {
                                          "journalId": controller.journals[i].id
                                              .toString(),
                                        },
                                        arguments: controller.journals[i]);
                                  },
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                CustomButton(
                                  title: "Hapus",
                                  buttonType: "danger",
                                  action: () {
                                    Get.back();
                                    CustomDialog.showDialog(
                                        title: "Konfirmasi Hapus Jurnal",
                                        message: "Apakah kamu yakin?",
                                        confirmText: "Ya, yakin",
                                        confirmAction: () {
                                          controller.destroy(
                                              controller.journals[i].id);
                                        },
                                        dismissAction: () {
                                          Get.back();
                                        });
                                  },
                                ),
                              ])
                            },
                            child: Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: ColorValues.white,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                            controller
                                                .journals[i].subject!.name,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.journals[i].user!.name,
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.journals[i].started_at !=
                                                    null
                                                ? Formatter.dateFormatter(
                                                    date: controller.journals[i]
                                                        .started_at!,
                                                    type: "displayDateTime")
                                                : "-",
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.journals[i].started_at !=
                                                    null
                                                ? Formatter.dateFormatter(
                                                    date: controller.journals[i]
                                                        .finished_at!,
                                                    type: "displayDateTime")
                                                : "-",
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      const Icon(Icons.more_vert,
                                          color: ColorValues.grey)
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ])
              : CustomEmptyState(
                  action: () => {
                    CustomModal.showModal(
                        mainContent: _addJournalModalContent(modalFormKey),
                        // onInit: controller.getAllStudents,
                        confirmAction: () {
                          if (modalFormKey.currentState!.validate()) {
                            controller.saveJournalData();
                          }
                        },
                        dismissAction: () {
                          controller.subject = null;
                          controller.teacher = null;
                          controller.startedAt.value = null;
                          controller.finishedAt.value = null;
                          controller.noteController.text = "";
                        })
                  },
                )))
    ]);
  }

  Widget _addJournalModalContent(modalFormKey) {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Jurnal Kelas",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Form(
          key: modalFormKey,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  children: [
                    CustomDropdown(
                      asyncItems: (String filter) =>
                          controller.getClasses(filter),
                      isFilterOnline: true,
                      showSearchBox: true,
                      selectedItem: controller.classroom.value,
                      itemToString: (item) => Class.getDropdownLabel(item),
                      onChanged: (value) {
                        controller.classroom.value = value;
                        controller.getList(classId: value.id);
                      },
                      validator: (value) {
                        if (value == null) {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomDropdown(
                      hint: "Mata Pelajaran",
                      asyncItems: (String filter) =>
                          controller.getSubjects(filter),
                      isFilterOnline: true,
                      showSearchBox: true,
                      selectedItem: controller.subject,
                      itemToString: (item) => Subject.getDropdownLabel(item),
                      onChanged: (value) {
                        controller.subject = value;
                      },
                      validator: (value) {
                        if (value == null) {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomDropdown(
                      hint: "Guru",
                      asyncItems: (String filter) =>
                          controller.getTeachers(filter),
                      isFilterOnline: true,
                      showSearchBox: true,
                      selectedItem: controller.teacher,
                      itemToString: (item) => User.getDropdownLabel(item),
                      onChanged: (value) {
                        controller.teacher = value;
                      },
                      validator: (value) {
                        if (value == null) {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                  child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: CustomDatePicker(
                            label: "Mulai Pada",
                            controller: controller.startedAtController,
                            dateValueCallback: controller.startedAtCallback,
                            date: controller.startedAt.value,
                            validator: (value) {
                              if (value == "") {
                                return "Wajib diisi";
                              } else {
                                return null;
                              }
                            }),
                      ),
                      const SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: CustomDatePicker(
                            label: "Selesai Pada",
                            controller: controller.finishedAtController,
                            dateValueCallback: controller.finishedAtCallback,
                            date: controller.finishedAt.value,
                            validator: (value) {
                              if (value == "") {
                                return "Wajib diisi";
                              } else {
                                return null;
                              }
                            }),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  CustomTextInput(
                      controller: controller.noteController,
                      label: "Catatan Materi",
                      maxLines: 5,
                      validator: (value) {
                        if ((value ??= "") == "") {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      }),
                ],
              ))
            ],
          ),
        ),
      ],
    );
  }
}
