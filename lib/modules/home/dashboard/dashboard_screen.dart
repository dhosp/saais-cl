import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/modules/home/student/student_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/text_style_values.dart';

class DashboardScreen extends GetView<StudentController> {
  const DashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
        potraitLayout: _potraitLayout(), landscapeLayout: _landscapeLayout());
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          "Selamat Datang di",
          style: TextStyleValues.textBold,
        ),
        SizedBox(height: 32),
        Text(
          "Saais - Smaisga Academic Information System",
          style: TextStyleValues.textUltraBold,
        ),
      ],
    );
  }
}
