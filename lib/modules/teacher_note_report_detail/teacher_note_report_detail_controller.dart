import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/teacher_note_api.dart';
import 'package:saais_client/apis/teacher_note_report_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/teacher_note_report.dart';
import 'package:saais_client/models/user.dart';

class TeacherNoteReportDetailController extends GetxController {
  // final _classAPI = Get.find<ClassAPI>();
  final _userAPI = Get.find<UserAPI>();
  final _teacherNoteAPI = Get.find<TeacherNoteAPI>();
  final _teacherNoteReportAPI = Get.find<TeacherNoteReportAPI>();

  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var isAddNew = false;

  var student = User(id: 0, name: "", username: "", user_roles: []).obs;

  // var for modal
  final searchDialogController = TextEditingController(text: "");
  List<TeacherNoteReport> allNotes =
      List<TeacherNoteReport>.empty(growable: true).obs;
  List<TeacherNoteReport> currentNotes =
      List<TeacherNoteReport>.empty(growable: true).obs;

  List<int> selectedNoteIds = List<int>.empty(growable: true).obs;
  List<int> tempSelectedNoteIds = List<int>.empty(growable: true).obs;
  // end modal var

  void beforeStart() {
    if (_userId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _userAPI.findOne(_userId);
      if (result.body?["status"] ?? false) {
        student.value = User.fromJson(result.body["data"]);
      }
    } else {
      student.value = Get.arguments;
    }

    getNotes(student.value.id);
  }

  void getNotes(int userId) async {
    var findAllResult =
        await _teacherNoteReportAPI.findAll(perPage: -1, student_id: userId);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      currentNotes.clear();
      selectedNoteIds.clear();
      for (var item in resultData) {
        var noteReport = TeacherNoteReport.fromJson(item);
        currentNotes.add(noteReport);
        selectedNoteIds.add(noteReport.homeroom_teacher_note!.id);
      }
      tempSelectedNoteIds.assignAll(selectedNoteIds);
    }
  }

  // func for modal
  void getAllNotes({String search = ""}) async {
    var findAllResult = await _teacherNoteAPI.findAll(search: search);

    List<dynamic> resultData = findAllResult.body["data"];

    allNotes.clear();
    for (var item in resultData) {
      if (!selectedNoteIds.contains(item["id"])) {
        item["homeroom_teacher_note"] = item;
        allNotes.add(TeacherNoteReport.fromJson(item));
      }
    }
  }

  void selectNoteIds(int id) {
    int index = tempSelectedNoteIds.indexOf(id);
    if (index >= 0) {
      tempSelectedNoteIds.removeAt(index);
    } else {
      tempSelectedNoteIds.add(id);
    }
  }

  void updateStudentNotes() async {
    CustomLoading.showLoading();
    Response result = await _teacherNoteReportAPI.create(
        studentId: student.value.id, noteIds: tempSelectedNoteIds);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      Get.close(1);
      getNotes(student.value.id);
    }
  }

  void clearTempSelectedNote() {
    tempSelectedNoteIds.assignAll(selectedNoteIds);
  }

  void removeStudentNote(int id) async {
    CustomLoading.showLoading();
    selectedNoteIds.remove(id);

    Response result = await _teacherNoteReportAPI.create(
        studentId: student.value.id, noteIds: selectedNoteIds);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      Get.close(1);
      getNotes(student.value.id);
    }
    CustomLoading.hideLoading();
  }
  // end func modal
}
