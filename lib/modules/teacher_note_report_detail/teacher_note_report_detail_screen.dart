import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/major_enum.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/class_detail/class_detail_controller.dart';
import 'package:saais_client/modules/teacher_note_report_detail/teacher_note_report_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class TeacherNoteReportDetailScreen
    extends GetView<TeacherNoteReportDetailController> {
  const TeacherNoteReportDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Catatan Wali Kelas",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return const Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // children: [
        //   _formGroup1(),
        //   const SizedBox(
        //     height: 16,
        //   ),
        //   _formGroup2()
        // ],
        );
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Column(
          children: [
            const SizedBox(height: 16),
            Container(
              padding: const EdgeInsets.all(32),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: ColorValues.white,
              ),
              constraints: const BoxConstraints(maxWidth: 1000),
              child: _listNotes(),
            )
          ],
        ),
      ],
    );
  }

  Widget _listNotes() {
    return Obx(
      () => Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Catatan Untuk ${User.getDropdownLabel(controller.student.value)}",
              style: TextStyleValues.textBold,
            ),
            CustomButton(
              title: "Tambah Catatan",
              icon: Icons.add,
              action: () {
                CustomModal.showModal(
                    mainContent: _addNoteModalContent(),
                    onInit: controller.getAllNotes,
                    confirmAction: () {
                      if (!listEquals(controller.tempSelectedNoteIds,
                          controller.selectedNoteIds)) {
                        controller.updateStudentNotes();
                        controller.searchDialogController.clear();
                      }
                    },
                    dismissAction: () {
                      controller.clearTempSelectedNote();
                      controller.searchDialogController.clear();
                    });
              },
            )
          ],
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: CustomTextInput(
                label: "Cari NIPD/NISN/Nama Siswa",
                suffixIcon: const Icon(Icons.search),
                onTapSuffixIcon: () {},
                onFieldSubmitted: (value) {},
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Column(
          children: [
            controller.currentNotes.isNotEmpty
                ? ListView.builder(
                    itemCount: controller.currentNotes.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      var currentNote = controller.currentNotes[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                      "${currentNote.homeroom_teacher_note!.description} ",
                                      style: TextStyleValues.textRegular),
                                ),
                                InkWell(
                                  onTap: () {
                                    CustomDialog.showDialog(
                                        title: "Konfirmasi Hapus Catatan Wali",
                                        message:
                                            "Kamu akan menghapus catatan wali kelas. Apakah kamu yakin?",
                                        confirmText: "Ya, yakin",
                                        confirmAction: () {
                                          controller.removeStudentNote(
                                              currentNote
                                                  .homeroom_teacher_note!.id);
                                        });
                                  },
                                  child: const Icon(
                                    Icons.delete_forever_rounded,
                                    color: ColorValues.red,
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    })
                : const CustomEmptyState(),
          ],
        )
      ]),
    );
  }

  Widget _addNoteModalContent() {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Siswa",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              flex: 3,
              child: CustomTextInput(
                  controller: controller.searchDialogController,
                  label: "Cari NIPD/Nama Siswa",
                  suffixIcon: const Icon(Icons.search),
                  onTapSuffixIcon: () {
                    controller.getAllNotes(
                        search: controller.searchDialogController.text);
                  },
                  onFieldSubmitted: (value) {
                    controller.getAllNotes(search: value);
                  }),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Obx(() => controller.allNotes.isNotEmpty
            ? ListView.builder(
                itemCount: controller.allNotes.length,
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  var currentNote = controller.allNotes[i];
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          controller.selectNoteIds(
                              currentNote.homeroom_teacher_note!.id);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                    "${currentNote.homeroom_teacher_note!.description} ",
                                    style: TextStyleValues.textRegular),
                              ),
                              Obx(() => controller.tempSelectedNoteIds.contains(
                                      currentNote.homeroom_teacher_note!.id)
                                  ? const Icon(
                                      Icons.check_box_rounded,
                                      color: ColorValues.bluePrimary,
                                    )
                                  : const Icon(
                                      Icons.check_box_outline_blank_rounded,
                                      color: ColorValues.grey,
                                    ))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      )
                    ],
                  );
                })
            : const CustomEmptyState()),
      ],
    );
  }
}
