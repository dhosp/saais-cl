import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/teacher_note_api.dart';
import 'package:saais_client/apis/teacher_note_report_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/class_detail/class_detail_controller.dart';
import 'package:saais_client/modules/teacher_note_report_detail/teacher_note_report_detail_controller.dart';

class TeacherNoteReportDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => TeacherNoteReportAPI());
    Get.lazyPut(() => TeacherNoteAPI());
    Get.lazyPut(() => TeacherNoteReportDetailController());
  }
}
