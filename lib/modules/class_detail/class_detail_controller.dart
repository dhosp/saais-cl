import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/student_class.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class ClassDetailController extends GetxController {
  final _classAPI = Get.find<ClassAPI>();
  final _userAPI = Get.find<UserAPI>();

  final classCountController = TextEditingController(text: "");
  final nameController = TextEditingController(text: "");
  var grade = "".obs;
  var major = "".obs;
  var regenerate = false.obs;
  var homeroomTeacher = User(id: 0, name: "", username: "", user_roles: []).obs;

  final _classId = int.tryParse(Get.parameters["classId"].toString()) ?? 0;
  var isAddNew = false;

  var classroom = Class().obs;

  // var for modal
  final searchDialogController = TextEditingController(text: "");
  List<StudentClass> allStudents = List<StudentClass>.empty(growable: true).obs;
  List<StudentClass> currentStudents =
      List<StudentClass>.empty(growable: true).obs;

  List<int> selectedStudentIds = List<int>.empty(growable: true).obs;
  List<int> tempSelectedStudentIds = List<int>.empty(growable: true).obs;

  // end modal var

  void beforeStart() {
    if (_classId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    selectedStudentIds.clear();
    tempSelectedStudentIds.clear();
    allStudents.clear();
    currentStudents.clear();

    if (Get.arguments == null) {
      var result = await _classAPI.findOne(_classId);
      if (result.body?["status"] ?? false) {
        classroom.value = Class.fromJson(result.body["data"]);
      }
    } else {
      classroom.value = Get.arguments;
    }

    nameController.text = classroom.value.name;
    grade.value = classroom.value.grade;
    major.value = classroom.value.major_id.toString();

    if (classroom.value.teacher_id != null) {
      homeroomTeacher.value = User(
          id: classroom.value.teacher_id!,
          name: classroom.value.teacher_name ?? "",
          username: "",
          user_roles: []);
    }

    currentStudents.assignAll(classroom.value.student_classes ?? []);
    for (var item in currentStudents) {
      selectedStudentIds.add(item.student_id);
    }

    tempSelectedStudentIds.assignAll(selectedStudentIds);
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result;
    if (isAddNew) {
      result = await _classAPI.create(
          class_count: int.parse(classCountController.text),
          major_id: int.parse(major.value),
          grade: grade.value,
          regenerate: regenerate.value);
    } else {
      result = await _classAPI.update(
          id: _classId, teacher_id: homeroomTeacher.value.id);
    }
    CustomLoading.hideLoading();
    if ((result.body?["status"] ?? false) && isAddNew) {
      isAddNew = false;
      grade.value = "";
      major.value = "";
      regenerate.value = false;
      classCountController.text = "";
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "2", "tab": "0"});
    }
  }

  Future<List<dynamic>> getTeachers(String? filter) async {
    List<User> teachers = [];
    var findAllResult =
        await _userAPI.findAll(role: "homeroom_teacher", search: filter ?? "");
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        teachers.add(User.fromJson(item));
      }
    }

    return teachers;
  }

  // func for modal
  void getAllStudents({String search = ""}) async {
    var allStudentFindAllResult =
        await _userAPI.findAll(role: "student", search: search);

    List<dynamic> resultData = allStudentFindAllResult.body["data"];

    allStudents.clear();
    for (var item in resultData) {
      if (!selectedStudentIds.contains(item["id"])) {
        item["student_id"] = item["id"];
        allStudents.add(StudentClass.fromJson(item));
      }
    }
  }

  void selectStudentIds(int id) {
    int index = tempSelectedStudentIds.indexOf(id);
    if (index >= 0) {
      tempSelectedStudentIds.removeAt(index);
    } else {
      tempSelectedStudentIds.add(id);
    }
  }

  void updateStudentClass() async {
    CustomLoading.showLoading();
    Response result = await _classAPI.updateStudents(
        id: _classId, student_ids: tempSelectedStudentIds);

    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      getData();
      Get.close(1);
    }
  }

  void clearTempSelectedStudent() {
    tempSelectedStudentIds.assignAll(selectedStudentIds);
  }

  void removeStudentClass(int id) async {
    CustomLoading.showLoading();
    selectedStudentIds.remove(id);

    Response result = await _classAPI.updateStudents(
        id: _classId, student_ids: selectedStudentIds);

    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      getData();
      Get.close(1);
    }
  }
  // end func modal
}
