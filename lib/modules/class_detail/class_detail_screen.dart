import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_checkbox.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/major_enum.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/class_detail/class_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class ClassDetailScreen extends GetView<ClassDetailController> {
  const ClassDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Kelas",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return const Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // children: [
        //   _formGroup1(),
        //   const SizedBox(
        //     height: 16,
        //   ),
        //   _formGroup2()
        // ],
        );
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Obx(
            () => Column(
              children: [
                Form(
                    key: formKey,
                    child: controller.isAddNew
                        ? _createNewFormGroup()
                        : Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(child: _formGroup1()),
                              const SizedBox(width: 16),
                              Expanded(child: _formGroup2()),
                            ],
                          )),
                const SizedBox(height: 32),
                _actionButton(formKey),
              ],
            ),
          ),
        ),
        controller.isAddNew
            ? Container()
            : Column(
                children: [
                  const SizedBox(height: 16),
                  Container(
                    padding: const EdgeInsets.all(32),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: ColorValues.white,
                    ),
                    constraints: const BoxConstraints(maxWidth: 1000),
                    child: _listStudent(),
                  )
                ],
              ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomTextInput(
          label: "Nama",
          readOnly: true,
          controller: controller.nameController,
        ),
        const SizedBox(height: 16),
        CustomDropdown(
          hint: "Tingkat",
          items: GradeEnum.getDropdownItems(),
          itemToString: (item) => GradeEnum.getDropdownLabel(grade: item),
          selectedItem:
              controller.grade.value == "" ? null : controller.grade.value,
          enabled: false,
        ),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        CustomDropdown(
          hint: "Wali Kelas",
          showSearchBox: true,
          asyncItems: (String filter) => controller.getTeachers(filter),
          selectedItem: controller.homeroomTeacher.value.id == 0
              ? null
              : controller.homeroomTeacher.value,
          itemToString: (item) => User.getDropdownLabel(item),
          onChanged: (value) => {controller.homeroomTeacher.value = value},
          isFilterOnline: true,
          validator: (value) {
            if (value == null) {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomDropdown(
          hint: "Jurusan",
          items: MajorEnum.getDropdownItems(),
          itemToString: (item) => MajorEnum.getDropdownLabel(major: item),
          selectedItem:
              controller.major.value == "" ? null : controller.major.value,
          enabled: false,
        )
      ],
    );
  }

  Widget _createNewFormGroup() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomTextInput(
          label: "Jumlah Kelas",
          controller: controller.classCountController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            if ((int.tryParse(value ?? "") ?? 0) <= 0) {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomDropdown(
            hint: "Jurusan",
            items: MajorEnum.getDropdownItems(),
            itemToString: (item) => MajorEnum.getDropdownLabel(major: item),
            selectedItem:
                controller.major.value == "" ? null : controller.major.value,
            onChanged: (value) {
              controller.major.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(height: 16),
        CustomDropdown(
            hint: "Tingkat",
            items: GradeEnum.getDropdownItems(),
            itemToString: (item) => GradeEnum.getDropdownLabel(grade: item),
            selectedItem:
                controller.grade.value == "" ? null : controller.grade.value,
            onChanged: (value) {
              controller.grade.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(height: 16),
        CustomCheckbox(
            label: "Hapus kelas yang sudah ada di kategori yang sama",
            value: controller.regenerate.value,
            onChanged: (value) {
              controller.regenerate.value = value ?? false;
            })
      ],
    );
  }

  Widget _listStudent() {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            "Daftar Siswa",
            style: TextStyleValues.textBold,
          ),
          CustomButton(
            title: "Tambah Siswa",
            icon: Icons.add,
            action: () {
              CustomModal.showModal(
                  mainContent: _addStudentModalContent(),
                  onInit: controller.getAllStudents,
                  confirmAction: () {
                    if (!listEquals(controller.tempSelectedStudentIds,
                        controller.selectedStudentIds)) {
                      controller.updateStudentClass();
                      controller.searchDialogController.clear();
                    }
                  },
                  dismissAction: () {
                    controller.clearTempSelectedStudent();
                    controller.searchDialogController.clear();
                  });
            },
          )
        ],
      ),
      const SizedBox(height: 16),
      Row(
        children: [
          Expanded(
            child: CustomTextInput(
              label: "Cari NIPD/NISN/Nama Siswa",
              suffixIcon: const Icon(Icons.search),
              onTapSuffixIcon: () {},
              onFieldSubmitted: (value) {},
            ),
          ),
        ],
      ),
      const SizedBox(
        height: 16,
      ),
      Column(
        children: [
          Obx(() => controller.currentStudents.isNotEmpty
              ? ListView.builder(
                  itemCount: controller.currentStudents.length,
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                    "${controller.currentStudents[i].username} - ${controller.currentStudents[i].name} ",
                                    style: TextStyleValues.textRegular),
                              ),
                              InkWell(
                                onTap: () {
                                  CustomDialog.showDialog(
                                      title: "Konfirmasi Hapus Siswa",
                                      message:
                                          "Menghapus siswa berarti menghapus semua riwayat pembelajaran. Apakah kamu yakin?",
                                      confirmText: "Ya, yakin",
                                      confirmAction: () {
                                        controller.removeStudentClass(controller
                                            .currentStudents[i].student_id);
                                      });
                                },
                                child: const Icon(
                                  Icons.delete_forever_rounded,
                                  color: ColorValues.red,
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        )
                      ],
                    );
                  })
              : const CustomEmptyState()),
        ],
      )
    ]);
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () => {
              if (formKey.currentState!.validate()) {controller.saveData()}
            },
          ),
        ),
      ],
    );
  }

  Widget _addStudentModalContent() {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Siswa",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              flex: 3,
              child: CustomTextInput(
                  controller: controller.searchDialogController,
                  label: "Cari NIPD/Nama Siswa",
                  suffixIcon: const Icon(Icons.search),
                  onTapSuffixIcon: () {
                    controller.getAllStudents(
                        search: controller.searchDialogController.text);
                  },
                  onFieldSubmitted: (value) {
                    controller.getAllStudents(search: value);
                  }),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Obx(() => controller.allStudents.isNotEmpty
            ? ListView.builder(
                itemCount: controller.allStudents.length,
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          controller.selectStudentIds(
                              controller.allStudents[i].student_id);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(
                                    "${controller.allStudents[i].username} - ${controller.allStudents[i].name} ",
                                    style: TextStyleValues.textRegular),
                              ),
                              Obx(() => controller.tempSelectedStudentIds
                                      .contains(
                                          controller.allStudents[i].student_id)
                                  ? const Icon(
                                      Icons.check_box_rounded,
                                      color: ColorValues.bluePrimary,
                                    )
                                  : const Icon(
                                      Icons.check_box_outline_blank_rounded,
                                      color: ColorValues.grey,
                                    ))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      )
                    ],
                  );
                })
            : const CustomEmptyState()),
      ],
    );
  }
}
