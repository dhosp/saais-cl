import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/models/month.dart';
import 'package:saais_client/modules/school_bill_individual/school_bill_individual_controller.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';
import 'package:intl/intl.dart';

class SchoolBillIndividualScreen
    extends GetView<SchoolBillIndividualController> {
  const SchoolBillIndividualScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(),
      landscapeLayout: _landscapeLayout(),
      includeAppBar: true,
      greySpace: false,
      includeScroll: false,
    );
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    return Container(
        constraints: const BoxConstraints(maxWidth: 1000),
        child: Column(children: [
          Obx(
            () => CustomContentHeader(
              title: "Daftar Tagihan ${controller.userName.value}",
              totalData: controller.totalData.value,
              totalDisplayedData: controller.displayedData,
              onRefresh: () => controller.getList(),
              actions: const [
                CustomButton(
                  icon: Icons.folder_open,
                  buttonType: "secondary",
                ),
                SizedBox(
                  width: 16,
                ),
                CustomButton(
                  icon: Icons.file_download_outlined,
                  buttonType: "secondary",
                ),
                // const SizedBox(
                //   width: 16,
                // ),
                // CustomButton(
                //   icon: Icons.add,
                //   title: "Generate",
                //   buttonType: "secondary",
                //   action: () => {
                //     Get.toNamed(
                //       Routes.billDetail,
                //       parameters: {
                //         "billId": "0",
                //         "addNew": "true",
                //       },
                //     )
                //   },
                // )
              ],
              filters: [
                Expanded(
                  child: CustomDropdown(
                    items: const ["1", "2"],
                    hint: "Semua Tahun Ajaran",
                    onChanged: (value) => {},
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: CustomDropdown(
                    items: const ["due", "partial", "paid"],
                    itemToString: (value) {
                      switch (value) {
                        case "due":
                          return "Belum Dibayar";
                        case "partial":
                          return "Dibayar Sebagian";
                        default:
                          return "Lunas";
                      }
                    },
                    hint: "Semua Status Pembayaran",
                    onChanged: (value) => {},
                  ),
                ),
              ],
            ),
          ),
          Obx(
            () => Expanded(
              child: controller.totalData > 0
                  ? Column(children: [
                      Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Bulan",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Status",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Nominal",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Catatan",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            const Icon(Icons.more_vert,
                                color: Colors.transparent)
                          ],
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: controller.bills.length,
                          itemBuilder: (context, i) {
                            return InkWell(
                              onTap: () => {
                                Get.toNamed(Routes.billDetail,
                                    parameters: {
                                      "billId":
                                          controller.bills[i].id.toString(),
                                    },
                                    arguments: controller.bills[i])
                              },
                              child: Column(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(16),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(4),
                                      color: ColorValues.white,
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                              "${Month.getDropdownLabel(controller.bills[i].month)} (Sem. ${controller.bills[i].school_period!.semester} ${controller.bills[i].school_period!.start_year} - ${controller.bills[i].school_period!.end_year})",
                                              textAlign: TextAlign.center,
                                              style:
                                                  TextStyleValues.textMedium),
                                        ),
                                        Expanded(
                                          child: Text(
                                              controller.bills[i].status ==
                                                      "due"
                                                  ? "Belum Dibayar"
                                                  : controller.bills[i]
                                                              .status ==
                                                          "partial"
                                                      ? "Dibayar Sebagian"
                                                      : "Lunas",
                                              textAlign: TextAlign.center,
                                              style:
                                                  TextStyleValues.textMedium),
                                        ),
                                        Expanded(
                                          child: Text(
                                              NumberFormat.currency(
                                                      symbol: "Rp ")
                                                  .format(controller
                                                      .bills[i].amount),
                                              textAlign: TextAlign.center,
                                              style:
                                                  TextStyleValues.textMedium),
                                        ),
                                        Expanded(
                                          child: Text(
                                            "${(controller.bills[i].note ?? "").length > 20 ? ("${controller.bills[i].note!.substring(0, 20)}...") : controller.bills[i].note}",
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium,
                                          ),
                                        ),
                                        const Icon(Icons.more_vert,
                                            color: ColorValues.grey)
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ])
                  : const CustomEmptyState(),
            ),
          ),
        ]));
  }
}
