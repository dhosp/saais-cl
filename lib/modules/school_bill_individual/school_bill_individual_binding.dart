import 'package:get/get.dart';
import 'package:saais_client/apis/school_bill_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/school_bill_individual/school_bill_individual_controller.dart';

class SchoolBillIndividualBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SchoolBillAPI());
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => SchoolBillIndividualController());
  }
}
