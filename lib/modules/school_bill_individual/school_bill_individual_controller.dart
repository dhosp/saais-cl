import 'package:get/get.dart';
import 'package:saais_client/apis/school_bill_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/school_bill.dart';
import 'package:saais_client/values/constant_values.dart';

class SchoolBillIndividualController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final _billAPI = Get.find<SchoolBillAPI>();

  final displayedData = ConstantValues.maxDisplayedData;
  var totalData = 0.obs;
  final _studentId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;
  var userName = "".obs;
  List<SchoolBill> bills = List<SchoolBill>.empty(growable: true).obs;

  void beforeStart() {
    if (_studentId > 0) {
      getUser();
      getList();
    }
  }

  void getUser() async {
    var result = await _userAPI.findOne(_studentId);
    if (result.body?["status"] ?? false) {
      userName.value = result.body["data"]["name"];
    }
  }

  void getList() async {
    var findAllResult =
        await _billAPI.findAll(perPage: displayedData, student_id: _studentId);

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      totalData(findAllResult.body["total"]);
      bills.clear();
      for (var item in resultData) {
        bills.add(SchoolBill.fromJson(item));
      }
    }
  }
}
