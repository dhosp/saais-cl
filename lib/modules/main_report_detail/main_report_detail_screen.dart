import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/modules/main_report_detail/main_report_detail_controller.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/responsive_layout.dart';

class MainReportDetailScreen extends GetView<MainReportDetailController> {
  const MainReportDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();

    return ResponsiveLayout(
      potraitLayout: _potraitLayout(context),
      landscapeLayout: _landscapeLayout(context),
      includeAppBar: true,
      appBarTitle: "Detail Rapor",
      greySpace: false,
      addPadding: false,
      // includeScroll: false,
    );
  }

  Widget _potraitLayout(context) {
    return Obx(
      () => SizedBox(
        height: MediaQuery.of(context).size.height - kToolbarHeight - 32,
        child: controller.isLoading.value
            ? const Center(child: CircularProgressIndicator())
            : PdfPreview(
                maxPageWidth: 1000,
                canDebug: false,
                canChangeOrientation: false,
                canChangePageFormat: false,
                build: (format) => buildPdf(format),
                actions: <PdfPreviewAction>[
                  if (!kIsWeb)
                    PdfPreviewAction(
                      icon: const Icon(Icons.save),
                      onPressed: controller.saveAsFile,
                    )
                ],
              ),
      ),
    );
  }

  Widget _landscapeLayout(context) {
    return Obx(
      () => SizedBox(
        height: MediaQuery.of(context).size.height - kToolbarHeight,
        child: controller.isLoading.value
            ? const Center(child: CircularProgressIndicator())
            : PdfPreview(
                maxPageWidth: 1000,
                canDebug: false,
                canChangeOrientation: false,
                canChangePageFormat: false,
                build: (format) => buildPdf(format),
                actions: <PdfPreviewAction>[
                  if (!kIsWeb)
                    PdfPreviewAction(
                      icon: const Icon(Icons.save),
                      onPressed: controller.saveAsFile,
                    )
                ],
              ),
      ),
    );
  }

  Future<Uint8List> buildPdf(PdfPageFormat format) async {
    var report = controller.mainReport;

    final pw.Document doc = pw.Document();

    final regularFont = await PdfGoogleFonts.openSansRegular();
    final boldFont = await PdfGoogleFonts.openSansBold();

    final textSmall = pw.TextStyle(
      fontSize: 9,
      fontWeight: pw.FontWeight.normal,
    );
    final textRegular = pw.TextStyle(
      fontSize: 10,
      fontWeight: pw.FontWeight.normal,
    );
    final textBold = pw.TextStyle(
      fontSize: 10,
      fontWeight: pw.FontWeight.bold,
    );
    final textUltraBold = pw.TextStyle(
      fontSize: 14,
      fontWeight: pw.FontWeight.bold,
    );
    // page score 1
    doc.addPage(
      pw.Page(
        pageFormat: format.copyWith(
          height: 29.7 * PdfPageFormat.cm,
          width: 21 * PdfPageFormat.cm,
          marginTop: 1.5 * PdfPageFormat.cm,
          marginBottom: 1.5 * PdfPageFormat.cm,
          marginLeft: 1.5 * PdfPageFormat.cm,
          marginRight: 1.5 * PdfPageFormat.cm,
        ),
        theme: pw.ThemeData.withFont(
          base: regularFont,
          bold: boldFont,
        ),
        build: (pw.Context context) {
          return pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.Center(
                    child: pw.Text("PENCAPAIAN KOMPETENSI PESERTA DIDIK",
                        style: textUltraBold)),
                pw.SizedBox(height: 32),
                pw.Row(children: [
                  pw.Expanded(
                      child: pw.Text("Nama Sekolah",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      flex: 2,
                      child: pw.Text(
                        ": SMA ISLAM SABILURROSYAD",
                        style: textBold,
                        textAlign: pw.TextAlign.left,
                      )),
                  pw.Expanded(
                      child: pw.Text("Kelas",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      child: pw.Text(
                          ": ${controller.user.value.student_classes?.isNotEmpty ?? false ? Class.getDropdownLabel(Class(id: 1, name: controller.user.value.student_classes![0].class_name!, major_name: controller.user.value.student_classes![0].class_major_name!, grade: controller.user.value.student_classes![0].class_grade!)) : "-"}",
                          style: textBold,
                          textAlign: pw.TextAlign.left)),
                ]),
                pw.SizedBox(height: 4),
                pw.Row(children: [
                  pw.Expanded(
                      child: pw.Text("Alamat",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      flex: 2,
                      child: pw.Text(
                        ": Jl. Candi VI C 303 Gasek",
                        style: textBold,
                        textAlign: pw.TextAlign.left,
                      )),
                  pw.Expanded(
                      child: pw.Text("Semester",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      child: pw.Text(
                          ": ${report["school_period"]?["semester"] ?? ""} (${report["school_period"]?["semester"] == "1" ? "Satu" : report["school_period"]?["semester"] == "2" ? "Dua" : ""})",
                          style: textBold,
                          textAlign: pw.TextAlign.left)),
                ]),
                pw.SizedBox(height: 4),
                pw.Row(children: [
                  pw.Expanded(
                      child: pw.Text("Nama",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      flex: 2,
                      child: pw.Text(
                        ": ${controller.user.value.name}",
                        style: textBold,
                        textAlign: pw.TextAlign.left,
                      )),
                  pw.Expanded(
                      child: pw.Text("Tahun Pelajaran",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      child: pw.Text(
                          ": ${report["school_period"]?["start_year"] ?? ""} - ${report["school_period"]?["end_year"] ?? ""}",
                          style: textBold,
                          textAlign: pw.TextAlign.left)),
                ]),
                pw.SizedBox(height: 4),
                pw.Row(children: [
                  pw.Expanded(
                      child: pw.Text("No. Induk / NISN",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      flex: 2,
                      child: pw.Text(
                        ": ${controller.user.value.username} / ${controller.user.value.user_detail?.nisn ?? "-"}",
                        style: textBold,
                        textAlign: pw.TextAlign.left,
                      )),
                  pw.Expanded(
                      child: pw.Text("",
                          style: textRegular, textAlign: pw.TextAlign.left)),
                  pw.Expanded(
                      child: pw.Text("",
                          style: textBold, textAlign: pw.TextAlign.left)),
                ]),
                pw.SizedBox(height: 32),
                pw.Text("A. SIKAP", style: textBold),
                pw.Text("1. Sikap Spiritual", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                  cellHeight: 100,
                  columnWidths: {1: const pw.FixedColumnWidth(200)},
                  cellAlignments: {0: pw.Alignment.topCenter},
                  context: context,
                  headerHeight: 10,
                  headers: ['Predikat', 'Deskripsi'],
                  data: [
                    [
                      report["attitude_score"]?["spiritual"]?["grade"] ?? "",
                      report["attitude_score"]?["spiritual"]?["description"] ??
                          ""
                    ],
                  ],
                ),
                pw.SizedBox(height: 8),
                pw.Text("2. Sikap Sosial", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                  cellHeight: 100,
                  columnWidths: {1: const pw.FixedColumnWidth(200)},
                  cellAlignments: {0: pw.Alignment.topCenter},
                  context: context,
                  headerHeight: 10,
                  headers: ['Predikat', 'Deskripsi'],
                  data: [
                    [
                      report["attitude_score"]?["sosial"]?["grade"] ?? "",
                      report["attitude_score"]?["sosial"]?["description"] ?? ""
                    ]
                  ],
                ),
              ]);
        },
      ),
    );

    // page score 2
    doc.addPage(
      pw.Page(
        pageFormat: format.copyWith(
          height: 29.7 * PdfPageFormat.cm,
          width: 21 * PdfPageFormat.cm,
          marginTop: 1.5 * PdfPageFormat.cm,
          marginBottom: 1.5 * PdfPageFormat.cm,
          marginLeft: 1.5 * PdfPageFormat.cm,
          marginRight: 1.5 * PdfPageFormat.cm,
        ),
        theme: pw.ThemeData.withFont(
          base: regularFont,
          bold: boldFont,
        ),
        build: (pw.Context context) {
          List<List<dynamic>> subjectScoreData = [];

          var counter = 1;
          if (report["subject_score"]?["knowledge"] != null) {
            for (var item in report["subject_score"]["knowledge"]) {
              subjectScoreData.add([
                counter.toString(),
                item["name"] ?? "",
                item["pass_score"] ?? "",
                item["score"] ?? "",
                (item["grade"] ?? "").toString().toUpperCase(),
                item["description"] ?? "",
              ]);
              counter++;
            }
          }

          return pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.Text("B. PENGETAHUAN", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                  cellAlignments: {
                    0: pw.Alignment.center,
                    1: pw.Alignment.centerLeft,
                    2: pw.Alignment.center,
                    3: pw.Alignment.center,
                    4: pw.Alignment.center,
                    5: pw.Alignment.centerLeft
                  },
                  columnWidths: {5: const pw.FixedColumnWidth(150)},
                  cellStyle: textSmall,
                  context: context,
                  headers: [
                    'No',
                    'Mata Pelajaran',
                    'KKM',
                    'Nilai',
                    'Predikat',
                    'Deskripsi'
                  ],
                  data: subjectScoreData,
                ),
              ]);
        },
      ),
    );

    // page score 3
    doc.addPage(
      pw.Page(
        pageFormat: format.copyWith(
          height: 29.7 * PdfPageFormat.cm,
          width: 21 * PdfPageFormat.cm,
          marginTop: 1.5 * PdfPageFormat.cm,
          marginBottom: 1.5 * PdfPageFormat.cm,
          marginLeft: 1.5 * PdfPageFormat.cm,
          marginRight: 1.5 * PdfPageFormat.cm,
        ),
        theme: pw.ThemeData.withFont(
          base: regularFont,
          bold: boldFont,
        ),
        build: (pw.Context context) {
          List<List<dynamic>> subjectScoreData = [];

          var counter = 1;
          if (report["subject_score"]?["skill"] != null) {
            for (var item in report["subject_score"]["skill"]) {
              subjectScoreData.add([
                counter.toString(),
                item["name"] ?? "",
                item["pass_score"] ?? "",
                item["score"] ?? "",
                (item["grade"] ?? "").toString().toUpperCase(),
                item["description"] ?? "",
              ]);
              counter++;
            }
          }

          List<List<dynamic>> gradeData = [];
          if (report["subject_score"]?["grade"] != null) {
            for (MapEntry item in report["subject_score"]["grade"].entries) {
              gradeData.add([
                item.key,
                '< ${item.value["d"]?["max"] ?? 0}',
                '${item.value["c"]?["min"] ?? 0} - ${item.value["c"]?["max"] ?? 0}',
                '${item.value["b"]?["min"] ?? 0} - ${item.value["b"]?["max"] ?? 0}',
                '${item.value["a"]?["min"] ?? 0} - ${item.value["a"]?["max"] ?? 0}',
              ]);
            }
          }

          return pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.Text("C. KETERAMPILAN", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                    cellAlignments: {
                      0: pw.Alignment.center,
                      1: pw.Alignment.centerLeft,
                      2: pw.Alignment.center,
                      3: pw.Alignment.center,
                      4: pw.Alignment.center,
                      5: pw.Alignment.centerLeft
                    },
                    columnWidths: {5: const pw.FixedColumnWidth(150)},
                    cellStyle: textSmall,
                    context: context,
                    headers: [
                      'No',
                      'Mata Pelajaran',
                      'KKM',
                      'Nilai',
                      'Predikat',
                      'Deskripsi'
                    ],
                    data: subjectScoreData),
                pw.SizedBox(height: 8),
                pw.TableHelper.fromTextArray(
                    cellAlignment: pw.Alignment.topCenter,
                    context: context,
                    headers: [
                      'KKM',
                      'Kurang',
                      'Cukup',
                      'Baik',
                      'Sangat Baik',
                    ],
                    data: gradeData)
              ]);
        },
      ),
    );

    // page score 4
    doc.addPage(
      pw.Page(
        pageFormat: format.copyWith(
          height: 29.7 * PdfPageFormat.cm,
          width: 21 * PdfPageFormat.cm,
          marginTop: 1.5 * PdfPageFormat.cm,
          marginBottom: 1.5 * PdfPageFormat.cm,
          marginLeft: 1.5 * PdfPageFormat.cm,
          marginRight: 1.5 * PdfPageFormat.cm,
        ),
        theme: pw.ThemeData.withFont(
          base: regularFont,
          bold: boldFont,
        ),
        build: (pw.Context context) {
          List<List<dynamic>> extracurricularData = [];
          if (report["extracurricular_score"] != null) {
            var counter = 1;
            for (var item in report["extracurricular_score"]) {
              extracurricularData.add([
                counter.toString(),
                item["name"] ?? "",
                item["grade"] ?? ""
              ]);
              counter++;
            }
          }

          if (extracurricularData.isEmpty) {
            extracurricularData.add([" ", " ", " "]);
          }

          List<List<dynamic>> achievementData = [];
          if (report["achievement"] != null) {
            var counter = 1;
            for (var item in report["achievement"]) {
              achievementData.add([
                counter.toString(),
                item["type"] ?? "",
                item["description"] ?? ""
              ]);
              counter++;
            }
          }

          if (achievementData.isEmpty) {
            achievementData.add([" ", " ", " "]);
          }

          List<List<dynamic>> attendanceData = [];
          if (report["attendance_report"] != null) {
            for (var item in report["attendance_report"]) {
              attendanceData.add([item["type"] ?? "", item["total"] ?? ""]);
            }
          }

          return pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.Text("D. EKSTRAKURIKULER", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                    cellAlignments: {
                      0: pw.Alignment.topCenter,
                      2: pw.Alignment.topCenter,
                    },
                    context: context,
                    headers: [
                      'No',
                      'Kegiatan Ekstrakurikuler',
                      'Predikat',
                    ],
                    data: extracurricularData),
                pw.SizedBox(height: 32),
                pw.Text("E. PRESTASI", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                    cellAlignments: {
                      0: pw.Alignment.topCenter,
                      1: pw.Alignment.topCenter,
                    },
                    context: context,
                    headers: [
                      'No',
                      'Jenis Prestasi',
                      'Keterangan',
                    ],
                    data: achievementData),
                pw.SizedBox(height: 32),
                pw.Text("F. KETIDAKHADIRAN", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                    cellAlignments: {1: pw.Alignment.topCenter},
                    context: context,
                    headerCount: 0,
                    data: attendanceData),
              ]);
        },
      ),
    );

    // page score 5
    doc.addPage(
      pw.Page(
        pageFormat: format.copyWith(
          height: 29.7 * PdfPageFormat.cm,
          width: 21 * PdfPageFormat.cm,
          marginTop: 1.5 * PdfPageFormat.cm,
          marginBottom: 1.5 * PdfPageFormat.cm,
          marginLeft: 1.5 * PdfPageFormat.cm,
          marginRight: 1.5 * PdfPageFormat.cm,
        ),
        theme: pw.ThemeData.withFont(
          base: regularFont,
          bold: boldFont,
        ),
        build: (pw.Context context) {
          return pw.Column(
              crossAxisAlignment: pw.CrossAxisAlignment.start,
              children: [
                pw.Text("G. CATATAN WALI KELAS", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                  context: context,
                  cellHeight: 100,
                  headerCount: 0,
                  data: [
                    [
                      report["teacher_note"] ?? "",
                    ],
                  ],
                ),
                pw.SizedBox(height: 32),
                pw.Text("H. TANGGAPAN ORANG TUA", style: textBold),
                pw.SizedBox(height: 4),
                pw.TableHelper.fromTextArray(
                  context: context,
                  cellHeight: 100,
                  data: [
                    [
                      ' ',
                    ],
                  ],
                ),
                pw.Expanded(child: pw.Container()),
                pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                    children: [
                      pw.Column(
                          mainAxisAlignment: pw.MainAxisAlignment.start,
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Text("Mengetahui,",
                                textAlign: pw.TextAlign.left,
                                style: textRegular),
                            pw.Text("Orang Tua/Wali,",
                                textAlign: pw.TextAlign.left,
                                style: textRegular),
                            pw.SizedBox(height: 100),
                            pw.Text("......................................",
                                textAlign: pw.TextAlign.left,
                                style: textRegular),
                            pw.Text("..",
                                textAlign: pw.TextAlign.left,
                                style: textRegular.copyWith(
                                    color: PdfColor.fromHex("fff"))),
                          ]),
                      pw.Column(
                          mainAxisAlignment: pw.MainAxisAlignment.start,
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Text("..",
                                textAlign: pw.TextAlign.left,
                                style: textRegular.copyWith(
                                    color: PdfColor.fromHex("fff"))),
                            pw.Text("Wali Kelas,",
                                textAlign: pw.TextAlign.left,
                                style: textRegular),
                            pw.SizedBox(height: 100),
                            pw.Text(report["homeroom_teacher"]?["name"] ?? "",
                                textAlign: pw.TextAlign.left, style: textBold),
                            pw.Text(
                                "NIP. ${report["homeroom_teacher"]?["username"] ?? ""}",
                                textAlign: pw.TextAlign.left,
                                style: textBold)
                          ]),
                      pw.Column(
                          mainAxisAlignment: pw.MainAxisAlignment.start,
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          children: [
                            pw.Text(
                                "Kota Malang, ${Formatter.dateFormatter(date: DateTime.now(), type: "display")}",
                                textAlign: pw.TextAlign.left,
                                style: textRegular),
                            pw.Text("Kepala Sekolah,",
                                textAlign: pw.TextAlign.left,
                                style: textRegular),
                            pw.SizedBox(height: 100),
                            pw.Text(report["principal"]?["name"] ?? "",
                                textAlign: pw.TextAlign.left, style: textBold),
                            pw.Text(
                                "NIP. ${report["principal"]?["username"] ?? ""}",
                                textAlign: pw.TextAlign.left,
                                style: textBold)
                          ]),
                    ]),
              ]);
        },
      ),
    );

    return await doc.save();
  }
}
