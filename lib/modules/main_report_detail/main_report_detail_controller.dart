import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:saais_client/apis/report_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/models/user.dart';

class MainReportDetailController extends GetxController {
  final _userAPI = Get.find<UserAPI>();
  final _reportAPI = Get.find<ReportAPI>();

  final _userId = int.tryParse(Get.parameters["userId"].toString()) ?? 0;

  var user = User(id: 0, name: "", user_roles: []).obs;
  var isLoading = true.obs;
  var mainReport = {}.obs;

  void beforeStart() {
    if (_userId > 0) {
      getData();
    }
  }

  void getData() async {
    isLoading.value = true;
    if (Get.arguments == null) {
      var result = await _userAPI.findOne(_userId);
      if (result.body?["status"] ?? false) {
        user.value = User.fromJson(result.body["data"]);
      }
    } else {
      user.value = Get.arguments;
    }

    await getMainReport();

    isLoading.value = false;
  }

  Future getMainReport() async {
    var result = await _reportAPI.getMainReport(_userId);
    if (result.body?["status"] ?? false) {
      mainReport.value = result.body["data"];
    }
    return;
  }

  Future<void> saveAsFile(
    BuildContext context,
    LayoutCallback build,
    PdfPageFormat pageFormat,
  ) async {
    final bytes = await build(pageFormat);

    final appDocDir = await getApplicationDocumentsDirectory();
    final appDocPath = appDocDir.path;
    final file = File('$appDocPath/rapor.pdf');
    await file.writeAsBytes(bytes);
    await OpenFile.open(file.path);
  }
}
