import 'package:get/get.dart';
import 'package:saais_client/apis/report_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/main_report_detail/main_report_detail_controller.dart';

class MainReportDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => ReportAPI());
    Get.lazyPut(() => MainReportDetailController());
  }
}
