import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/competency_api.dart';
import 'package:saais_client/apis/competency_score_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/competency.dart';
import 'package:saais_client/models/competency_score.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class SubjectScoreDetailController extends GetxController {
  final _subjectAPI = Get.find<SubjectAPI>();
  final _classAPI = Get.find<ClassAPI>();
  final _competencyAPI = Get.find<CompetencyAPI>();
  final _competencyScoreAPI = Get.find<CompetencyScoreAPI>();
  final _userAPI = Get.find<UserAPI>();

  var classroom = Class().obs;
  var subject = Subject().obs;
  var competencyType = "knowledge".obs;

  var competency = Competency().obs;

  List<CompetencyScore> scores =
      List<CompetencyScore>.empty(growable: true).obs;
  List<CompetencyScore> allUsers =
      List<CompetencyScore>.empty(growable: true).obs;

  List<TextEditingController> scoreControllers =
      List<TextEditingController>.empty(growable: true).obs;

  final _subjectId = int.tryParse(Get.parameters["subjectId"].toString()) ?? 0;
  var isAddNew = false;

  void beforeStart() {
    competencyType.value = Get.parameters["type"] ?? "knowledge";

    if (_subjectId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _subjectAPI.findOne(_subjectId);
      if (result.body?["status"] ?? false) {
        subject.value = Subject.fromJson(result.body["data"]);
      }
    } else {
      subject.value = Get.arguments;
    }

    // get classes
    var classResult = await _classAPI.findOne(subject.value.class_id);

    if (classResult.body?["status"] ?? false) {
      classroom.value = Class.fromJson(classResult.body["data"]);
    }
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result = await _competencyScoreAPI.create(
        competency_id: competency.value.id, scores: scores);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "7", "tab": "1"});
    }
  }

  void getCompetencyScores() async {
    if (allUsers.isEmpty) {
      var findAllResult =
          await _userAPI.findAll(perPage: -1, class_id: classroom.value.id);
      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];
        for (var item in resultData) {
          allUsers.add(CompetencyScore(user: User.fromJson(item)));
        }
      }
    }

    var findAllResult =
        await _competencyScoreAPI.findAll(competencyId: competency.value.id);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      scores.assignAll(allUsers);
      scoreControllers.clear();

      for (var item in scores) {
        scoreControllers.add(TextEditingController(text: "0"));
      }

      for (var item in resultData) {
        var currentItem = CompetencyScore.fromJson(item);
        var userIndex =
            scores.indexWhere((item) => item.user!.id == currentItem.user!.id);
        scores[userIndex].score = currentItem.score;
        scoreControllers[userIndex].text = currentItem.score.toString();
      }
    }
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    List<Class> classes = [];

    var findAllResult = await _classAPI.findAll(search: filter ?? "");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classes.add(Class.fromJson(item));
      }
    }

    return classes;
  }

  Future<List<dynamic>> getSubjects(String? filter) async {
    List<Subject> subjects = [];

    if (classroom.value.id > 0) {
      var findAllResult = await _subjectAPI.findAll(
          search: filter ?? "", class_id: classroom.value.id);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];

        for (var item in resultData) {
          subjects.add(Subject.fromJson(item));
        }
      }
    }

    return subjects;
  }

  Future<List<dynamic>> getCompetencies(String? filter) async {
    List<Competency> competencies = [];

    if (subject.value.id > 0) {
      var findAllResult = await _competencyAPI.findAll(
          perPage: -1,
          search: filter ?? "",
          subject_id: subject.value.id,
          type: competencyType.value);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];

        for (var item in resultData) {
          competencies.add(Competency.fromJson(item));
        }
      }
    }

    return competencies;
  }
}
