import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/competency_api.dart';
import 'package:saais_client/apis/competency_score_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/subject_score_detail%20/subject_score_detail_controller.dart';

class SubjectScoreDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SubjectAPI());
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => ClassAPI());
    Get.lazyPut(() => CompetencyAPI());
    Get.lazyPut(() => CompetencyScoreAPI());
    Get.lazyPut(() => SubjectScoreDetailController());
  }
}
