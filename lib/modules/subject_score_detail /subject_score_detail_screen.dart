import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/competency.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/subject_score_detail%20/subject_score_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SubjectScoreDetailScreen extends GetView<SubjectScoreDetailController> {
  const SubjectScoreDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Nilai Mata Pelajaran",
      greySpace: false,
      includeScroll: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Expanded(
          child: Container(
            padding: const EdgeInsets.all(32),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: ColorValues.white,
            ),
            constraints: const BoxConstraints(maxWidth: 1000),
            child: Form(
              key: formKey,
              child: Obx(
                () => Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: _formGroup1()),
                        const SizedBox(
                          width: 16,
                        ),
                        Expanded(child: _formGroup2()),
                      ],
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    Expanded(child: _listScores()),
                    const SizedBox(height: 32),
                    _actionButton(formKey)
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () {
              if (formKey.currentState!.validate()) {
                controller.saveData();
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomDropdown(
            hint: "Kelas",
            showSearchBox: true,
            isFilterOnline: true,
            asyncItems: (String filter) => controller.getClasses(filter),
            itemToString: (item) => Class.getDropdownLabel(item),
            selectedItem: controller.classroom.value.id == 0
                ? null
                : controller.classroom.value,
            onChanged: (value) {
              controller.classroom.value = value;
              controller.subject.value = Subject();
              controller.competency.value = Competency();
            },
            validator: (value) {
              if (value?.id == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(
          height: 16,
        ),
        CustomDropdown(
            hint: "Tipe Kompetensi",
            items: const ["knowledge", "skill"],
            selectedItem: controller.competencyType.value,
            itemToString: (item) {
              switch (item) {
                case "knowledge":
                  return "Pengetahuan";
                case "skill":
                  return "Keterampilan";
                default:
                  return "";
              }
            },
            onChanged: (value) {
              controller.competencyType.value = value;
              controller.competency.value = Competency();
              controller.scores.clear();
            },
            validator: (value) {
              if (value == "") {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        CustomDropdown(
            hint: "Mata Pelajaran",
            showSearchBox: true,
            isFilterOnline: true,
            asyncItems: (String filter) => controller.getSubjects(filter),
            itemToString: (item) => Subject.getDropdownLabel(item),
            selectedItem: controller.subject.value.id == 0
                ? null
                : controller.subject.value,
            onChanged: (value) {
              controller.subject.value = value;
              controller.competency.value = Competency();
            },
            validator: (value) {
              if (value?.id == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(
          height: 16,
        ),
        CustomDropdown(
            hint: "Kompetensi",
            showSearchBox: true,
            asyncItems: (String filter) => controller.getCompetencies(filter),
            itemToString: (item) => Competency.getDropdownLabel(item),
            selectedItem: controller.competency.value.id == 0
                ? null
                : controller.competency.value,
            onChanged: (value) {
              controller.competency.value = value;
              controller.getCompetencyScores();
            },
            validator: (value) {
              if (value?.id == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
      ],
    );
  }

  Widget _listScores() {
    return Column(
      children: [
        const Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Daftar Siswa",
              style: TextStyleValues.textBold,
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Obx(
          () => Expanded(
            child: Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(color: ColorValues.greyLight, width: 1.5),
              ),
              child: controller.scores.isEmpty
                  ? const CustomEmptyState()
                  : ListView.builder(
                      itemCount: controller.scores.length,
                      itemBuilder: (context, i) {
                        var score = controller.scores[i];
                        return Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.0),
                                  color: ColorValues.greyBackground),
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 4,
                                    child: Text(
                                        User.getDropdownLabel(score.user!),
                                        style: TextStyleValues.textRegular),
                                  ),
                                  Expanded(
                                    child: Container(
                                        color: ColorValues.white,
                                        child: CustomTextInput(
                                          label: "Nilai",
                                          isInputDigit: true,
                                          controller:
                                              controller.scoreControllers[i],
                                          validator: (value) {
                                            var amount =
                                                int.tryParse(value ?? "") ?? 0;
                                            if (value == null || amount > 100) {
                                              return "Diisi 0-100";
                                            } else {
                                              return null;
                                            }
                                          },
                                          onChanged: (value) {
                                            controller.scores[i].score =
                                                int.tryParse(value) ?? 0;
                                          },
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 8,
                            )
                          ],
                        );
                      }),
            ),
          ),
        ),
      ],
    );
  }
}
