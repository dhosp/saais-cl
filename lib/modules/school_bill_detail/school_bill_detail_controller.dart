import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/school_bill_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/school_bill.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class SchoolBillDetailController extends GetxController {
  final _billAPI = Get.find<SchoolBillAPI>();

  final amountController = TextEditingController(text: "");
  final noteController = TextEditingController(text: "");

  var bill = SchoolBill().obs;

  var grade = "".obs;
  var regenerate = false.obs;

  final _billId = int.tryParse(Get.parameters["billId"].toString()) ?? 0;
  var isAddNew = false;

  void beforeStart() {
    if (_billId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _billAPI.findOne(_billId);
      if (result.body?["status"] ?? false) {
        bill.value = SchoolBill.fromJson(result.body["data"]);
      }
    } else {
      bill.value = Get.arguments;
    }
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result = await _billAPI.create(
        amount: int.tryParse(amountController.text) ?? 0,
        grade: grade.value,
        note: noteController.text,
        regenerate: regenerate.value,
        studentIds: []);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "8", "tab": "0"});
    }
  }

// func for modal
  void savePaymentData() async {
    CustomLoading.showLoading();
    Response result = await _billAPI.createPayment(
        bill_id: _billId, amount: int.parse(amountController.text));
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      getData();
      Get.close(1);
      amountController.text = "";
    }
  }

  void removePaymentData(int paymentId) async {
    CustomLoading.showLoading();
    Response result = await _billAPI.removePayment(payment_id: paymentId);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      getData();
      Get.close(1);
    }
  }

  // end func for modal
}
