import 'package:get/get.dart';
import 'package:saais_client/apis/school_bill_api.dart';
import 'package:saais_client/modules/school_bill_detail/school_bill_detail_controller.dart';

class SchoolBillDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SchoolBillAPI());
    Get.lazyPut(() => SchoolBillDetailController());
  }
}
