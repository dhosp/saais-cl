import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_checkbox.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/month.dart';
import 'package:saais_client/modules/school_bill_detail/school_bill_detail_controller.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SchoolBillDetailScreen extends GetView<SchoolBillDetailController> {
  const SchoolBillDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Tagihan Keuangan",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(() => Form(
                    key: formKey,
                    child: controller.isAddNew
                        ? _createNewFormGroup()
                        : Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(child: _formGroup1()),
                              const SizedBox(width: 16),
                              Expanded(child: _formGroup2())
                            ],
                          ),
                  )),
              const SizedBox(height: 32),
              controller.isAddNew ? _actionButton(formKey) : Container()
            ],
          ),
        ),
        controller.isAddNew
            ? Container()
            : Column(
                children: [
                  const SizedBox(height: 16),
                  Container(
                    padding: const EdgeInsets.all(32),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: ColorValues.white,
                    ),
                    constraints: const BoxConstraints(maxWidth: 1000),
                    child: _listPayment(),
                  )
                ],
              ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            const Expanded(
                child: Text(
              "Nominal Tagihan",
              style: TextStyleValues.textMedium,
            )),
            Expanded(
              flex: 2,
              child: Text(
                ": ${Formatter.numberFormatter(number: controller.bill.value.amount)}",
                style: TextStyleValues.textMedium,
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            const Expanded(
                child: Text(
              "Jumlah Dibayar",
              style: TextStyleValues.textMedium,
            )),
            Expanded(
              flex: 2,
              child: Text(
                ": ${Formatter.numberFormatter(number: controller.bill.value.paid_amount)}",
                style: TextStyleValues.textMedium,
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            const Expanded(
                child: Text(
              "Sisa Tagihan",
              style: TextStyleValues.textMedium,
            )),
            Expanded(
              flex: 2,
              child: Text(
                ": ${Formatter.numberFormatter(number: controller.bill.value.amount - controller.bill.value.paid_amount)}",
                style: TextStyleValues.textMedium,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () => {
              if (formKey.currentState!.validate()) {controller.saveData()}
            },
          ),
        ),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            const Expanded(
                child: Text(
              "Bulan",
              style: TextStyleValues.textMedium,
            )),
            Expanded(
              flex: 2,
              child: Text(
                ": ${Month.getDropdownLabel(controller.bill.value.month)} (Sem. ${controller.bill.value.school_period?.semester} ${controller.bill.value.school_period?.start_year} - ${controller.bill.value.school_period?.end_year})",
                style: TextStyleValues.textMedium,
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            const Expanded(
                child: Text(
              "Status",
              style: TextStyleValues.textMedium,
            )),
            Expanded(
              flex: 2,
              child: Text(
                ": ${controller.bill.value.status == "due" ? "Belum Dibayar" : controller.bill.value.status == "partial" ? "Dibayar Sebagian" : "Lunas"}",
                style: TextStyleValues.textMedium,
              ),
            ),
          ],
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            const Expanded(
                child: Text(
              "Catatan",
              style: TextStyleValues.textMedium,
            )),
            Expanded(
              flex: 2,
              child: Text(
                ": ${controller.bill.value.note}",
                style: TextStyleValues.textMedium,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _createNewFormGroup() {
    return Column(
      children: [
        CustomTextInput(
          label: "Tagihan Per Bulan",
          controller: controller.amountController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            value ??= "";
            if (value == "") {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomDropdown(
          hint: "Tingkat",
          items: GradeEnum.getDropdownItems(),
          itemToString: (item) => GradeEnum.getDropdownLabel(grade: item),
          selectedItem:
              controller.grade.value == "" ? null : controller.grade.value,
          onChanged: (value) {
            controller.grade.value = value;
          },
          validator: (value) {
            if (value == null) {
              return "Wajib diisi";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Catatan",
          inputType: TextInputType.multiline,
          maxLines: 3,
          controller: controller.noteController,
        ),
        const SizedBox(height: 16),
        CustomCheckbox(
            label: "Hapus tagihan yang sudah ada di kategori yang sama",
            value: controller.regenerate.value,
            onChanged: (value) {
              controller.regenerate.value = value ?? false;
            })
      ],
    );
  }

  Widget _listPayment() {
    final GlobalKey<FormState> modalFormKey = GlobalKey<FormState>();
    return Column(children: [
      Row(
        children: [
          const Expanded(
            child: Text(
              "Daftar Pembayaran",
              style: TextStyleValues.textBold,
            ),
          ),
          const SizedBox(height: 16),
          Obx(
            () => controller.bill.value.status == "paid"
                ? Container()
                : CustomButton(
                    title: "Tambah Pembayaran",
                    icon: Icons.add,
                    action: () {
                      CustomModal.showModal(
                          mainContent: _addPaymentModalContent(modalFormKey),
                          confirmAction: () {
                            if (modalFormKey.currentState!.validate()) {
                              controller.savePaymentData();
                            }
                          },
                          dismissAction: () {
                            controller.amountController.text = "";
                          });
                    },
                  ),
          )
        ],
      ),
      const SizedBox(
        height: 16,
      ),
      Column(
        children: [
          Obx(() => controller.bill.value.school_bill_payments?.isNotEmpty ??
                  false
              ? ListView.builder(
                  itemCount: controller.bill.value.school_bill_payments!.length,
                  shrinkWrap: true,
                  itemBuilder: (context, i) {
                    return Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Text(
                                  Formatter.dateFormatter(
                                      date: controller.bill.value
                                          .school_bill_payments![i].paid_at,
                                      type: "displayDateTime"),
                                  style: TextStyleValues.textRegular),
                              const SizedBox(
                                width: 32,
                              ),
                              Expanded(
                                child: Text(
                                    Formatter.numberFormatter(
                                        number: controller.bill.value
                                            .school_bill_payments![i].amount),
                                    style: TextStyleValues.textRegular),
                              ),
                              InkWell(
                                onTap: () {
                                  CustomDialog.showDialog(
                                      title: "Konfirmasi Hapus Pembayaran",
                                      message:
                                          "Menghapus pembayaran akan mengubah status tagihan. Apakah kamu yakin?",
                                      confirmText: "Ya, yakin",
                                      confirmAction: () {
                                        controller.removePaymentData(controller
                                            .bill
                                            .value
                                            .school_bill_payments![i]
                                            .id);
                                      });
                                },
                                child: const Icon(
                                  Icons.delete_forever_rounded,
                                  color: ColorValues.red,
                                ),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        )
                      ],
                    );
                  })
              : const CustomEmptyState()),
        ],
      )
    ]);
  }

  Widget _addPaymentModalContent(modalFormKey) {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Pembayaran",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Form(
          key: modalFormKey,
          child: CustomTextInput(
            controller: controller.amountController,
            label: "Jumlah Dibayar",
            validator: (value) {
              int amount = int.tryParse(value ?? "") ?? 0;
              if (amount <= 0) {
                return "Wajib diisi";
              } else if (amount >
                  (controller.bill.value.amount -
                      controller.bill.value.paid_amount)) {
                return "Nominal maksimal ${Formatter.numberFormatter(number: controller.bill.value.amount - controller.bill.value.paid_amount)}";
              } else {
                return null;
              }
            },
          ),
        ),
      ],
    );
  }
}
