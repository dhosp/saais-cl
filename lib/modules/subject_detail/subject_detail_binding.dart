import 'package:get/get.dart';
import 'package:saais_client/apis/competency_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';

class SubjectDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => SubjectAPI());
    Get.lazyPut(() => CompetencyAPI());
    Get.lazyPut(() => SubjectDetailController());
  }
}
