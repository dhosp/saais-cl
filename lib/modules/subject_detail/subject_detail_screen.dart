import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_checkbox.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/major_enum.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SubjectDetailScreen extends GetView<SubjectDetailController> {
  const SubjectDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Mata Pelajaran",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(() => Form(
                    key: formKey,
                    child: controller.isAddNew
                        ? _createNewFormGroup()
                        : Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(child: _formGroup1()),
                              const SizedBox(width: 16),
                              Expanded(child: _formGroup2()),
                            ],
                          ),
                  )),
              const SizedBox(height: 32),
              _actionButton(formKey)
            ],
          ),
        ),
        controller.isAddNew
            ? Container()
            : Column(
                children: [
                  const SizedBox(height: 16),
                  Container(
                    padding: const EdgeInsets.all(32),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: ColorValues.white,
                    ),
                    constraints: const BoxConstraints(maxWidth: 1000),
                    child: _listCompetencies(),
                  )
                ],
              ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () {
              if (formKey.currentState!.validate()) {
                controller.saveData();
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomDropdown(
            hint: "Kelas",
            itemToString: (item) =>
                Class.getDropdownLabel(controller.classroom.value),
            selectedItem: controller.classroom.value.id == 0
                ? null
                : controller.classroom.value,
            enabled: false),
        const SizedBox(height: 16),
        Row(children: [
          Expanded(
            child: CustomTextInput(
              label: "Kode",
              readOnly: true,
              controller: controller.codeController,
            ),
          ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
            child: CustomTextInput(
              label: "Nama",
              readOnly: true,
              controller: controller.nameController,
            ),
          ),
        ]),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        CustomDropdown(
            hint: "Guru Pengampu",
            showSearchBox: true,
            asyncItems: (String filter) => controller.getTeachers(filter),
            itemToString: (item) => User.getDropdownLabel(item),
            selectedItem: controller.teacher.value.id == 0
                ? null
                : controller.teacher.value,
            onChanged: (value) {
              controller.teacher.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "KKM",
          controller: controller.scoreController,
          isInputDigit: true,
          inputType: TextInputType.number,
          validator: (value) {
            int amount = int.tryParse(value ?? "") ?? 0;
            if (amount < 0 || amount > 100) {
              return "Diisi 0-100";
            } else {
              return null;
            }
          },
        ),
      ],
    );
  }

  Widget _createNewFormGroup() {
    return Column(
      children: [
        CustomTextInput(
          label: "Nama",
          controller: controller.nameController,
          validator: (value) {
            value ??= "";
            if (value.length < 3) {
              return "Diisi minimal 3 karakter";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(height: 16),
        CustomTextInput(
          label: "Kode",
          controller: controller.codeController,
          validator: (value) {
            value ??= "";
            if (value.length < 3) {
              return "Diisi minimal 3 karakter";
            } else {
              return null;
            }
          },
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              child: CustomDropdown(
                  hint: "Tingkat",
                  items: GradeEnum.getDropdownItems(),
                  itemToString: (item) =>
                      GradeEnum.getDropdownLabel(grade: item),
                  selectedItem: controller.grade.value == ""
                      ? null
                      : controller.grade.value,
                  onChanged: (value) {
                    controller.grade.value = value;
                  },
                  validator: (value) {
                    if (value == null) {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDropdown(
                  hint: "Jurusan",
                  items: MajorEnum.getDropdownItems(),
                  itemToString: (item) =>
                      MajorEnum.getDropdownLabel(major: item),
                  selectedItem: controller.major.value == ""
                      ? null
                      : controller.major.value,
                  onChanged: (value) {
                    controller.major.value = value;
                  },
                  validator: (value) {
                    if (value == null) {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
          ],
        ),
        const SizedBox(height: 16),
        CustomDropdown(
            hint: "Guru Pengampu",
            showSearchBox: true,
            asyncItems: (String filter) => controller.getTeachers(filter),
            itemToString: (item) => User.getDropdownLabel(item),
            selectedItem: controller.teacher.value.id == 0
                ? null
                : controller.teacher.value,
            onChanged: (value) {
              controller.teacher.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(height: 16),
        CustomCheckbox(
            label: "Hapus mata pelajaran yang sudah ada di kategori yang sama",
            value: controller.regenerate.value,
            onChanged: (value) {
              controller.regenerate.value = value ?? false;
            })
      ],
    );
  }

  Widget _listCompetencies() {
    final GlobalKey<FormState> modalFormKey = GlobalKey<FormState>();
    return Obx(
      () => Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Daftar Kompetensi",
              style: TextStyleValues.textBold,
            ),
            CustomButton(
              title: "Tambah Kompetensi",
              icon: Icons.add,
              action: () {
                CustomModal.showModal(
                    mainContent: _addCompetencyModalContent(modalFormKey),
                    confirmAction: () {
                      if (modalFormKey.currentState!.validate()) {
                        controller.saveCompetencyData();
                      }
                    },
                    dismissAction: () {
                      controller.competencyCodeController.text = "";
                      controller.competencyDescController.text = "";
                    });
              },
            )
          ],
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            Expanded(
              child: CustomDropdown(
                hint: "Tipe Kompetensi",
                items: const ["knowledge", "skill"],
                selectedItem: controller.competencyType.value,
                itemToString: (item) {
                  switch (item) {
                    case "knowledge":
                      return "Pengetahuan";
                    case "skill":
                      return "Keterampilan";
                    default:
                      return "";
                  }
                },
                onChanged: (value) {
                  controller.competencyType.value = value;
                  controller.getData();
                },
              ),
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomTextInput(
                label: "Cari Kode/Nama Kompetensi",
                suffixIcon: const Icon(Icons.search),
                onTapSuffixIcon: () {},
                onFieldSubmitted: (value) {},
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Column(
          children: [
            Obx(() => controller.competencies.isNotEmpty
                ? ListView.builder(
                    itemCount: controller.competencies.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) {
                      var competency = controller.competencies[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                      "${competency.code} - ${competency.description.length > 40 ? competency.description.substring(0, 40) : competency.description}",
                                      style: TextStyleValues.textRegular),
                                ),
                                InkWell(
                                  onTap: () {
                                    CustomDialog.showDialog(
                                        title: "Konfirmasi Hapus Kompetensi",
                                        message:
                                            "Menghapus kompetensi berarti menghapus semua riwayat nilai yang berkaitan. Apakah kamu yakin?",
                                        confirmText: "Ya, yakin",
                                        confirmAction: () {
                                          controller.removeCompetencyData(
                                              competency.id);
                                        });
                                  },
                                  child: const Icon(
                                    Icons.delete_forever_rounded,
                                    color: ColorValues.red,
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    })
                : const CustomEmptyState()),
          ],
        )
      ]),
    );
  }

  Widget _addCompetencyModalContent(modalFormKey) {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Kompetensi",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Form(
          key: modalFormKey,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  children: [
                    CustomDropdown(
                      hint: "Tipe Kompetensi",
                      items: const ["knowledge", "skill"],
                      selectedItem: controller.competencyType.value,
                      itemToString: (item) {
                        switch (item) {
                          case "knowledge":
                            return "Pengetahuan";
                          case "skill":
                            return "Keterampilan";
                          default:
                            return "";
                        }
                      },
                      onChanged: (value) {
                        controller.competencyType.value = value;
                        controller.getData();
                      },
                      validator: (value) {
                        if (value == null) {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextInput(
                      label: "Kode",
                      controller: controller.competencyCodeController,
                      validator: (value) {
                        value ??= "";
                        if (value.isEmpty) {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      },
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    CustomTextInput(
                      label: "Deskripsi",
                      controller: controller.competencyDescController,
                      validator: (value) {
                        value ??= "";
                        if (value.isEmpty) {
                          return "Wajib diisi";
                        } else {
                          return null;
                        }
                      },
                    )
                  ],
                ),
              ),
              const SizedBox(
                width: 16,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
