import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/competency_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/competency.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class SubjectDetailController extends GetxController {
  final _subjectAPI = Get.find<SubjectAPI>();
  final _userAPI = Get.find<UserAPI>();
  final _competencyAPI = Get.find<CompetencyAPI>();

  final nameController = TextEditingController(text: "");
  final codeController = TextEditingController(text: "");
  final scoreController = TextEditingController(text: "");
  var grade = "".obs;
  var major = "".obs;
  var regenerate = false.obs;
  var teacher = User(id: 0, name: "", username: "", user_roles: []).obs;
  var classroom = Class().obs;
  List<Competency> competencies = List<Competency>.empty(growable: true).obs;

  final _subjectId = int.tryParse(Get.parameters["subjectId"].toString()) ?? 0;
  var isAddNew = false;
  var subject = Subject().obs;

// var for modal
  var competencyType = "knowledge".obs;
  final competencyCodeController = TextEditingController(text: "");
  final competencyDescController = TextEditingController(text: "");
  // end var for modal

  void beforeStart() {
    if (_subjectId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _subjectAPI.findOne(_subjectId);
      if (result.body?["status"] ?? false) {
        subject.value = Subject.fromJson(result.body["data"]);
      }
    } else {
      subject.value = Get.arguments;
    }

    nameController.text = subject.value.name;
    codeController.text = subject.value.code;
    scoreController.text = subject.value.pass_score.toString();

    teacher.value = User(
        id: subject.value.user!.id,
        name: subject.value.user!.name,
        username: "",
        user_roles: []);

    classroom.value = Class(
        id: subject.value.class_id,
        name: subject.value.class_name,
        grade: subject.value.class_grade,
        major_name: subject.value.class_major_name,
        major_id: subject.value.class_major_id);

    getCompetencyList("");
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result;
    if (isAddNew) {
      result = await _subjectAPI.create(
          name: nameController.text,
          code: codeController.text,
          teacher_id: teacher.value.id,
          class_major_id: int.parse(major.value),
          class_grade: grade.value,
          regenerate: regenerate.value);
    } else {
      result = await _subjectAPI.update(
          id: _subjectId,
          teacher_id: teacher.value.id,
          pass_score: int.tryParse(scoreController.text) ?? 0);
    }
    CustomLoading.hideLoading();

    if ((result.body?["status"] ?? false) && isAddNew) {
      isAddNew = false;
      grade.value = "";
      major.value = "";
      regenerate.value = false;
      teacher.value = User(id: 0, name: "", username: "", user_roles: []);
      classroom.value = Class();
      nameController.text = "";
      codeController.text = "";
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "3", "tab": "0"});
    }
  }

  Future<List<dynamic>> getTeachers(String? filter) async {
    List<User> teachers = [];

    var teacherFindAllResult =
        await _userAPI.findAll(role: "teacher", search: filter ?? "");

    if (teacherFindAllResult.body != null &&
            teacherFindAllResult.body?["status"] ??
        false) {
      List<dynamic> resultData = teacherFindAllResult.body["data"];

      for (var item in resultData) {
        teachers.add(User.fromJson(item));
      }
    }

    return teachers;
  }

  void getCompetencyList(String? filter) async {
    var findAllResult = await _competencyAPI.findAll(
        perPage: -1, subject_id: subject.value.id, search: filter ?? "");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      competencies.clear();
      for (var item in resultData) {
        competencies.add(Competency.fromJson(item));
      }
    }
  }

  void saveCompetencyData() async {
    CustomLoading.showLoading();
    Response result = await _competencyAPI.create(
        classroom: classroom.value,
        subject: subject.value,
        competency: Competency(
            type: competencyType.value,
            code: competencyCodeController.text,
            description: competencyDescController.text));
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      Get.close(1);
      competencyCodeController.text = "";
      competencyDescController.text = "";
      getData();
    }
  }

  void removeCompetencyData(int competencyId) async {
    CustomLoading.showLoading();
    Response result = await _competencyAPI.remove(competencyId);
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      Get.close(1);
      getData();
    }
  }
}
