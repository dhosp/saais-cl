import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/extracurricular_score_detail/extracurricular_score_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class ExtracurricularScoreDetailScreen
    extends GetView<ExtracurricularScoreDetailController> {
  const ExtracurricularScoreDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Nilai Ekstrakurikuler",
      greySpace: false,
      includeScroll: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Expanded(
          child: Container(
            padding: const EdgeInsets.all(32),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              color: ColorValues.white,
            ),
            constraints: const BoxConstraints(maxWidth: 1000),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Obx(() => Expanded(child: _formGroup1(formKey))),
                  const SizedBox(height: 32),
                  _actionButton(formKey)
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () {
              if (formKey.currentState!.validate()) {
                controller.saveData();
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _formGroup1(formKey) {
    return Column(
      children: [
        CustomDropdown(
            hint: "Ekstrakurikuler",
            showSearchBox: true,
            isFilterOnline: true,
            asyncItems: (String filter) =>
                controller.getExtracurriculars(filter),
            itemToString: (item) => Extracurricular.getDropdownLabel(item),
            selectedItem: controller.extracurricular.value.id == 0
                ? null
                : controller.extracurricular.value,
            onChanged: (value) {
              controller.extracurricular.value = value;
              controller.getScores(value.id);
            },
            validator: (value) {
              if (value?.id == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text(
              "Daftar Siswa",
              style: TextStyleValues.textBold,
            ),
            CustomButton(
              title: "Tambah Siswa",
              icon: Icons.add,
              action: () {
                if (formKey.currentState!.validate()) {
                  CustomModal.showModal(
                      onInit: controller.getAllStudents,
                      mainContent: _addStudentModalContent(),
                      confirmAction: () {
                        if (controller.tempScoreUsers.isNotEmpty) {
                          controller.updateStudentScores();
                          controller.tempScoreUserIds.clear();
                          controller.tempScoreUsers.clear();
                        }
                      },
                      dismissAction: () {
                        controller.tempScoreUserIds.clear();
                        controller.tempScoreUsers.clear();
                      });
                } else {}
              },
            )
          ],
        ),
        const SizedBox(height: 16),
        Expanded(
          child: Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              border: Border.all(color: ColorValues.greyLight, width: 1.5),
            ),
            child: controller.scores.isEmpty
                ? const CustomEmptyState()
                : ListView.builder(
                    itemCount: controller.scores.length,
                    itemBuilder: (context, i) {
                      var score = controller.scores[i];
                      return Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4.0),
                                color: ColorValues.greyBackground),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 4,
                                  child: Text(
                                      User.getDropdownLabel(score.user!),
                                      style: TextStyleValues.textRegular),
                                ),
                                Expanded(
                                  child: CustomDropdown(
                                    hint: "Predikat",
                                    selectedItem: score.grade,
                                    items: const ['SB', 'B', 'C', 'K'],
                                    onChanged: (value) {
                                      controller.scores[i].grade = value;
                                    },
                                    validator: (value) {
                                      if (value == "") {
                                        return "Wajib diisi";
                                      } else {
                                        return null;
                                      }
                                    },
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                InkWell(
                                  onTap: () {
                                    CustomDialog.showDialog(
                                        title: "Konfirmasi Hapus Siswa",
                                        message: "Apakah kamu yakin?",
                                        confirmText: "Ya, yakin",
                                        confirmAction: () {
                                          controller.scoreUserIds.removeWhere(
                                              (item) =>
                                                  item ==
                                                  controller
                                                      .scores[i].user!.id);
                                          controller.scores.removeWhere(
                                              (item) =>
                                                  item.user!.id ==
                                                  controller
                                                      .scores[i].user!.id);
                                          Get.close(1);
                                        });
                                  },
                                  child: const Icon(
                                    Icons.delete_forever_rounded,
                                    color: ColorValues.red,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          )
                        ],
                      );
                    }),
          ),
        ),
      ],
    );
  }

  Widget _addStudentModalContent() {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Siswa",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        const Row(
          children: [
            Expanded(
              flex: 3,
              child: CustomTextInput(
                // controller: controller.searchDialogController,
                label: "Cari NIPD/Nama Siswa",
                suffixIcon: Icon(Icons.search),
                // onTapSuffixIcon: () {
                //   controller.getAllStudents(
                //       search: controller.searchDialogController.text);
                // },
                // onFieldSubmitted: (value) {
                //   controller.getAllStudents(search: value);
                // }
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Obx(() => controller.allStudentScores.isNotEmpty
            ? ListView.builder(
                itemCount: controller.allStudentScores.length,
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  var score = controller.allStudentScores[i];
                  return Column(
                    children: [
                      InkWell(
                        onTap: () {
                          controller.selectStudentIds(score);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              color: ColorValues.greyBackground),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text(User.getDropdownLabel(score.user!),
                                    style: TextStyleValues.textRegular),
                              ),
                              Obx(() => controller.tempScoreUserIds.contains(
                                      controller.allStudentScores[i].user!.id)
                                  ? const Icon(
                                      Icons.check_box_rounded,
                                      color: ColorValues.bluePrimary,
                                    )
                                  : const Icon(
                                      Icons.check_box_outline_blank_rounded,
                                      color: ColorValues.grey,
                                    ))
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      )
                    ],
                  );
                })
            : const CustomEmptyState()),
      ],
    );
  }
}
