import 'package:get/get.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/extracurricular_score_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_controller.dart';
import 'package:saais_client/modules/extracurricular_score_detail/extracurricular_score_detail_controller.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';

class ExtracurricularScoreDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ExtracurricularAPI());
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => ExtracurricularScoreAPI());
    Get.lazyPut(() => ExtracurricularScoreDetailController());
  }
}
