import 'package:get/get.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/extracurricular_score_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/extracurricular.dart';
import 'package:saais_client/models/extracurricular_score.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class ExtracurricularScoreDetailController extends GetxController {
  final _extracurricularAPI = Get.find<ExtracurricularAPI>();
  final _extracurricularScoreAPI = Get.find<ExtracurricularScoreAPI>();
  final _userAPI = Get.find<UserAPI>();

  var extracurricular = Extracurricular().obs;
  List<ExtracurricularScore> scores =
      List<ExtracurricularScore>.empty(growable: true).obs;
  List<int> scoreUserIds = List<int>.empty(growable: true).obs;

  final _exculId = int.tryParse(Get.parameters["exculId"].toString()) ?? 0;
  var isAddNew = false;

  // var for modal
  List<ExtracurricularScore> allStudentScores =
      List<ExtracurricularScore>.empty(growable: true).obs;
  List<ExtracurricularScore> tempScoreUsers =
      List<ExtracurricularScore>.empty(growable: true).obs;
  List<int> tempScoreUserIds = List<int>.empty(growable: true).obs;
  // end modal var

  void beforeStart() {
    if (_exculId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _extracurricularAPI.findOne(_exculId);
      if (result.body?["status"] ?? false) {
        extracurricular.value = Extracurricular.fromJson(result.body["data"]);
      }
    } else {
      extracurricular.value = Get.arguments;
    }

    getScores(extracurricular.value.id);
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result = await _extracurricularScoreAPI.create(
        exculId: extracurricular.value.id, scores: scores);
    CustomLoading.hideLoading();
    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "7", "tab": "3"});
    }
  }

  void getScores(int exculId) async {
    var findAllResult =
        await _extracurricularScoreAPI.findAll(exculId: exculId);
    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];
      scores.clear();
      scoreUserIds.clear();
      for (var item in resultData) {
        var currentItem = ExtracurricularScore.fromJson(item);
        scores.add(currentItem);
        scoreUserIds.add(currentItem.user!.id);
      }
    }
  }

  Future<List<dynamic>> getExtracurriculars(String? filter) async {
    List<Extracurricular> extracurriculars = [];

    var findAllResult = await _extracurricularAPI.findAll(search: filter ?? "");

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        extracurriculars.add(Extracurricular.fromJson(item));
      }
    }

    return extracurriculars;
  }

  // func for modal
  void getAllStudents({String search = ""}) async {
    var findAllResult = await _userAPI.findAll(role: "student", search: search);

    List<dynamic> resultData = findAllResult.body["data"];

    allStudentScores.clear();
    for (var item in resultData) {
      if (!scoreUserIds.contains(item["id"])) {
        allStudentScores.add(ExtracurricularScore.fromJson(
            {"id": 0, "grade": "", "user": item}));
      }
    }
  }

  void selectStudentIds(ExtracurricularScore score) {
    int index = tempScoreUserIds.indexOf(score.user!.id);
    int objIndex =
        tempScoreUsers.indexWhere((item) => item.user!.id == score.user!.id);
    if (index >= 0) {
      tempScoreUserIds.removeAt(index);
    } else {
      tempScoreUserIds.add(score.user!.id);
    }

    if (objIndex >= 0) {
      tempScoreUsers.removeAt(index);
    } else {
      tempScoreUsers.add(score);
    }
  }

  void updateStudentScores() async {
    scores.addAll(tempScoreUsers);
    scoreUserIds.addAll(tempScoreUserIds);
    Get.close(1);
  }
// end func for modal
}
