import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_date_picker.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/grade_enum.dart';
import 'package:saais_client/models/major_enum.dart';
import 'package:saais_client/models/schedule.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/models/user.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_controller.dart';
import 'package:saais_client/modules/schedule_detail/schedule_detail_controller.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';

class ScheduleDetailScreen extends GetView<ScheduleDetailController> {
  const ScheduleDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(formKey),
      landscapeLayout: _landscapeLayout(formKey),
      includeAppBar: true,
      appBarTitle: "Detail Jadwal",
      greySpace: false,
    );
  }

  Widget _potraitLayout(formKey) {
    return Container();
  }

  Widget _landscapeLayout(formKey) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(32),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: ColorValues.white,
          ),
          constraints: const BoxConstraints(maxWidth: 1000),
          child: Column(
            children: [
              Obx(
                () => Form(
                  key: formKey,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(child: _formGroup1()),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        child: _formGroup2(),
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 32),
              _actionButton(formKey)
            ],
          ),
        ),
      ],
    );
  }

  Widget _actionButton(formKey) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(flex: 3, child: Container()),
        Expanded(
          child: CustomButton(
            title: "Simpan",
            action: () {
              if (formKey.currentState!.validate()) {
                controller.saveData();
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _formGroup1() {
    return Column(
      children: [
        CustomDropdown(
            hint: "Kelas",
            showSearchBox: true,
            asyncItems: (String filter) => controller.getClasses(filter),
            itemToString: (item) => Class.getDropdownLabel(item),
            selectedItem: controller.classroom.value.id == 0
                ? null
                : controller.classroom.value,
            onChanged: (value) {
              controller.classroom.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        SizedBox(
          height: 16,
        ),
        CustomDropdown(
            hint: "Mata Pelajaran",
            showSearchBox: true,
            asyncItems: (String filter) => controller.getSubjects(filter),
            itemToString: (item) => Subject.getDropdownLabel(item),
            selectedItem: controller.subject.value.id == 0
                ? null
                : controller.subject.value,
            onChanged: (value) {
              controller.subject.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
      ],
    );
  }

  Widget _formGroup2() {
    return Column(
      children: [
        CustomDropdown(
            hint: "Hari",
            items: [
              0,
              1,
              2,
              3,
              4,
              5,
              6,
            ],
            itemToString: (item) => Schedule.getDayDropdownLabel(item),
            selectedItem: controller.day.value,
            onChanged: (value) {
              controller.day.value = value;
            },
            validator: (value) {
              if (value == null) {
                return "Wajib diisi";
              } else {
                return null;
              }
            }),
        SizedBox(
          height: 16,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: CustomDatePicker(
                  label: "Jam Mulai",
                  controller: controller.startTimeController,
                  dateValueCallback: controller.startTimeCallback,
                  date: controller.startTime.value,
                  type: "time",
                  validator: (value) {
                    if (value == "") {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            ),
            SizedBox(
              width: 16,
            ),
            Expanded(
              child: CustomDatePicker(
                  label: "Jam Selesai",
                  controller: controller.finishTimeController,
                  dateValueCallback: controller.finishTimeCallback,
                  date: controller.finishTime.value,
                  type: "time",
                  validator: (value) {
                    if (value == "") {
                      return "Wajib diisi";
                    } else {
                      return null;
                    }
                  }),
            )
          ],
        )
      ],
    );
  }
}
