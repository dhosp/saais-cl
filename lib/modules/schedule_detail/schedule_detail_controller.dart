import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/schedule_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/class.dart';
import 'package:saais_client/models/schedule.dart';
import 'package:saais_client/models/subject.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class ScheduleDetailController extends GetxController {
  final _scheduleAPI = Get.find<ScheduleAPI>();
  final _classAPI = Get.find<ClassAPI>();
  final _subjectAPI = Get.find<SubjectAPI>();

  final _scheduleId =
      int.tryParse(Get.parameters["scheduleId"].toString()) ?? 0;
  var isAddNew = false;

  var schedule = Schedule().obs;
  var classroom = Class().obs;
  var subject = Subject().obs;
  var day = 0.obs;
  final startTimeController = TextEditingController(text: "");
  var startTime = DateTime.tryParse("").obs;
  final finishTimeController = TextEditingController(text: "");
  var finishTime = DateTime.tryParse("").obs;

  void beforeStart() {
    if (_scheduleId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _scheduleAPI.findOne(_scheduleId);
      if (result.body?["status"] ?? false) {
        schedule.value = Schedule.fromJson(result.body["data"]);
      }
    } else {
      schedule.value = Get.arguments;
    }

    classroom.value = Class(
        id: schedule.value.subject!.class_id,
        name: schedule.value.subject!.class_name,
        grade: schedule.value.subject!.class_grade,
        major_name: schedule.value.subject!.class_major_name);
    subject.value = schedule.value.subject!;
    day.value = schedule.value.day;
    var currentDate = DateTime.now();
    startTimeController.text = schedule.value.start_time.substring(0, 5);
    startTime.value = DateTime(
        currentDate.year,
        currentDate.month,
        currentDate.day,
        int.tryParse(schedule.value.start_time.substring(0, 2)) ?? 0,
        int.tryParse(schedule.value.start_time.substring(3, 5)) ?? 0);
    finishTimeController.text = schedule.value.finish_time.substring(0, 5);
    finishTime.value = DateTime(
        currentDate.year,
        currentDate.month,
        currentDate.day,
        int.tryParse(schedule.value.finish_time.substring(0, 2)) ?? 0,
        int.tryParse(schedule.value.finish_time.substring(3, 5)) ?? 0);
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result;
    if (isAddNew) {
      result = await _scheduleAPI.create(
          schedule: Schedule(
              day: day.value,
              start_time: startTimeController.text,
              finish_time: finishTimeController.text,
              subject: Subject(
                id: subject.value.id,
              )));
    } else {
      result = await _scheduleAPI.update(
          id: _scheduleId,
          schedule: Schedule(
            day: day.value,
            start_time: startTimeController.text,
            finish_time: finishTimeController.text,
          ));
    }
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "2", "tab": "1"});
    }
  }

  Future<List<dynamic>> getClasses(String? filter) async {
    List<Class> classes = [];

    var findAllResult = await _classAPI.findAll();

    if (findAllResult.body?["status"] ?? false) {
      List<dynamic> resultData = findAllResult.body["data"];

      for (var item in resultData) {
        classes.add(Class.fromJson(item));
      }
    }

    return classes;
  }

  Future<List<dynamic>> getSubjects(String? filter) async {
    List<Subject> subjects = [];

    if (classroom.value.id > 0) {
      var findAllResult =
          await _subjectAPI.findAll(class_id: classroom.value.id);

      if (findAllResult.body?["status"] ?? false) {
        List<dynamic> resultData = findAllResult.body["data"];

        for (var item in resultData) {
          subjects.add(Subject.fromJson(item));
        }
      }
    }

    return subjects;
  }

  startTimeCallback(DateTime date) {
    startTime.value = date;
  }

  finishTimeCallback(DateTime date) {
    finishTime.value = date;
  }
}
