import 'package:get/get.dart';
import 'package:saais_client/apis/class_api.dart';
import 'package:saais_client/apis/extracurricular_api.dart';
import 'package:saais_client/apis/schedule_api.dart';
import 'package:saais_client/apis/subject_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_controller.dart';
import 'package:saais_client/modules/schedule_detail/schedule_detail_controller.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_controller.dart';

class ScheduleDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ScheduleAPI());
    Get.lazyPut(() => ClassAPI());
    Get.lazyPut(() => SubjectAPI());
    Get.lazyPut(() => ScheduleDetailController());
  }
}
