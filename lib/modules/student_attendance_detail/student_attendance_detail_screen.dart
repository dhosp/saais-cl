import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/custom_widgets/custom_button.dart';
import 'package:saais_client/custom_widgets/custom_content_header.dart';
import 'package:saais_client/custom_widgets/custom_date_picker.dart';
import 'package:saais_client/custom_widgets/custom_dialog.dart';
import 'package:saais_client/custom_widgets/custom_dropdown.dart';
import 'package:saais_client/custom_widgets/custom_empty_state.dart';
import 'package:saais_client/custom_widgets/custom_modal.dart';
import 'package:saais_client/custom_widgets/custom_text_input.dart';
import 'package:saais_client/modules/student_attendance_detail/student_attendance_detail_controller.dart';
import 'package:saais_client/utils/formatter.dart';
import 'package:saais_client/utils/responsive_layout.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class StudentAttendanceDetailScreen
    extends GetView<StudentAttendanceDetailController> {
  const StudentAttendanceDetailScreen({super.key});
  @override
  Widget build(BuildContext context) {
    controller.beforeStart();
    return ResponsiveLayout(
      potraitLayout: _potraitLayout(),
      landscapeLayout: _landscapeLayout(),
      includeAppBar: true,
      greySpace: false,
      includeScroll: false,
    );
  }

  Widget _potraitLayout() {
    return Container();
  }

  Widget _landscapeLayout() {
    final GlobalKey<FormState> modalFormKey = GlobalKey<FormState>();
    return Container(
        constraints: const BoxConstraints(maxWidth: 1000),
        child: Column(children: [
          Obx(
            () => CustomContentHeader(
              title: "Daftar Kehadiran ${controller.userName.value}",
              totalData: controller.totalData.value,
              totalDisplayedData: controller.displayedData,
              onRefresh: () => controller.getList(),
              actions: [
                const CustomButton(
                  icon: Icons.folder_open,
                  buttonType: "secondary",
                ),
                const SizedBox(
                  width: 16,
                ),
                const CustomButton(
                  icon: Icons.file_download_outlined,
                  buttonType: "secondary",
                ),
                const SizedBox(
                  width: 16,
                ),
                CustomButton(
                  icon: Icons.add,
                  title: "Tambah",
                  buttonType: "secondary",
                  action: () => {
                    CustomModal.showModal(
                        mainContent: _addAttendanceModalContent(modalFormKey),
                        confirmAction: () {
                          if (modalFormKey.currentState!.validate()) {
                            controller.saveAttendanceData();
                          }
                        },
                        dismissAction: () {
                          controller.getList();
                        })
                  },
                )
              ],
              filters: [
                Expanded(
                  child: CustomDropdown(
                    items: const ["1", "2"],
                    hint: "Semua Kehadiran",
                    onChanged: (value) => {},
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: CustomDropdown(
                    items: const ["1", "2"],
                    hint: "Tanggal Mulai",
                    onChanged: (value) => {},
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                Expanded(
                  child: CustomDropdown(
                    items: const ["1", "2"],
                    hint: "Tanggal Selesai",
                    onChanged: (value) => {},
                  ),
                ),
              ],
            ),
          ),
          Obx(
            () => Expanded(
              child: controller.totalData > 0
                  ? Column(children: [
                      Container(
                        padding: const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Tanggal",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Kehadiran",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Jam Masuk",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              child: Text(
                                "Jam Pulang",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Text(
                                "Catatan",
                                textAlign: TextAlign.center,
                                style: TextStyleValues.textMedium
                                    .copyWith(color: ColorValues.grey),
                              ),
                            ),
                            const Icon(Icons.more_vert,
                                color: Colors.transparent)
                          ],
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: controller.attendances.length,
                          itemBuilder: (context, i) {
                            return Column(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(4),
                                    color: ColorValues.white,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                            Formatter.dateFormatter(
                                                date: controller.attendances[i]
                                                    .clock_in_date!,
                                                type: "display"),
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            controller.attendances[i].type ==
                                                    "i"
                                                ? "Izin"
                                                : controller.attendances[i]
                                                            .type ==
                                                        "a"
                                                    ? "Tanpa Keterangan"
                                                    : controller.attendances[i]
                                                                .type ==
                                                            "h"
                                                        ? "Hadir"
                                                        : controller
                                                                    .attendances[
                                                                        i]
                                                                    .type ==
                                                                "s"
                                                            ? "Sakit"
                                                            : "-",
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                            Formatter.dateFormatter(
                                                date: controller.attendances[i]
                                                    .clock_in_at!,
                                                type: "displayTime"),
                                            textAlign: TextAlign.center,
                                            style: TextStyleValues.textMedium),
                                      ),
                                      Expanded(
                                        child: Text(
                                          controller.attendances[i]
                                                      .clock_out_at !=
                                                  null
                                              ? Formatter.dateFormatter(
                                                  date: controller
                                                      .attendances[i]
                                                      .clock_out_at!,
                                                  type: "displayTime")
                                              : "-",
                                          textAlign: TextAlign.center,
                                          style: TextStyleValues.textMedium,
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Text(
                                          (controller.attendances[i].reason ??
                                                          "")
                                                      .length >
                                                  20
                                              ? ("${controller.attendances[i].reason!.substring(0, 20)}...")
                                              : controller
                                                      .attendances[i].reason ??
                                                  "",
                                          textAlign: TextAlign.center,
                                          style: TextStyleValues.textMedium,
                                        ),
                                      ),
                                      InkWell(
                                        onTap: () {
                                          CustomDialog.showDialog(
                                              title:
                                                  "Konfirmasi Hapus Kehadiran",
                                              message:
                                                  "Kamu akan menghapus data kehadiran. Apakah kamu yakin?",
                                              confirmText: "Ya, yakin",
                                              confirmAction: () {
                                                controller.removeAttendanceData(
                                                    controller
                                                        .attendances[i].id);
                                              });
                                        },
                                        child: const Icon(
                                            Icons.delete_forever_rounded,
                                            color: ColorValues.red),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                )
                              ],
                            );
                          },
                        ),
                      ),
                    ])
                  : const CustomEmptyState(),
            ),
          ),
        ]));
  }

  Widget _addAttendanceModalContent(modalFormKey) {
    return Column(
      children: [
        const Row(
          children: [
            Text(
              "Tambah Kehadiran",
              style: TextStyleValues.textBold,
            )
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Form(
          key: modalFormKey,
          child: Column(
            children: [
              CustomDropdown(
                hint: "Tipe Kehadiran",
                items: const ["h", "i", "s", "a"],
                itemToString: (value) {
                  switch (value) {
                    case "i":
                      return "Izin";
                    case "a":
                      return "Tanpa Keterangan";
                    case "h":
                      return "Hadir";
                    case "s":
                      return "Sakit";
                    default:
                      return "-";
                  }
                },
                validator: (value) {
                  if (value == null) {
                    return "Wajib diisi";
                  } else {
                    return null;
                  }
                },
                onChanged: (value) {
                  controller.attendanceType.value = value;
                },
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Expanded(
                      child: CustomDatePicker(
                          label: "Jam Masuk",
                          controller: controller.clockInController,
                          dateValueCallback: controller.clockInCallback,
                          date: controller.clockIn.value,
                          validator: (value) {
                            if (value == "") {
                              return "Wajib diisi";
                            } else {
                              return null;
                            }
                          })),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                      child: CustomDatePicker(
                          label: "Jam Pulang",
                          controller: controller.clockOutController,
                          dateValueCallback: controller.clockOutCallback,
                          date: controller.clockOut.value,
                          validator: (value) {
                            if (value == "") {
                              return "Wajib diisi";
                            } else {
                              return null;
                            }
                          })),
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              CustomTextInput(
                controller: controller.reasonController,
                label: "Keterangan",
                maxLines: 3,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
