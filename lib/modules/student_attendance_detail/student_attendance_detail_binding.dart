import 'package:get/get.dart';
import 'package:saais_client/apis/school_attendance_api.dart';
import 'package:saais_client/apis/user_api.dart';
import 'package:saais_client/modules/student_attendance_detail/student_attendance_detail_controller.dart';

class StudentAttendanceDetailBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => UserAPI());
    Get.lazyPut(() => SchoolAttendanceAPI());
    Get.lazyPut(() => StudentAttendanceDetailController());
  }
}
