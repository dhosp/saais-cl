import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:saais_client/modules/splash/splash_controller.dart';
import 'package:saais_client/values/color_values.dart';
import 'package:saais_client/values/image_values.dart';
import 'package:saais_client/values/text_style_values.dart';

class SplashScreen extends GetView<SplashController> {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    controller.redirectHome();
    return Scaffold(
        body: Container(
            color: ColorValues.white,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  child: SvgPicture.asset(
                    ImageValues.icLogo,
                    height: 200,
                    width: 200,
                  ),
                ),
                Text(
                  "Saais",
                  style: TextStyleValues.textUltraBold
                      .copyWith(color: ColorValues.bluePrimary),
                ),
              ],
            ))));
  }
}
