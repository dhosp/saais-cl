import 'package:get/get.dart';
import 'package:saais_client/routes/app_pages.dart';

class SplashController extends GetxController {
  void redirectHome() async {
    await Future.delayed(const Duration(milliseconds: 2000));

    Get.offAllNamed(Routes.home);
  }
}
