import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:saais_client/apis/teacher_note_api.dart';
import 'package:saais_client/custom_widgets/custom_loading.dart';
import 'package:saais_client/models/teacher_note.dart';
import 'package:saais_client/routes/app_pages.dart';
import 'package:saais_client/utils/get_back_route.dart';

class TeacherNoteDetailController extends GetxController {
  final _teacherNoteAPI = Get.find<TeacherNoteAPI>();

  final _noteId = int.tryParse(Get.parameters["noteId"].toString()) ?? 0;
  var isAddNew = false;
  var teacherNote = TeacherNote().obs;

  final descController = TextEditingController(text: "");

  void beforeStart() {
    if (_noteId > 0) {
      getData();
    } else if (Get.parameters["addNew"] == "true") {
      isAddNew = true;
    }
  }

  void getData() async {
    if (Get.arguments == null) {
      var result = await _teacherNoteAPI.findOne(_noteId);
      if (result.body?["status"] ?? false) {
        teacherNote.value = TeacherNote.fromJson(result.body["data"]);
      }
    } else {
      teacherNote.value = Get.arguments;
    }

    descController.text = teacherNote.value.description;
  }

  void saveData() async {
    CustomLoading.showLoading();
    Response result;
    if (isAddNew) {
      result = await _teacherNoteAPI.create(
          teacherNote: TeacherNote(
        description: descController.text,
      ));
    } else {
      result = await _teacherNoteAPI.update(
          id: _noteId, attitude: TeacherNote(description: descController.text));
    }
    CustomLoading.hideLoading();

    if (result.body?["status"] ?? false) {
      GetBackRoute.getBack(
          namedRoute: Routes.home, parameters: {"menu": "3", "tab": "3"});
    }
  }
}
