import 'package:get/get.dart';
import 'package:saais_client/middlewares/auth_middleware.dart';
import 'package:saais_client/middlewares/param_check_middleware.dart';
import 'package:saais_client/modules/achievement_detail/achievement_detail_binding.dart';
import 'package:saais_client/modules/achievement_detail/achievement_detail_screen.dart';
import 'package:saais_client/modules/attitude_detail/attitude_detail_binding.dart';
import 'package:saais_client/modules/attitude_detail/attitude_detail_screen.dart';
import 'package:saais_client/modules/attitude_report_detail/attitude_report_detail_binding.dart';
import 'package:saais_client/modules/attitude_report_detail/attitude_report_detail_screen.dart';
import 'package:saais_client/modules/class_detail/class_detail_binding.dart';
import 'package:saais_client/modules/class_detail/class_detail_screen.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_binding.dart';
import 'package:saais_client/modules/extracurricular_detail/extracurricular_detail_screen.dart';
import 'package:saais_client/modules/extracurricular_score_detail/extracurricular_score_detail_binding.dart';
import 'package:saais_client/modules/extracurricular_score_detail/extracurricular_score_detail_screen.dart';
import 'package:saais_client/modules/home/home_binding.dart';
import 'package:saais_client/modules/home/home_screen.dart';
import 'package:saais_client/modules/journal_detail/journal_detail_binding.dart';
import 'package:saais_client/modules/journal_detail/journal_detail_screen.dart';
import 'package:saais_client/modules/login/login_binding.dart';
import 'package:saais_client/modules/login/login_screen.dart';
import 'package:saais_client/modules/main_report_detail/main_report_detail_binding.dart';
import 'package:saais_client/modules/main_report_detail/main_report_detail_screen.dart';
import 'package:saais_client/modules/profile/profile_binding.dart';
import 'package:saais_client/modules/profile/profile_screen.dart';
import 'package:saais_client/modules/realtime_attendance/realtime_attendance_binding.dart';
import 'package:saais_client/modules/realtime_attendance/realtime_attendance_screen.dart';
import 'package:saais_client/modules/reset_pass/reset_pass_binding.dart';
import 'package:saais_client/modules/reset_pass/reset_pass_screen.dart';
import 'package:saais_client/modules/schedule_detail/schedule_detail_binding.dart';
import 'package:saais_client/modules/schedule_detail/schedule_detail_screen.dart';
import 'package:saais_client/modules/school_bill_detail/school_bill_detail_binding.dart';
import 'package:saais_client/modules/school_bill_detail/school_bill_detail_screen.dart';
import 'package:saais_client/modules/school_bill_individual/school_bill_individual_binding.dart';
import 'package:saais_client/modules/school_bill_individual/school_bill_individual_screen.dart';
import 'package:saais_client/modules/splash/splash_binding.dart';
import 'package:saais_client/modules/splash/splash_screen.dart';
import 'package:saais_client/modules/student_attendance_detail/student_attendance_detail_binding.dart';
import 'package:saais_client/modules/student_attendance_detail/student_attendance_detail_screen.dart';
import 'package:saais_client/modules/student_detail/student_detail_binding.dart';
import 'package:saais_client/modules/student_detail/student_detail_screen.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_binding.dart';
import 'package:saais_client/modules/subject_detail/subject_detail_screen.dart';
import 'package:saais_client/modules/subject_score_detail%20/subject_score_detail_binding.dart';
import 'package:saais_client/modules/subject_score_detail%20/subject_score_detail_screen.dart';
import 'package:saais_client/modules/teacher_attendance_detail/teacher_attendance_detail_binding.dart';
import 'package:saais_client/modules/teacher_attendance_detail/teacher_attendance_detail_screen.dart';
import 'package:saais_client/modules/teacher_detail/teacher_detail_binding.dart';
import 'package:saais_client/modules/teacher_detail/teacher_detail_screen.dart';
import 'package:saais_client/modules/teacher_note_detail/teacher_note_detail_binding.dart';
import 'package:saais_client/modules/teacher_note_detail/teacher_note_detail_screen.dart';
import 'package:saais_client/modules/teacher_note_report_detail/teacher_note_report_detail_binding.dart';
import 'package:saais_client/modules/teacher_note_report_detail/teacher_note_report_detail_screen.dart';

part 'app_routes.dart';

class AppPages {
  static final routes = [
    GetPage(
      name: Routes.splash,
      page: () => const SplashScreen(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.login,
      page: () => const LoginScreen(),
      binding: LoginBinding(),
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
        name: Routes.home,
        page: () => HomeScreen(),
        binding: HomeBinding(),
        middlewares: [AuthMiddleware()]),
    GetPage(
        name: Routes.profile,
        page: () => const ProfileScreen(),
        binding: ProfileBinding(),
        middlewares: [AuthMiddleware()]),
    GetPage(
        name: Routes.student,
        page: () => const StudentDetailScreen(),
        binding: StudentDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.teacher,
        page: () => const TeacherDetailScreen(),
        binding: TeacherDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.classroom,
        page: () => const ClassDetailScreen(),
        binding: ClassDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "classId")
        ]),
    GetPage(
        name: Routes.subject,
        page: () => const SubjectDetailScreen(),
        binding: SubjectDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "subjectId")
        ]),
    GetPage(
        name: Routes.billDetail,
        page: () => const SchoolBillDetailScreen(),
        binding: SchoolBillDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "billId")
        ]),
    GetPage(
        name: Routes.billIndividual,
        page: () => const SchoolBillIndividualScreen(),
        binding: SchoolBillIndividualBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.studentAttendance,
        page: () => const StudentAttendanceDetailScreen(),
        binding: StudentAttendanceDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.teacherAttendance,
        page: () => const TeacherAttendanceDetailScreen(),
        binding: TeacherAttendanceDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.journal,
        page: () => const JournalDetailScreen(),
        binding: JournalDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "journalId")
        ]),
    GetPage(
        name: Routes.extracurricular,
        page: () => const ExtracurricularDetailScreen(),
        binding: ExtracurricularDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "exculId")
        ]),
    GetPage(
        name: Routes.attitude,
        page: () => const AttitudeDetailScreen(),
        binding: AttitudeDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "attitudeId")
        ]),
    GetPage(
        name: Routes.teacherNote,
        page: () => const TeacherNoteDetailScreen(),
        binding: TeacherNoteDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "noteId")
        ]),
    GetPage(
        name: Routes.schedule,
        page: () => const ScheduleDetailScreen(),
        binding: ScheduleDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "scheduleId")
        ]),
    GetPage(
        name: Routes.extracurricularScore,
        page: () => const ExtracurricularScoreDetailScreen(),
        binding: ExtracurricularScoreDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "exculId")
        ]),
    GetPage(
        name: Routes.achievement,
        page: () => const AchievementDetailScreen(),
        binding: AchievementDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.teacherNoteReport,
        page: () => const TeacherNoteReportDetailScreen(),
        binding: TeacherNoteReportDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.subjectScore,
        page: () => const SubjectScoreDetailScreen(),
        binding: SubjectScoreDetailBinding(),
        middlewares: [
          AuthMiddleware(),
        ]),
    GetPage(
        name: Routes.attitudeReport,
        page: () => const AttitudeReportDetailScreen(),
        binding: AttitudeReportDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.mainReport,
        page: () => const MainReportDetailScreen(),
        binding: MainReportDetailBinding(),
        middlewares: [
          AuthMiddleware(),
          ParamCheckMiddleware(paramToCheck: "userId")
        ]),
    GetPage(
        name: Routes.realtimeAttendance,
        page: () => const RealtimeAttendanceScreen(),
        binding: RealtimeAttendanceBinding(),
        middlewares: [
          AuthMiddleware(),
        ]),
    GetPage(
      name: Routes.resetPass,
      page: () => const ResetPassScreen(),
      binding: ResetPassBinding(),
    )
  ];
}
