part of 'app_pages.dart';

abstract class Routes {
  static const splash = '/';
  static const login = '/login';
  static const home = '/home';
  static const profile = '/profile';
  static const student = '/student';
  static const teacher = '/teacher';
  static const classroom = '/class';
  static const subject = '/subject';
  static const billIndividual = '/bill';
  static const billDetail = '/bill-detail';
  static const studentAttendance = '/student-attendance';
  static const teacherAttendance = '/teacher-attendance';
  static const journal = '/journal';
  static const extracurricular = '/extracurricular';
  static const attitude = '/attitude';
  static const teacherNote = '/teacher-note';
  static const schedule = '/schedule';
  static const extracurricularScore = '/extracurricular-score';
  static const achievement = '/achievement';
  static const teacherNoteReport = '/teacher-note-report';
  static const subjectScore = '/subject-score';
  static const attitudeReport = '/attitude-report';
  static const mainReport = '/main-report';
  static const realtimeAttendance = '/realtime-attendance';
  static const resetPass = '/reset-pass';
}
