import 'package:flutter/material.dart';

class ColorValues {
  static const Color bluePrimary = Color(0xFF4392f1);
  static const Color black = Colors.black;
  static const Color white = Colors.white;
  static const Color grey = Color(0xFF8E8E8E);
  static const Color greyLight = Color(0xFFD4D5E5);
  static const Color greyBackground = Color(0xFFECE8EF);
  static const Color red = Color(0xFFFB696D);
  static const Color redBackground = Color(0xFFFFE5E5);
  static const Color green = Color(0xFF2FAF52);
  static const Color greenBackground = Color(0xFFE8FFF5);
}
