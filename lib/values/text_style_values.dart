import 'package:flutter/material.dart';

import 'color_values.dart';

class TextStyleValues {
  static const textRegular = TextStyle(
      fontSize: 14,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w400,
      color: ColorValues.black);
  static const textMedium = TextStyle(
      fontSize: 16,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w400,
      color: ColorValues.black);
  static const textBold = TextStyle(
      fontSize: 20,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w500,
      color: ColorValues.black);
  static const textUltraBold = TextStyle(
      fontSize: 28,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w800,
      color: ColorValues.black);
}
