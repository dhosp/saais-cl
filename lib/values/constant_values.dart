class ConstantValues {
  static const int maxPotraitWidth = 800;
  static const int maxDisplayedData = 100;
  static const int maxNetworkTimeout = 10; //seconds
  static const String apiUrl = String.fromEnvironment("api_url");
  static const String displayDateFormat = "dd MMMM yyyy";
  static const String displayDateTimeFormat = "dd MMMM yyyy HH:mm";
  static const String displayTimeFormat = "HH:mm";
  static const String apiDateFormat = "yyyy-MM-dd HH:mm:ss";
}
