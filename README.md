# Client Saais - Smaisga Academic Information System

# Description
Client for Saais - Smaisga Academic Information System (Siakad / Sistem Informasi Akademik)

## Getting Started
- Install Flutter n Android SDKs
- Clone and run Saais Backend repo (https://gitlab.com/dhosp/saais-be/)
- Clone this repo
- Run "flutter pub get"
- Run "flutter run --dart-define api_url="http://saais-be.url""

## Authors and Acknowledgment
Dimas Setiawan (dimasdhimek@gmail.com)

## License
This program is free software.
It is licensed under the GNU GPL version 3 or later.
That means you are free to use this program for any purpose;
free to study and modify this program to suit your needs;
and free to share this program or your modifications with anyone.
If you share this program or your modifications
you must grant the recipients the same freedoms.
To be more specific: you must share the source code under the same license.
For details see https://www.gnu.org/licenses/gpl-3.0.html
